__author__ = 'eelcovv'
# Set up a batch processing session. A  window is openen showing a table. Some buttons are put a the right
# site allowing to read a file list from a directory, and set some filters on the filename
# With the 'Execute' button the files are being processed.

import os
import logging
from PyQt4 import QtCore, QtGui
import pandas as pd
import re
import numpy as np
import glob
import fnmatch

# read the new dialog which is able to show the image
from showImageDlg import ShowImageDlg


def get_regex_pattern(search_pattern):
    """
    Routine to turn a string into a regular expression which can be used to match a string
    An empty string or an invalid search_pattern will yield a None return
    :param search_pattern:
    :return: regegular_expression_pattern
    """
    regular_expresion = None
    if search_pattern != "":
        try:
            regular_expresion = re.compile(search_pattern)
        except re.error:
            regular_expresion = None
    return regular_expresion


class BatchProcessDlg(QtGui.QDialog):
    def __init__(self, pipe, show_batch_table, parent=None):
        super(BatchProcessDlg, self).__init__(parent)

        # make sure the dialog is close (not hidden)
        # actually, I want to hide it
        # self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        # pipe is the class to all the properties of a single pipe. Must be given as a parameter. This is only used
        # connect the execution button to the method to process all the pipe in the pipeModule class. Also,
        # a dictionary with the file list created in this dialog is passed back with this call
        self.pipe = pipe

        self.show_batch_table = show_batch_table

        self.batchseries = None

        # file name of the batch file containing process information
        self.batch_file = None
        self.latest_batch_file = None

        self.process_file_batch = None

        # the selected directory for scanning all the stl files
        self.walk_dir = None

        self.logger = logging.getLogger(__name__)

        # a close button
        self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        # a start button
        self.buttonExecute = QtGui.QPushButton("&Execute")
        # a stop button
        self.buttonStop = QtGui.QPushButton("&Stop")

        # a save button to save the image
        self.buttonScanDir = QtGui.QPushButton("&Open directory")

        # a save button to save the image
        self.buttonOpenBatch = QtGui.QPushButton("&Open Batch")
        self.buttonSaveBatch = QtGui.QPushButton("&Save Batch")
        self.buttonResetBatch = QtGui.QPushButton("&Reset Batch")

        self.buttonShowPipe = QtGui.QPushButton("Show &Pipe")

        self.buttonApplyFilters = QtGui.QPushButton("&Apply Filters")

        auto_apply_layout = QtGui.QHBoxLayout()
        auto_apply_filters = QtGui.QLabel("&Auto Apply Filters")
        self.auto_apply_checkBox = QtGui.QCheckBox()
        auto_apply_filters.setBuddy(self.auto_apply_checkBox)
        self.auto_apply_checkBox.setToolTip("Automatically apply the filter settings")
        self.auto_apply_checkBox.setStatusTip(self.auto_apply_checkBox.toolTip())
        self.auto_apply_checkBox.setChecked(False)
        auto_apply_layout.addWidget(auto_apply_filters)
        auto_apply_layout.addWidget(self.auto_apply_checkBox)

        max_depth_label = QtGui.QLabel("&Maximum Search Depth:")
        self.max_depth_spinbox = QtGui.QSpinBox()
        max_depth_label.setBuddy(self.max_depth_spinbox)
        self.max_depth_spinbox.setRange(0, 999)
        self.max_depth_spinbox.setValue(1)
        self.max_depth_spinbox.setToolTip("Limit the scan depth")
        self.max_depth_spinbox.setStatusTip(self.max_depth_spinbox.toolTip())
        self.max_depth_spinbox.setFocusPolicy(QtCore.Qt.NoFocus)

        has_string_label = QtGui.QLabel("&Include Pattern:")
        self.has_string_editline = QtGui.QLineEdit()
        has_string_label.setBuddy(self.has_string_editline)
        self.has_string_editline.setToolTip("The file name should contain")
        self.has_string_editline.setStatusTip(self.has_string_editline.toolTip())

        has_not_string_label = QtGui.QLabel("&Exclude Pattern:")
        self.has_not_string_editline = QtGui.QLineEdit()
        has_not_string_label.setBuddy(self.has_not_string_editline)
        self.has_not_string_editline.setToolTip("The file name should contain")
        self.has_not_string_editline.setStatusTip(self.has_not_string_editline.toolTip())

        skip_checkbox_layout = QtGui.QHBoxLayout()
        skip_already_processsed = QtGui.QLabel("&Skip Processed:")
        self.skip_processed_checkBox = QtGui.QCheckBox()
        skip_already_processsed.setBuddy(self.skip_processed_checkBox)
        self.skip_processed_checkBox.setToolTip("Skip cases which are already processed (contain a .xls output")
        self.skip_processed_checkBox.setStatusTip(self.skip_processed_checkBox.toolTip())
        self.skip_processed_checkBox.setChecked(True)
        skip_checkbox_layout.addWidget(skip_already_processsed)
        skip_checkbox_layout.addWidget(self.skip_processed_checkBox)

        horizontalLine = QtGui.QFrame()
        horizontalLine.setFrameStyle(QtGui.QFrame.HLine)
        horizontalLine.setSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)


        # a verical layout to contain the spinbox, combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(self.buttonScanDir)
        buttonLayout.addWidget(self.buttonApplyFilters)
        buttonLayout.addLayout(auto_apply_layout)
        buttonLayout.addWidget(max_depth_label)
        buttonLayout.addWidget(self.max_depth_spinbox)
        buttonLayout.addWidget(has_string_label)
        buttonLayout.addWidget(self.has_string_editline)
        buttonLayout.addWidget(has_not_string_label)
        buttonLayout.addWidget(self.has_not_string_editline)
        buttonLayout.addLayout(skip_checkbox_layout)

        buttonLayout.addWidget(horizontalLine)

        buttonLayout.addWidget(self.buttonSaveBatch)
        buttonLayout.addWidget(self.buttonOpenBatch)
        buttonLayout.addWidget(self.buttonResetBatch)
        buttonLayout.addWidget(self.buttonShowPipe)

        buttonLayout.addWidget(horizontalLine)


        buttonLayout.addStretch()
        buttonLayout.addWidget(horizontalLine)
        buttonLayout.addWidget(self.buttonExecute)
        buttonLayout.addWidget(self.buttonStop)

        #buttonLayout.addWidget(self.buttonBox)

        self.table = QtGui.QTableWidget()
        self.table.setAlternatingRowColors(True)

        self.table.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.table.customContextMenuRequested.connect(self.handle_table_menu)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.table)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        # add some signals to the buttons and spinboxes
        self.connect(self.buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))

        self.connect(self.buttonScanDir, QtCore.SIGNAL("clicked()"),
                     self.scan_directory)

        self.connect(self.buttonApplyFilters, QtCore.SIGNAL("clicked()"),
                     self.apply_scan_directory)

        self.connect(self.buttonExecute, QtCore.SIGNAL("clicked()"),
                     self.execute_batch)

        self.connect(self.buttonSaveBatch, QtCore.SIGNAL("clicked()"),
                     self.save_batch_file)

        self.connect(self.buttonOpenBatch, QtCore.SIGNAL("clicked()"),
                     self.open_batch_file)

        self.connect(self.buttonResetBatch, QtCore.SIGNAL("clicked()"),
                     self.reset_batch_file)

        self.connect(self.buttonShowPipe, QtCore.SIGNAL("clicked()"),
                     self.show_pipe_image)

        self.connect(self.buttonStop, QtCore.SIGNAL("clicked()"),
                     self.stop_execution_batch)

        self.connect(self.has_string_editline,
                     QtCore.SIGNAL("textEdited(QString)"), self.update_file_scan)

        self.connect(self.has_not_string_editline,
                     QtCore.SIGNAL("textEdited(QString)"), self.update_file_scan)

        self.connect(self.has_not_string_editline,
                     QtCore.SIGNAL("textEdited(QString)"), self.update_file_scan)

        self.connect(self.max_depth_spinbox, QtCore.SIGNAL("valueChanged(int)"),
                     self.update_file_scan)

        self.connect(self.skip_processed_checkBox, QtCore.SIGNAL("toggled(bool)"),
                     self.update_file_scan)

        self.connect(self.table, QtCore.SIGNAL('currentItemChanged(QTableWidgetItem*, QTableWidgetItem*)'),
                     self.update_current_image_plot)

        #self.connect(self.table,
        #             QtCore.SIGNAL("itemDoubleClicked(QTableWidgetItem*)"),
        #             self.update_current_image_plot)


        settings = QtCore.QSettings()
        size = settings.value("BatchProcessDlg/Size",
                              QtCore.QVariant(QtCore.QSize(800, 500))).toSize()
        self.resize(size)
        position = settings.value("BatchProcessDlg/Position",
                                  QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
        self.move(position)

        self.setWindowTitle("Batch Processing Dialog")

    def set_row_color(self, row):
        item = QtGui.QTableWidgetItem("Text")
        item.setBackground(QtGui.QColor(255, 0, 0))
        self.table.setHorizontalHeaderItem(row, item)
        self.logger.debug("coloring row {}".format(row))
        self.updateTable()

    def update_file_scan(self):
        # called by the filter text field on change. Only apply when auto-apply checkbox has been checked
        if self.auto_apply_checkBox.isChecked():
            self.scan_directory(update_current_walk_dir=True)

    def apply_scan_directory(self):
        self.scan_directory(update_current_walk_dir=True)

    def scan_directory(self, update_current_walk_dir=False):
        """
        Scans the selector directory and retrieves all stl file. Copy the list to a process_file_batch and filter it
        :return:
        """

        if self.walk_dir is None:
            settings = QtCore.QSettings()
            self.walk_dir = unicode(settings.value("BatchProcessDlg/LastDirectory").toString())
            self.logger.debug("setting last dir loading {}".format(self.walk_dir))
        else:
            self.logger.debug("last dir is already -{}-".format(self.walk_dir))

        if not update_current_walk_dir:
            # open the directory dialog only if we are not updating a field

            self.walk_dir = unicode(
                QtGui.QFileDialog.getExistingDirectory(self, "Select Directory", self.walk_dir)
            )

        self.setWindowTitle("Batch Processing Dialog - Directory {}".format(self.walk_dir))

        self.logger.debug("scanning directory {}".format(self.walk_dir))

        # get the filter strings
        has_string = get_regex_pattern(unicode(self.has_string_editline.text()))
        self.logger.debug("{} and {}".format(unicode(self.has_string_editline.text()), has_string))
        has_not_string = get_regex_pattern(unicode(self.has_not_string_editline.text()))

        # loop over all directories and sub directories
        file_list = []
        below_scan_depth = False
        for root, subdirs, files in os.walk(self.walk_dir, topdown=True):

            # get the relative path towards the top directory (walk_dir)
            relative_path = os.path.relpath(root, self.walk_dir)

            # count the number of separators in the current path
            number_of_sep = relative_path.count(os.path.sep)

            if (self.max_depth_spinbox.value() == 0) or \
                    (relative_path != "." and number_of_sep == self.max_depth_spinbox.value() - 1):
                # stop scanning as soon as the max depth is either 0 (only current directory or if the current
                # directory is not . (top dir) and the number of sepators equals the max depth -1
                subdirs[:] = []

            # loop over the file in the current subdirectory
            for filename in files:

                (filebase, extension) = os.path.splitext(filename)

                # if the extension matches .stl, add the relative path to the list
                if re.match(".stl", extension, re.IGNORECASE):

                    add_file = False

                    if has_string is None or bool(has_string.search(filebase)):
                        # if has_string is none, the search pattern was either empty or invalid (which happens during
                        # typing the regex in the edit_box). In this case, always add the file. If not none, filter
                        # on the regex, so only add the file if the search pattern is in the filename
                        add_file = True

                    # do not add the file in case the has_not string edit has been set (!="") and if the file
                    # contains the pattern
                    if has_not_string is not None and bool(has_not_string.search(filebase)):
                        # in case we want to exclude the file, the has_not search pattern must be valid so may not be
                        # None
                        add_file = False

                    # create the full base name file
                    file_name_to_add = os.path.join(self.walk_dir, relative_path, filebase)

                    # check if the file has been procesed already by looking at the excel output file
                    if self.skip_processed_checkBox.isChecked() and os.path.exists(file_name_to_add + ".xls"):
                        add_file = False

                    # get the path to the stl relative to the selected scan directory
                    if add_file:
                        file_list.append(file_name_to_add + extension)

        # copy the scanned file list to a pandas data frame. Apply the string filter
        self.process_file_batch = pd.DataFrame(
            [[file, 0, "", 0, "", "", "", "" ] for file in file_list],
            columns=["Filename", "Status", "Timer", "Quality", "Rmean", "ID", "OOR", "Theta"])

        # update the table widget
        self.updateTable()

    def handle_table_menu(self):
        menu = QtGui.QMenu("Set Status to")
        # change the color of the menu.
        #     menu.setStyleSheet("""
        #     QMenu::item {
        #         background-color: #264F7D;
        #         color: white;
        #         font-weight:bold;}
        # """)

        text = menu.addAction("Set Status of this Pipe to")
        text.setEnabled(False)
        menu.addSeparator()

        # two menu items
        set_unprocessed = menu.addAction('Unprocessed')
        set_processed_and_ok = menu.addAction('OK')
        set_error = menu.addAction('Error')

        # chatch the right mouse button action
        action = menu.exec_(QtGui.QCursor.pos())

        # do the requested action
        if action == set_unprocessed:
            status = 0
        elif action == set_processed_and_ok:
            status = 2
        elif action == set_error:
            status = 3
        else:
            self.logger.warning("Not yet implemented")
            return

        # update status of current row
        current_row = self.table.currentRow()
        self.logger.debug("Updating row {} to status {}".format(current_row, status))

        # since we are dealing with a copy of the data frame (because it was passed as a reference to the batch
        # process routine, we can not change a single value anymore. The trick is to update the whole list of values
        status_list = self.process_file_batch["Status"]
        quality_list = self.process_file_batch["Quality"]
        status_list[current_row] = status
        if status == 0:
            quality_list[current_row] = "Unprocessed"
        elif status == 2:
            quality_list[current_row] = "OK"
        elif status == 3:
            quality_list[current_row] = "Imposed error"

        self.process_file_batch["Status"] = status_list
        self.process_file_batch["Quality"] = quality_list
        # this does not work
#        self.process_file_batch.ix[current_row]["Status"] = status
        self.logger.debug("value now  {} ".format(self.process_file_batch.ix[current_row].Status))

        self.updateTable()

    def updateTable(self, current=None):
        brushes = []
        brushes.append(QtGui.QBrush(QtGui.QColor(0, 0, 0)))
        brushes.append(QtGui.QBrush(QtGui.QColor(0, 0, 200)))   # blue: processing
        brushes.append(QtGui.QBrush(QtGui.QColor(0, 200, 0)))   # green: process and success  on check (OK)
        brushes.append(QtGui.QBrush(QtGui.QColor(200, 0, 0)))   # red: processed and failed on check STL
        brushes.append(QtGui.QBrush(QtGui.QColor(200, 200, 0))) # yellow: processed and failed on check slice

        self.table.clear()
        self.table.setRowCount(len(self.process_file_batch.index))
        self.table.setColumnCount(7)
        self.table.setHorizontalHeaderLabels(["File", "Timer", "Quality", "Rmean", "ID", "OOR", "Theta"])
        self.table.setAlternatingRowColors(True)
        self.table.setEditTriggers(QtGui.QTableWidget.NoEditTriggers)
        self.table.setSelectionBehavior(QtGui.QTableWidget.SelectRows)
        self.table.setSelectionMode(QtGui.QTableWidget.SingleSelection)
        selected = None
        for row, stl_file in enumerate(self.process_file_batch.Filename.tolist()):
            item = QtGui.QTableWidgetItem(stl_file)
            if current is not None and current == id(stl_file):
                selected = item
            item.setData(QtCore.Qt.UserRole, QtCore.QVariant(long(id(stl_file))))
            item.setForeground(brushes[self.process_file_batch.loc[row].Status])
            self.table.setItem(row, 0, item)

            try:
                item = QtGui.QTableWidgetItem(self.process_file_batch.loc[row].Timer)
                self.table.setItem(row, 1, item)
            except TypeError:
                self.logger.debug("Could not set the Timer")

            try:
                item = QtGui.QTableWidgetItem(self.process_file_batch.loc[row].Quality)
                self.table.setItem(row, 2, item)
            except TypeError:
                self.logger.debug("Could not set the Quality")

            try:
                item = QtGui.QTableWidgetItem("{}".format(self.process_file_batch.loc[row].Rmean))
                self.table.setItem(row, 3, item)
            except TypeError:
                self.logger.debug("Could not set the Rmean")

            try:
                item = QtGui.QTableWidgetItem("{}".format(self.process_file_batch.loc[row].ID))
                self.table.setItem(row, 4, item)
            except TypeError:
                self.logger.debug("Could not set the ID")

            try:
                item = QtGui.QTableWidgetItem("{}".format(self.process_file_batch.loc[row].OOR))
                self.table.setItem(row, 5, item)
            except TypeError:
                self.logger.debug("Could not set the OOR")

            try:
                item = QtGui.QTableWidgetItem("{}".format(self.process_file_batch.loc[row].Theta))
                self.table.setItem(row, 6, item)
            except TypeError:
                self.logger.debug("Could not set the Theta")

        self.table.resizeColumnsToContents()
        if selected is not None:
            selected.setSelected(True)
            self.table.setCurrentItem(selected)
            self.table.scrollToItem(selected)

    def save_batch_file(self):
        self.logger.debug("Save the batch file")

        if self.batch_file is None:
            settings = QtCore.QSettings()
            self.batch_file = unicode(settings.value("BatchProcessDlg/LastBatchFile").toString())
            self.logger.debug("setting last batch file to {}".format(self.batch_file))

        fname = self.batch_file if self.batch_file is not None else "."
        formats = (["*.{}".format(format.lower()) for format in ["xls"]])

        fname = QtGui.QFileDialog.getSaveFileName(self,
                                                  "Pipe Slicer - Export Batch file", fname,
                                                  "Excel files ({})".format(" ".join(formats)))
        if fname:
            self.logger.info("Writing batch info to {}".format(fname))

            writer = pd.ExcelWriter(str(fname))
            self.process_file_batch.to_excel(writer)
            writer.save()

    def open_batch_file(self):
        self.logger.debug("Open the batch file")
        try:
            settings = QtCore.QSettings()
            self.batch_file = unicode(settings.value("BatchProcessDlg/LastBatchFile").toString())
            self.logger.debug("setting last batch file to {}".format(self.batch_file))
        except AttributeError:
            self.logger.debug("Could not get last batch file.")

        fname = self.batch_file if self.batch_file is not None else "."
        formats = (["*.{}".format(format.lower()) for format in ["xls"]])

        fname = QtGui.QFileDialog.getOpenFileName(self,
                                                  "Pipe Slicer - Export Batch file", fname,
                                                  "Excel files ({})".format(" ".join(formats)))
        if fname:
            self.logger.info("Reading batch info from  {}".format(fname))
            self.process_file_batch = pd.read_excel(str(fname))
            self.updateTable()

            # update the last batch file
            settings = QtCore.QSettings()
            settings.setValue("BatchProcessDlg/LastBatchFile", fname)

    def reset_batch_file(self):
        self.logger.debug("Resetting the batch serie")
        self.process_file_batch["Status"] = 0
        self.process_file_batch["Quality"] = ""
        self.process_file_batch["Rmean"] = ""
        self.process_file_batch["ID"] = ""
        self.process_file_batch["OOR"] = ""
        self.process_file_batch["Theta"] = ""
        self.updateTable()

    def show_pipe_image(self):
        self.logger.debug("showing the current pipe")

        if self.buttonShowPipe.isEnabled():
            try:
                self.logger.debug("opening the image dialog")
                self.showImageDlg = ShowImageDlg(self.buttonShowPipe)
                self.showImageDlg.update_logger(self.logger.level)
                self.showImageDlg.show()
                self.buttonShowPipe.setEnabled(False)
            except TypeError:
                pass

            self.update_current_image_plot()
        else:
            self.logger.debug("closing the image")
            self.close_pipe_image()

    def close_pipe_image(self):
        self.showImageDlg = None
        if self.buttonShowPipe.isEnabled():
            self.buttonShowPipe.setEnabled(True)

    def execute_batch(self):
        self.logger.debug("Execute the batch")

        self.buttonExecute.setEnabled(False)
        self.buttonScanDir.setEnabled(False)
        self.pipe.emit(QtCore.SIGNAL("start_batch_execute"), self.process_file_batch)
        QtGui.QApplication.processEvents()

    def stop_execution_batch(self):
        self.logger.debug("Stop Executing the batch")

        self.buttonExecute.setEnabled(True)
        self.buttonScanDir.setEnabled(True)

        self.pipe.emit(QtCore.SIGNAL("stop_batch_execute"), self.process_file_batch)
        QtGui.QApplication.processEvents()

    def update_logger(self, level):
        self.logger.setLevel(level)

    def update_current_image_plot(self):
        self.logger.debug("updating current plot")
        current_row = None
        stl_file = None
        current_row = self.table.currentRow()
        try:
            stl_file = self.table.item(current_row, 0).text()
        except AttributeError:
            self.logger.debug("No row selected. go back")
            return

        out_dir = re.sub(".stl$","_out",str(stl_file))
        self.logger.debug("out_dir {}".format(out_dir))
        search_pattern = os.path.join(out_dir,"*_{}*.png".format(self.pipe.slice_extend_name))
        self.logger.debug("search pattern {}".format(search_pattern))
        png_list = glob.glob(search_pattern)
        self.logger.debug("png_list {}".format(png_list))
        image_files = []
        try:
            image_files.append(png_list[0])
        except IndexError:
            image_files.append(None)

        try:
            search_pattern = os.path.join(out_dir,"*{}.png".format(self.pipe.export_extension_list[1]))
            self.logger.debug("second search pattern list {}".format(search_pattern))
            png_list = glob.glob(search_pattern)
            self.logger.debug("second png list {}".format(png_list))
            image_files.append(png_list[0])
        except IndexError:
            image_files.append(None)

        self.logger.debug("plotting list {}".format(image_files))
        try:
            self.showImageDlg.image_name_list = image_files
            self.showImageDlg.show_image(0)
        except AttributeError:
            self.logger.debug("No image dialog opened yet. Do nothing")

def closeEvent(self, event):
    # before closing the window store its size and position
        settings = QtCore.QSettings()
        if self.walk_dir is not None:
            self.logger.debug("saving dialog dir {}".format(self.walk_dir))
            settings.setValue("BatchProcessDlg/LastDirectory", os.path.dirname(self.walk_dir))

        settings.setValue("BatchProcessDlg/LastBatchFile", self.batch_file)
        settings.setValue("BatchProcessDlg/Size", QtCore.QVariant(self.size()))
        settings.setValue("BatchProcessDlg/Position",
                          QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_batch_table.isChecked():
            self.show_batch_table.setChecked(False)
            self.show_batch_table.setEnabled(True)

        # close the image as well
        self.showImageDlg = None
