from __future__ import division
from builtins import range
from past.utils import old_div
__author__ = 'eelcovv'
# this dialog will plot the contours

import logging

from PyQt4 import QtGui, QtCore

import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import re
import numpy as np
from numpy import pi


class ContourPlotDlg(QtGui.QDialog):
    def __init__(self, pipe, show_contour_plot, parent=None):
        super(ContourPlotDlg, self).__init__(parent)

        self.pipe = pipe

        self.show_contour_plot = show_contour_plot

        self.logger = logging.getLogger(__name__)

        # a close button
        self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        # a save button to save the image
        self.buttonSave = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save)

        # a save button to save the image
        self.buttonEdit = QtGui.QPushButton("Edit Axes...")

        # a widget to contain the image
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)

        # a tool bar which we don't show, but we only take the save image functionality
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.hide()

        self._contours = None

        self.image = None

        # initialise the axis with a polar plot
        self.axis = []
        self.aidlines = []
        self.aidpoints = []

        self.init_plot()

        # a combobox to ask for the plot type
        labelCombo = QtGui.QLabel(self.tr("Plot Type:"))
        labelCombo.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.plotTypeComboBox = QtGui.QComboBox()
        self.plotTypeComboBox.addItem("AlphaBeta")
        self.plotTypeComboBox.addItem("Spherical Angle JPDF")

        # a verical layout combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(labelCombo)
        buttonLayout.addWidget(self.plotTypeComboBox)
        buttonLayout.addWidget(self.buttonEdit)

        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonSave)
        buttonLayout.addWidget(self.buttonBox)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.canvas)
        # graphLayout.addWidget(self.toolbar)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        # layout.addWidget(self.canvas,10)
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        # add some signals to the buttons and
        self.connect(self.buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))
        self.buttonSave.clicked.connect(self.save_figure)

        self.connect(self.buttonEdit, QtCore.SIGNAL("clicked()"),
                     self.edit_axis)

        self.connect(self.plotTypeComboBox, QtCore.SIGNAL("currentIndexChanged(int)"),
                     self.reset_axis)

        settings = QtCore.QSettings()

        size = settings.value("ContourPlotDlg/Size",
                              QtCore.QVariant(QtCore.QSize(800, 500))).toSize()
        self.resize(size)
        position = settings.value("ContourPlotDlg/Position",
                                  QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
        self.move(position)

        self.setWindowTitle("Pipe Contour Plot")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def edit_axis(self):
        self.toolbar.edit_parameters()

    def save_figure(self):
        self.toolbar.save_figure()

    def reset_axis(self, plot_number=0):
        # reset the axis. In case you want to do this from a batch (without the dialog, pass the init_plot_number
        # as an argument
        #self.clear_contours()
        self.figure.clear()

        self.init_plot(plot_number)

        self.canvas.draw()
        self.update_plots(plot_number)

    def init_plot(self,plot_type=0):
        # initialise the current plot area. Called every time we change plot type with the combo box
        self.axis = []
        gs = gridspec.GridSpec(1, 1)
        # create some extra space
        gs.update(bottom=0.1)

        self.bbox_to_anchor_pos = (1.3, 1.1)
        self.axis.append(self.figure.add_subplot(gs[0, 0]))

        if plot_type==0:
            self.axis[-1].set_xlabel(r"$\alpha$ [rad]")
            self.axis[-1].set_ylabel(r"$\beta$ [rad]")
            self.axis[-1].relim()
            self.axis[-1].autoscale_view(True,True,True)
            self.axis[-1].set_title(r"Summation Nx^2 vs rotational angle over y and z axis")

        if plot_type == 1:
            self.axis[-1].set_title(r"Spherical angle distribution")
            self.axis[-1].set_xlim(-pi,pi)
            self.axis[-1].set_ylim(old_div(-pi,2),old_div(pi,2))
            self.axis[-1].set_xticks([-pi, old_div(-pi, 2), 0, old_div(pi,2), pi])
            self.axis[-1].set_yticks([old_div(-pi, 2), 0, old_div(pi,2)])
            self.axis[-1].set_xticklabels([r"$-\pi$", r"$-\pi/2$", 0, r"$\pi/2$", r"$\pi$"])
            self.axis[-1].set_yticklabels([r"$-\pi/2$", 0, r"$\pi/2$"])
            self.axis[-1].set_xlabel(r"$\phi$ [rad]")
            self.axis[-1].set_ylabel(r"$\theta$ [rad]")
    def clear_contours(self):

        if self.contours:
            # remove the contours. There are both present in the axis and contours variable. Bit tricky, but
            # this works
            while self.contours.collections:
                for coll in self.contours.collections:
                    self.axis[0].collections.remove(coll)
                    self.contours.collections.remove(coll)
                self.contours.collections=[]
                self.axis[0].collections=[]

        # also remove the text labels if present
        for i in range(len(self.axis[0].texts)):
                self.axis[0].texts[0].remove()

        # finally, clean up the scatter points and lines if present
        if self.aidpoints is not None:
            self.aidpoints=[]
        if self.aidlines is not None:
            self.aidlines[0].remove()
            self.aidlines=[]

    def update_plots(self, plot_number=None):
        if plot_number is not None:
            number = plot_number
        elif self.plotTypeComboBox.currentText() == "AlphaBeta":
            number = 0
        else:
            number = 1

        if number == 0:
            self.logger.debug("calling alpha beta")
            self.update_alpha_beta_plot()
        else:
            self.logger.debug("calling theta phi")
            self.update_theta_phi_plot()

    def update_alpha_beta_plot(self):
        try:
            self.logger.debug("Updating the contour plot")
            self.axis[0].imshow(np.transpose(self.pipe.xcoorsqrsum), interpolation='nearest', origin='low',
                        extent=[self.pipe.alpha[0],
                                self.pipe.alpha[-1],
                                self.pipe.beta[0],
                                self.pipe.beta[-1]])
            self.canvas.draw()
        except IndexError:
            pass

    def update_theta_phi_plot(self):
        try:
            self.logger.debug("Plotting theta-phi PDF")
            self.axis[0].imshow(self.pipe.theta_phi_pdf, interpolation='nearest', origin='low',
                    extent=[self.pipe.theta_edges[0],
                            self.pipe.theta_edges[-1],
                            self.pipe.phi_edges[0],
                            self.pipe.phi_edges[-1]])
            self.canvas.draw()
        except AttributeError:
            pass

    def save_figure_batch(self, filename):
        self.logger.debug("Saving figure to {}".format(filename))
        plt.figure(self.figure.number)
        plt.savefig(filename, bbox_inches='tight', pad_inches=.1)

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        settings.setValue("ContourPlotDlg/Size", QtCore.QVariant(self.size()))
        settings.setValue("ContourPlotDlg/Position",
                          QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_contour_plot.isChecked():
            self.show_contour_plot.setChecked(False)
            self.show_contour_plot.setEnabled(True)
