from __future__ import print_function
from __future__ import absolute_import
from builtins import str
from builtins import range
from builtins import object

__author__ = 'eelcovv'
"""
PipeSlicer: software package to read an STL file, align it and create slices at given positions

required: python 2.7
libraries: vtk 5

PipeSlicer.py is the main routine in which the GUI is set up and all control is taken care off. The numerical
core is put into a separate module: pipeModule, containing a class to hold all the properties of a pipe

Eelco van Vliet
2015
"""

import sys
import os
import platform
import errno
from win32api import LoadResource
import oval_fitter as ovalf
import matplotlib.pyplot as plt

from pipe_fitter.pyqt_import import *

# import the parameter tree widget
from pyqtgraph.parametertree import ParameterTree
import logging as log
import vtk
from vtk.qt4.QVTKRenderWindowInteractor import QVTKRenderWindowInteractor
import pandas as pd
from collections import OrderedDict
import re
from numpy import random
import time
from setup_settings import set_and_write_default_values
import line_profiler  # only required if the performance of the routines needs to be evaluated

# all parameters are given via a parameter_tree. This widget is given by pyqtgraph. The
# parameter_tree is fully given by the module file PipeParameters. In order to transfer the
# parameters given by the parameter tree
# to the pipeModule class, the routine transfer_parameters is used
from parameters import PipeParameters
import pipeModule as pm

# the dialog show a plot of the slice of a profile
from SlicePlotDialog import SlicePlotDlg

# The dialog showing a contour plot of the normal vector distribution
from ContourPlotDialog import ContourPlotDlg

# The dialog  used to set up a batch processing session
from BatchProcessDlg import BatchProcessDlg

import numpy as np

import geometryUtils as gu

logfile = None
from distutils import version
import _version

try:
    # when the binary version of pipeslice is running, search for the VERSIONTAG which was set by
    # setup.py
    __version__ = LoadResource(0, u"VERSIONTAG", 1)
    # succesfully loaded VERSIONTAG, so we are using the binary mode
    # therefore define the output log file
    from os.path import expanduser

    home = expanduser("~")
    logbase = os.path.join(home, ".PipeSlicer")
    logfile = logbase + ".log"
    sys.stderr = open(logbase + ".err", 'w')
except Exception as e:
    # if the normal python code is running, just get the version from the _version module set with
    # versioneer
    print("get versioneer version")
    __version__ = _version.get_versions()['version']
    print("version = {}".format(__version__))

except:
    # both version retrievals failed. Raise an error
    raise

# import all the icons images (should be generated with
# pyrcc4 -py2 -o resources.py resources.qrc
# where resource.qrc is an xml file containing a list with the aliases
import resources


class Timer(object):
    # this class can be used to time a event
    def __init__(self, message="Elapsed time", name="routine", verbose=True):
        self.verbose = verbose
        self.message = message
        self.secs = 0
        self.name = name
        self.logger = log.getLogger(__name__)

    def __enter__(self):
        self.start = time.time()
        return self

    def __exit__(self, *args):
        self.end = time.time()
        self.secs = self.end - self.start
        if self.verbose:
            self.logger.info(
                "{:<20s} {:<20s} : {:>10.0f} s".format(self.message, self.name, self.secs))


def get_clean_version(version):
    """
    turns the full version string into a clean one without the build
    :param version:
    :return:
    """
    match = re.search("([.|\d]+)([+]*)(.*)", __version__)
    if bool(match):
        version = match.group(1)
    else:
        version = __version__
    return version


class PipeSlicerMain(QMainWindow):
    # @profile
    def __init__(self, loghisory_file, parent=None):
        super(PipeSlicerMain, self).__init__(parent)

        # initial logging level is WARNING (no output except to make warnings). Can be changed via
        # the gui later
        log.basicConfig(filename=loghisory_file,
                        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
                        level=log.INFO, filemode='w')

        self.logger = log.getLogger(__name__)
        self.logger.info("Start Pipe Slicer version {}".format(__version__))

        self.dirty = False
        self.filename = None
        self.recentFiles = list()
        self.filenameStl = None
        self.screenshotfile = None
        self.exportfile = None
        self.exportStlfile = None
        self.exportXlsfile = None
        self.outdir = None

        # the name of the target points file
        self.export_targets_file = None
        self.target_file_extension = None

        # initialise some render actors
        # for some reason, the cubeaxes can not be deleted without getting into problems (a crash
        # later). So just create them one time and only update the bounding box
        self.cubeAxesActor = vtk.vtkCubeAxesActor()
        self.initCubeAxis()
        self.surfaceActor = None
        self.cutActor = None

        # build the whole GUI
        self.setupGUI()

        # add a permanent progress bar
        self.progress_bar = QtGui.QProgressBar(self.statusBar())
        self.statusBar().addPermanentWidget(self.progress_bar)
        self.hide_progress_bar()

        # set up the parameter tree at the left and connect the signall
        self.pars = PipeParameters()
        self.pars.connect_signals_to_slots()

        # will contain the plot canvas later
        self.slicePlotDialog = None
        self.batchProcessDlg = None

        # a flag controlling if we are batchprocessing or not
        self.stop_batch_processing = False

        # the class to hold all the data related to the pipe
        self.pipe = pm.PipeAnalyse()

        # the parameter tree
        self.tree.setParameters(self.pars.Parameters, showTop=False)

        # call the method which copies all the parameters set in the parametertree
        self.transfer_parameters()

        # set empty actors
        self.ovality_target_actors = list()
        self.ovality_target_plane_actors = list()
        self.ovality_target_text_actors = list()
        self.cut_plane = None
        self.cut_plane_arrow = None

        # TODO: update all qt 4 stuff to qt 5 stuff.
        # create some connections
        self.connect(self.pars.Parameters,
                     QtCore.SIGNAL('tree_parameter_changed'), self.transfer_parameters)

        self.connect(self.pipe, QtCore.SIGNAL("update_statusbar_message"),
                     self.show_message_in_status_bar)

        self.connect(self.pipe, QtCore.SIGNAL("update_alpha_beta_contour_plot"),
                     self.update_alpha_beta_contour_plot)

        self.connect(self.pipe, QtCore.SIGNAL("progressChanged"),
                     self.update_progress)
        self.connect(self.pipe, QtCore.SIGNAL("displayFinished"),
                     self.hide_progress_bar)

        self.connect(self.pipe, QtCore.SIGNAL("start_batch_execute"),
                     self.batch_execute)

        self.connect(self.pipe, QtCore.SIGNAL("stop_batch_execute"),
                     self.stop_batch_execution)

        # the section below is used to store the windows settings  of this application so that with a next
        # start up size and position can be restored
        settings = QtCore.QSettings()

        if sys.version_info[0] == 2:
            self.recentFiles = settings.value("RecentFiles").toStringList()
            size = settings.value("PipeSlicerMainWindow/Size",
                                  QtCore.QVariant(QtCore.QSize(1200, 1000))).toSize()
            self.resize(size)
            position = settings.value("PipeSlicerMainWindow/Position",
                                      QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
            self.move(position)

            self.restoreState(
                settings.value("PipeSlicerMainWindow/State").toByteArray())
        else:
            self.recentFiles = settings.value("RecentFiles") or []
            self.restoreGeometry(settings.value("PipeSlicerMainWindow/Geometry",
                                                QtCore.QByteArray()))
            self.restoreState(settings.value("PipeSlicerMainWindow/State",
                                             QtCore.QByteArray()))
        self.loadInitialFile()

    def show_message_in_status_bar(self, message=None, timer=0):
        self.statusBar().showMessage(self.tr(message), timer)

    def update_progress(self, n, nrows, message=""):
        self.progress_bar.show()
        self.progress_bar.setRange(0, nrows)
        self.progress_bar.setValue(n)
        self.statusBar().showMessage(self.tr(message))

    def hide_progress_bar(self):
        self.progress_bar.hide()
        self.statusBar().showMessage(self.tr("Ready"), 5000)

    # @profile
    def transfer_parameters(self, parameter=None):

        if parameter is not None:
            self.logger.debug("received signal from {}".format(parameter))
            self.dirty = True

        # the different sections of the parameter tree are stored in new variables for easier reference
        self.pars_slice = self.pars.Parameters.names["Slice"]
        self.pars_graph = self.pars.Parameters.names["Graph"]
        self.pars_numerical = self.pars.Parameters.names["Numerical"]
        self.pars_general = self.pars.Parameters.names["General"]
        self.pars_camera = self.pars.Parameters.param("Camera")

        # set the power of the units in order to convert from m to the units of the STL file
        self.pipe.power_units = self.pars_general.power_units.value()

        # create a slice list
        self.pipe.create_slice_list(self.pars_slice.X_start.value(), self.pars_slice.X_end.value(),
                                    self.pars_slice.n_slices.value())

        self.pipe.align_profile_cogs = self.pars_slice.align_profile_cogs.value()

        self.pipe.check_slice_index = self.pars_slice.check_slice_index.value()

        self.pipe.alpha_beta_tolerance = self.pars_numerical.tolerance.value()
        self.pipe.n_alpha = self.pars_numerical.n_alpha_beta_1.value()
        self.pipe.n_beta = self.pipe.n_alpha
        self.pipe.n_alpha_2 = self.pars_numerical.n_alpha_beta_2.value()
        self.pipe.n_beta_2 = self.pipe.n_alpha_2
        self.pipe.n_max_iterations = self.pars_numerical.n_max_iters_1.value()
        self.pipe.n_max_iterations_2 = self.pars_numerical.n_max_iters_2.value()
        self.pipe.reduction_factor = self.pars_numerical.reduction_factor.value()

        self.pipe.max_angle_reference_points = self.pars_numerical.max_angle_reference_points.value()
        self.pipe.max_pair_distance = self.pars_numerical.max_pair_distance.value()
        self.pipe.exclude_near_distance = self.pars_numerical.exclude_near_distance.value()

        self.pipe.max_slice_angle = self.pars_numerical.max_slice_angle.value()
        self.pipe.min_bounding_box = self.pars_numerical.min_bounding_box.value()

        if parameter is not None and bool(re.search("Update Reference Point", parameter)):
            self.logger.debug("Updating reference Point position")
            self.pipe.get_indices_nearest_neighbours()
            self.pipe.get_index_reference_point()
            self.updateVtkFrame()

        self.pipe.maximum_connected_regions_1 = self.pars_numerical.max_connected_regions_1.value()
        self.pipe.maximum_connected_regions_2 = self.pars_numerical.max_connected_regions_2.value()
        self.pipe.pick_largest_region = self.pars_numerical.pick_largest_region.value()

        self.pipe.clip_x_position_below = self.pars_numerical.clip_x_position_below.value()
        self.pipe.clip_x_position_above = self.pars_numerical.clip_x_position_above.value()
        self.pipe.clip_r_range = self.pars_numerical.clip_r_range.value()

        if parameter is not None and bool(re.search("Grab Camera", parameter)):
            self.logger.debug("Grabbing the camera position")
            self.pars_camera.store_camera(self.renderer.GetActiveCamera())

        if parameter is not None and bool(re.search("Restore Camera", parameter)):
            self.logger.debug("Restoring the camera position")
            self.pars_camera.restore_camera(self.renderer.GetActiveCamera())
            self.updateVtkFrame()

        # set the verbosity mode
        if parameter is not None and bool(re.search("Verbosity Mode", parameter)):
            verbosity_mode = int(self.pars_general.verbosity_mode.value())
            if verbosity_mode == 0:
                self.logger.info("Switching to WARNING Verbosity mode")
                self.logger.setLevel(log.WARNING)
            elif verbosity_mode == 1:
                self.logger.info("Switching to INFO Verbosity mode")
                self.logger.setLevel(log.INFO)
            elif verbosity_mode == 2:
                self.logger.info("Switching to DEBUG Verbosity mode")
                self.logger.setLevel(log.DEBUG)
            else:
                self.logger.warning(
                    "Verbosity mode {} not recoginised. Doing nothing".format(verbosity_mode))

            # also update the logger for the pipe and parameter class
            self.pipe.update_logger(self.logger.level)
            self.pars.update_logger(self.logger.level)
            try:
                self.slicePlotDialog.update_logger(self.logger.level)
            except AttributeError:
                pass
            try:
                self.batchProcessDlg.update_logger(self.logger.level)
            except AttributeError:
                pass

        if parameter is not None and bool(re.search("Graph", parameter)):
            # update the graph only if we change a Graph parameter
            self.updateVtkFrame()

        # button actions
        if parameter is not None and bool(re.search("Remove floor.plane ", parameter)):
            self.update_clip_plane()

        if parameter is not None and bool(
                re.search("Remove floor.Remove points below plane", parameter)):
            self.remove_model_section_below_clip_plane()

        if parameter is not None and bool(
                re.search("Remove floor.Remove smallest unconnected region", parameter)):
            self.RemoveSmallestRegion()

        if parameter is not None and bool(
                re.search("Remove floor.Remove all but XXX biggest regions", parameter)):
            self.KeepOnlyBiggestRegions()

        if parameter is not None and bool(
                re.search("Ellipse fitting.Create reference points.Make", parameter)):
            self.make_ovality_targets()

    # @profile
    def CloseSlicePlot(self):
        self.slicePlotDialog = None
        if self.showSlicePlot.isChecked():
            self.showSlicePlot.setChecked(False)

    # @profile
    def CloseContourPlot(self):
        self.ContourPlotDialog = None
        if self.showContourPlot.isChecked():
            self.showContourPlot.setChecked(False)

    # @profile
    def OpenSlicePlot(self):
        if self.showSlicePlot.isChecked():
            self.slicePlotDialog = SlicePlotDlg(self.pipe, self.showSlicePlot)
            self.slicePlotDialog.update_logger(self.logger.level)
            self.slicePlotDialog.update_figure()
            self.slicePlotDialog.show()
            self.showSlicePlot.setEnabled(False)
        else:
            self.CloseSlicePlot()

    def OpenContourPlot(self):
        if self.showContourPlot.isChecked():
            try:
                self.ContourPlotDialog = ContourPlotDlg(self.pipe, self.showContourPlot)
                self.ContourPlotDialog.update_logger(self.logger.level)
                self.ContourPlotDialog.update_plots()
                self.ContourPlotDialog.show()
                self.showContourPlot.setEnabled(False)
            except TypeError:
                pass
        else:
            self.CloseContourPlot()

    def update_alpha_beta_contour_plot(self):
        self.logger.debug("updating contour plot")
        if self.showContourPlot.isChecked():
            self.ContourPlotDialog.update_alpha_beta_plot()

    def BatchProcess(self):
        self.logger.info("start batch processing")
        if self.batchProcessAction.isChecked():
            log.debug("Open the dialog")
            if self.batchProcessDlg is None:
                self.batchProcessDlg = BatchProcessDlg(self.pipe, self.batchProcessAction)
            self.batchProcessDlg.update_logger(self.logger.level)
            self.batchProcessDlg.show()
            self.batchProcessAction.setEnabled(False)
        else:
            self.CloseBatchProcess()

    # @profile
    def CloseBatchProcess(self):
        if self.batchProcessDlg is not None:
            self.batchProcessDlg.hide()
            try:
                self.batchProcessDlg.showImageDlg.hide()
                self.batchProcessDlg.showImageDlg = None
            except AttributeError:
                pass

            self.batchProcessDlg.hide()
        if self.batchProcessAction.isChecked():
            self.batchProcessAction.setChecked(False)

    # @profile
    def clearActors(self):
        # remove all the actors from the scene and update. For testing mostly
        self.clearVTKFrame()
        self.pars_camera.restore_camera(self.renderer.GetActiveCamera())
        # self.renderer.ResetCamera()
        self.iren.GetRenderWindow().Render()

    # @profile
    def redrawActors(self):
        # remove all the actors from the scene and update. For testing mostly
        self.updateVtkFrame()
        self.pars_camera.restore_camera(self.renderer.GetActiveCamera())
        # self.renderer.ResetCamera()

    # @profile
    def setupGUI(self):
        # create the GUI of the main window

        # the file menu actions
        fileOpenAction = self.createAction("&Open...", self.fileOpen,
                                           QtGui.QKeySequence.Open, "fileopen",
                                           "Open a Pipe Slicer Configuration file")
        fileSaveAction = self.createAction("&Save", self.fileSave,
                                           QtGui.QKeySequence.Save, "filesave",
                                           "Save the current configuration")
        fileSaveAsAction = self.createAction("Save &As...", self.fileSaveAs,
                                             QtGui.QKeySequence.SaveAs,
                                             icon="filesaveas",
                                             tip="Save the current configuration to a new file")
        ImportSTLAction = self.createAction("&Import STL file...",
                                            self.importSTL, "Ctrl+I",
                                            icon="stlimport",
                                            tip="Import the STL data")
        self.ExportAction = self.createAction("&Export Slice Data...",
                                              self.sliceDataExportAs, "Ctrl+E",
                                              icon="export",
                                              tip="Export the data", disabled=True)
        self.ExportSTLAction = self.createAction("E&xport STL Data ...",
                                                 self.StlDataExportAs, "Ctrl+X",
                                                 icon="export",
                                                 tip="Export the data", disabled=True)
        self.screenShotAction = self.createAction("Save Screen S&hot...",
                                                  self.SaveScreenShot, "Ctrl+H",
                                                  icon="screenshot",
                                                  tip="Dump the current screen to file",
                                                  disabled=False)
        fileQuitAction = self.createAction("&Quit", self.close,
                                           "Ctrl+Q", "filequit", "Close the application")

        self.alignPipeAction = self.createAction("&Align the Pipe", self.AlignPipe,
                                                 "Ctrl+A", "alignpipe",
                                                 "Align the Pipe with the x-axis", disabled=True)
        self.slicePipeAction = self.createAction("&Slice the Pipe", self.SlicePipe,
                                                 "Ctrl+S", "tube1",
                                                 "Slice the Pipe at the given slice positions",
                                                 disabled=False)

        self.findOvalityTargets = self.createAction("&Find ovality targets",
                                                    slot=self.find_ovality_targets,
                                                    shortcut="Ctrl+O",
                                                    tip="Find the target positions for ovality calculation",
                                                    icon="findtarget",
                                                    disabled=False)

        self.calcOvality = self.createAction("&Calc ovality ",
                                             slot=self.calc_ovality,
                                             shortcut="Ctrl+O",
                                             tip="Calculate ovality at found target positions",
                                             icon="fitellipse",
                                             disabled=False)

        self.process_all_action = self.createAction("A&ll...", self.process_all, "Ctrl+L",
                                                    "processall",
                                                    "Perform all processing steps to get the slices of one pipe",
                                                    disabled=True)

        self.connectivityAction = self.createAction("&Clear STL", self.FilterConnectivity,
                                                    "Ctrl+C", "connectivity",
                                                    "Clear the STL by removing all non-connected triangles",
                                                    disabled=True)

        self.clipPipeAction = self.createAction("Clip the &pipe ", self.ClipPipe,
                                                "Ctrl+P", "cutter",
                                                "Clip the pipe to remove all triangles above the given x-position",
                                                disabled=True)

        self.batchProcessAction = self.createAction("&Batch Process", self.BatchProcess,
                                                    "Ctrl+B", "batch_process",
                                                    "Start a batch process session", True,
                                                    "toggled(bool)",
                                                    disabled=False)

        self.showSlicePlot = self.createAction("S&lice",
                                               self.OpenSlicePlot, "Ctrl+L", "plots",
                                               "Show the slices", True, "toggled(bool)",
                                               disabled=True)

        self.showContourPlot = self.createAction("Contou&r",
                                                 self.OpenContourPlot, "Ctrl+R", "surface",
                                                 "Show the contour", True, "toggled(bool)",
                                                 disabled=False)

        self.clearActorsAction = self.createAction("&Clear Scene ", self.clearActors,
                                                   "Ctrl+A", "clear",
                                                   "Clear all actors in the scene",
                                                   disabled=True)

        self.redrawActorsAction = self.createAction("&Redraw Scene", self.redrawActors,
                                                    "Ctrl+R", "redraw",
                                                    "Redraw all the actors in the scene",
                                                    disabled=True)

        self.aboutAction = self.createAction("&About...", self.OpenAboutDialog,
                                             "Ctrl+A", None, "Show info about this tool")

        # create the file tool bar
        self.fileToolbar = self.addToolBar("File")
        self.fileToolbar.setObjectName("FileToolBar")
        self.addActions(self.fileToolbar,
                        (fileOpenAction, fileSaveAction, ImportSTLAction, self.ExportAction))

        self.processToolBar = self.addToolBar("Process")
        self.processToolBar.setObjectName("processToolBar")
        self.addActions(self.processToolBar,
                        [self.batchProcessAction, self.process_all_action, self.slicePipeAction,
                         self.findOvalityTargets, self.calcOvality])

        # create the plot tool bar
        # self.processExpertToolBar = self.addToolBar("Process Expert")
        # self.processExpertToolBar.setObjectName("processExpertToolBar")
        # self.addActions(self.processExpertToolBar,
        #                 [self.alignPipeAction, self.clipPipeAction, self.connectivityAction, self.slicePipeAction])

        # create the plot tool bar
        self.plotToolBar = self.addToolBar("Plots")
        self.plotToolBar.setObjectName("PlotToolBar")
        self.addActions(self.plotToolBar,
                        [self.showSlicePlot, self.showContourPlot, self.clearActorsAction,
                         self.redrawActorsAction])

        # create the file menu
        self.fileMenu = self.menuBar().addMenu("&File")
        self.fileMenuActions = (fileOpenAction, fileSaveAction, fileSaveAsAction, ImportSTLAction,
                                self.ExportSTLAction, self.ExportAction, self.screenShotAction,
                                fileQuitAction)
        self.connect(self.fileMenu, QtCore.SIGNAL("aboutToShow()"),
                     self.updateFileMenu)

        # create the process menu
        self.processMenu = self.menuBar().addMenu("&Processs")
        self.addActions(self.processMenu,
                        [self.alignPipeAction, self.clipPipeAction, self.connectivityAction,
                         self.slicePipeAction,
                         self.process_all_action, self.batchProcessAction, self.findOvalityTargets])

        # create the plot menu
        self.plotMenu = self.menuBar().addMenu("P&lots")
        self.addActions(self.plotMenu,
                        [self.showSlicePlot, self.showContourPlot, self.clearActorsAction,
                         self.redrawActorsAction])

        # create the help menu
        self.helpMenu = self.menuBar().addMenu("&Help")
        self.addActions(self.helpMenu,
                        [self.aboutAction])

        # this layout is required to put in the central widget
        self.layout = QtGui.QGridLayout()
        self.layout.setContentsMargins(0, 0, 0, 0)

        # QFrame is a conventient way to show the VTK renderer
        self.frame = QtGui.QFrame()

        self.tree = ParameterTree(showHeader=False)

        self.hl = QtGui.QHBoxLayout()
        self.vtkWidget = QVTKRenderWindowInteractor(self.frame)
        self.hl.addWidget(self.tree, 2)
        self.hl.addWidget(self.vtkWidget, 5)

        self.renderer = vtk.vtkRenderer()
        self.vtkWidget.GetRenderWindow().AddRenderer(self.renderer)
        self.iren = self.vtkWidget.GetRenderWindow().GetInteractor()

        self.renderer.SetBackground(0.5, 0.5, 0.5)

        self.frame.setLayout(self.hl)

        self.setCentralWidget(self.frame)

        self.show()
        self.iren.Initialize()

        # add the parameter tree to the left side

        # activate the status bar
        self.sizeLabel = QtGui.QLabel()
        self.sizeLabel.setFrameStyle(QtGui.QFrame.StyledPanel | QtGui.QFrame.Sunken)
        self.statusbar = self.statusBar()
        self.statusbar.setSizeGripEnabled(False)
        self.statusbar.addPermanentWidget(self.sizeLabel)
        self.statusbar.showMessage("Ready", 5000)

    # @profile
    def initCubeAxis(self):
        # define the initialisation of the cubeaxes here, because you can do it only once (in the init of the class)
        self.cubeAxesActor.GetTitleTextProperty(0).SetColor(1.0, 0.0, 0.0)
        self.cubeAxesActor.GetLabelTextProperty(0).SetColor(1.0, 0.0, 0.0)

        self.cubeAxesActor.GetTitleTextProperty(1).SetColor(0.0, 1.0, 0.0)
        self.cubeAxesActor.GetLabelTextProperty(1).SetColor(0.0, 1.0, 0.0)

        self.cubeAxesActor.GetTitleTextProperty(2).SetColor(0.0, 0.0, 1.0)
        self.cubeAxesActor.GetLabelTextProperty(2).SetColor(0.0, 0.0, 1.0)

        self.cubeAxesActor.DrawXGridlinesOn()
        self.cubeAxesActor.DrawYGridlinesOn()
        self.cubeAxesActor.DrawZGridlinesOn()
        try:
            self.cubeAxesActor.SetGridLineLocation(vtk.VTK_GRID_LINES_FURTHEST)
        except AttributeError:
            self.cubeAxesActor.SetGridLineLocation(vtk.VTK_GRID_NEAREST)

        self.cubeAxesActor.XAxisMinorTickVisibilityOff()
        self.cubeAxesActor.YAxisMinorTickVisibilityOff()
        self.cubeAxesActor.ZAxisMinorTickVisibilityOff()

    # @profile
    def clearVTKFrame(self):

        # two version to clear all the actors, both of them seem to work
        # try:
        #     log.debug("clearing the render window")
        #     self.renderer.RemoveAllViewProps()
        #     self.iren.ExitCallback()
        #
        #     self.updateVtkFrame()
        # except AttributeError:
        #     log.debug("did not succeed remove all")
        # except:
        #     raise

        actors = self.renderer.GetActors()
        while actors.GetLastActor() is not None:
            self.renderer.RemoveActor(actors.GetLastActor())

    # @profile
    def updateVtkFrame(self, ipos=None, posx=None, name=None):
        # update the current vtk frame
        if self.pipe.stl_surface is not None:
            # we have a stl_surface loaded, to plot it so the render window

            # in case of any previous present surfaces: remove them
            self.clearVTKFrame()

            if self.pars_graph.wall_show.value():
                self.renderer.AddActor(
                    create_surface_actor(self.pipe.stl_surface,
                                         self.pars_graph.wall_color.value().getRgbF(),
                                         self.pars_graph.wall_opacity.value(), False)
                )
            if self.pars_graph.wall_show_mesh.value():
                self.renderer.AddActor(
                    create_surface_actor(self.pipe.stl_surface, (0, 0, 0),
                                         self.pars_graph.wall_opacity.value(), True)
                )

            # a pointer to the polydata spheres containing all the targets
            try:
                self.logger.debug(
                    "get the point of the polydata structure containing the reference points")
                vtk_polydata_points = self.pipe.vtk_targets_polydata.GetPoints()
                self.pipe.has_targets = True
            except AttributeError:
                self.logger.info(" No targets are present!")
                self.pipe.has_targets = False

            if self.pipe.has_targets:
                # create the reference points if available
                self.sphereActors = []
                if self.pars_graph.targets_show.value() and self.pipe.target_points is not None:
                    for index, row in self.pipe.target_points.iterrows():
                        target_position = vtk_polydata_points.GetPoint(index)

                        self.sphereActors.append(
                            create_sphere_actor(target_position,
                                                self.pars_graph.targets_size.value(),
                                                self.pars_graph.targets_color.value().getRgbF())
                        )

                        self.renderer.AddActor(self.sphereActors[-1])

                # create a sphere at the reference target of the STL pipe
                self.neighbourActors = []
                size_pair = [1, 1]
                self.logger.debug("neightbour list {}".format(self.pipe.neighbour_list))
                if self.pars_graph.ref_point_show.value():
                    if self.pipe.reference_point is not None:
                        ref_pointActor = create_sphere_actor(self.pipe.reference_point,
                                                             self.pars_graph.ref_point_size.value(),
                                                             self.pars_graph.ref_point_color.value().getRgbF()
                                                             )
                        self.renderer.AddActor(ref_pointActor)

                    if self.pars_graph.pair_show.value() > -1 and len(self.pipe.neighbour_list) > 0:
                        index_pair = min(self.pars_graph.pair_show.value(),
                                         len(self.pipe.neighbour_list[:]) - 1)
                        for j in range(2):
                            index = self.pipe.neighbour_list[index_pair][j]

                            target_position = vtk_polydata_points.GetPoint(index)

                            self.logger.debug(
                                "{} {} {} {}".format(index_pair, j, index, target_position))
                            self.neighbourActors.append(
                                create_sphere_actor(target_position,
                                                    self.pars_graph.ref_point_size.value(),
                                                    self.pars_graph.pair_color.value().getRgbF())
                            )

                            self.renderer.AddActor(self.neighbourActors[-1])

            # create a sphere at the origin
            if self.pars_graph.origin_show.value():
                originActor = create_sphere_actor([0, 0, 0], self.pars_graph.origin_size.value(),
                                                  self.pars_graph.origin_color.value().getRgbF())
                self.renderer.AddActor(originActor)

            if self.cutActor is not None:
                self.pipe.cutMapper.Update()
                self.renderer.AddActor(self.cutActor)

            # add the cubeaxes here and stay away from it
            self.renderer.AddActor(self.cubeAxesActor)

            if self.pars_graph.show_cube_axis.value():
                self.logger.debug("switchin axis on")
                BB = self.pipe.stl_surface.GetOutput().GetBounds()
                self.logger.debug("updating bounds of axes {}".format(BB))
                self.cubeAxesActor.SetBounds(BB)
                self.cubeAxesActor.SetVisibility(True)
                self.cubeAxesActor.SetCamera(self.renderer.GetActiveCamera())
            else:
                self.logger.debug("switchin axis off")
                self.cubeAxesActor.SetVisibility(False)

            annotation = ""
            if name is not None:
                annotation += "Scan : {}".format(name)
            try:
                annotation += "\nSlice # {} at position ({:6.1f},{:6.2f},{:6.2f})".format(ipos + 1,
                                                                                          posx,
                                                                                          self.pipe.centroid.x,
                                                                                          self.pipe.centroid.y)
            except:
                pass

            if self.pipe.reference_point is not None:
                try:
                    indexstr = " index={:3d} ".format(self.pipe.index_reference_point)
                except ValueError as e:
                    indexstr = ""
                annotation += "\n{:<20} :{}location=({:6.1f},{:6.1f},{:6.1f})".format(
                    "Reference Point",
                    indexstr,
                    self.pipe.reference_point.X,
                    self.pipe.reference_point.Y,
                    self.pipe.reference_point.Z)
            try:
                CL = self.pipe.corner_bottom_low
                TH = self.pipe.corner_top_high
                self.logger.debug("CL {}".format(CL))
                self.logger.debug("TH {}".format(TH))
                annotation += "\n{:<20} : ({:6.1f},{:6.1f},{:6.1f}) ({:6.1f},{:6.1f},{:6.1f})".format(
                    "Bounding Box",
                    CL[0], CL[1],
                    CL[2], TH[0],
                    TH[1], TH[2])
            except:
                pass

            if annotation != "":
                try:
                    self.renderer.RemoveActor(self.textActor)
                    self.textActor.Delete()
                except:
                    pass

                self.textActor = vtk.vtkTextActor()
                self.textActor.SetPosition2(0, 1)
                self.textActor.GetTextProperty().SetFontSize(18)
                self.textActor.GetTextProperty().SetFontFamilyToCourier()
                self.textActor.GetTextProperty().SetColor(0, 0, 1)
                self.textActor.SetInput(annotation)
                self.renderer.AddActor(self.textActor)

        self.iren.GetRenderWindow().Render()
        QtGui.QApplication.processEvents()

    def addActions(self, target, actions):
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)

    def createAction(self, text, slot=None, shortcut=None, icon=None,
                     tip=None, checkable=False, signal="triggered()", disabled=False):
        action = QtGui.QAction(text, self)
        if icon is not None:
            action.setIcon(QtGui.QIcon(":/{}.png".format(icon)))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, QtCore.SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        if disabled:
            action.setEnabled(False)
        return action

    def closeEvent(self, event):
        if self.okToContinue():
            # save the current state of the window and files
            settings = QtCore.QSettings()

            if sys.version_info[0] == 2:
                filename = QtCore.QVariant(QtCore.QString(self.filename)) \
                    if self.filename is not None else QtCore.QVariant()
                recentFiles = QtCore.QVariant(self.recentFiles) \
                    if self.recentFiles else QtCore.QVariant()
                settings.setValue("RecentFiles", recentFiles)
                settings.setValue("LastFile", filename)
                if self.filenameStl is not None:
                    settings.setValue("LastDirectory", os.path.dirname(self.filenameStl))
                settings.setValue("PipeSlicerMainWindow/Size", QtCore.QVariant(self.size()))
                settings.setValue("PipeSlicerMainWindow/Position",
                                  QtCore.QVariant(self.pos()))
                settings.setValue("PipeSlicerMainWindow/State",
                                  QtCore.QVariant(self.saveState()))
            else:
                settings.setValue("LastFile", self.filename)
                settings.setValue("RecentFiles", self.recentFiles or [])
                settings.setValue("PipeSlicerMainWindow/Geometry", self.saveGeometry())
                settings.setValue("PipeSlicerMainWindow/State", self.saveState())

            # make sure that the dialog is also closed
            self.CloseSlicePlot()
            self.CloseContourPlot()
            self.CloseBatchProcess()
        else:
            event.ignore()

    def okToContinue(self):
        if self.dirty:
            reply = QtGui.QMessageBox.question(self,
                                               "Settings changed",
                                               "Save unsaved changes?",
                                               QtGui.QMessageBox.Yes | QtGui.QMessageBox.No |
                                               QtGui.QMessageBox.Cancel)
            if reply == QtGui.QMessageBox.Cancel:
                return False
            elif reply == QtGui.QMessageBox.Yes:
                self.fileSave()
        return True

    # @profile
    def loadInitialFile(self):
        settings = QtCore.QSettings()
        if sys.version_info[0] == 3:
            fname = settings.value("LastFile")
        else:
            fname = str(settings.value("LastFile").toString())

        self.logger.info("last file: {}".format(fname))
        if fname and QtCore.QFile.exists(fname):
            self.logger.debug("loading {}".format(fname))
            self.statusBar().showMessage(
                "Initialising configuration : {} ...".format(os.path.basename(fname)))
            self.loadFile(fname)
        else:
            self.logger.debug("setting initial parameters ")
            self.statusBar().showMessage("Initialising default configuration...")
            self.transfer_parameters()

        self.statusBar().showMessage("Ready.", 5000)

    def updateFileMenu(self):
        self.fileMenu.clear()
        self.addActions(self.fileMenu, self.fileMenuActions[:-1])
        current = self.filename
        recentFiles = []
        for fname in self.recentFiles:
            if fname != current and QtCore.QFile.exists(fname):
                recentFiles.append(fname)
        if recentFiles:
            self.fileMenu.addSeparator()
            for i, fname in enumerate(recentFiles):
                action = QtGui.QAction("&{} {}".format(i + 1, QtCore.QFileInfo(fname).fileName()),
                                       self)
                action.setIcon(QtGui.QIcon(":/pipe.png"))
                action.setData(fname)
                self.connect(action, QtCore.SIGNAL("triggered()"),
                             self.loadFile)
                self.fileMenu.addAction(action)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.fileMenuActions[-1])

    def fileOpen(self):
        if not self.okToContinue():
            return
        dir = os.path.dirname(self.filename) \
            if self.filename is not None else "."

        formats = ["*.%s" % str(extension).lower() for extension in ["psc"]]

        fname = str(QtGui.QFileDialog.getOpenFileName(self,
                                                      "Pipe Slicer - Choose Configuration", dir,
                                                      "Configuration files ({})".format(
                                                          " ".join(formats))))

        if fname:
            self.loadFile(fname)

    # @profile
    def importSTL(self, fname=None):

        settings = QtCore.QSettings()

        if fname is None:
            if not self.okToContinue():
                return

            if sys.version_info[0] == 2:
                dir = str(settings.value("LastDirectory").toString())
            else:
                dir = settings.value("LastDirectory")

            formats = ["*.%s" % str(extension).lower() for extension in ["stl"]]

            fname = str(QtGui.QFileDialog.getOpenFileName(self,
                                                          "Pipe Mesh - Choose STL File", dir,
                                                          "STL files ({})".format(
                                                              " ".join(formats))))
        if fname:
            # import the stl
            self.logger.debug("Inside Import STL {} {}".format(type(fname), fname))
            self.importSTLFile(fname)

            self.filenameStl = fname

            settings = QtCore.QSettings()
            settings.setValue("LastDirectory", os.path.dirname(self.filenameStl))

            # enable some buttons of the tool bar
            self.alignPipeAction.setEnabled(True)
            self.screenShotAction.setEnabled(True)
            self.process_all_action.setEnabled(True)
            self.slicePipeAction.setEnabled(True)
            self.findOvalityTargets.setEnabled(True)
            self.showContourPlot.setEnabled(True)
            self.connectivityAction.setEnabled(True)
            self.clipPipeAction.setEnabled(True)
            self.clearActorsAction.setEnabled(True)
            self.redrawActorsAction.setEnabled(True)
            self.ExportSTLAction.setEnabled(False)
            self.ExportAction.setEnabled(False)

            # clear the actor lists for targets and planes
            self.ovality_target_actors = list()
            self.ovality_target_plane_actors = list()
            self.ovality_target_text_actors = list()

    # @profile
    def importSTLFile(self, fname=None):

        self.logger.debug("Inside Import STLFile {} {}".format(type(fname), fname))
        if fname is None:
            action = self.sender()
            if isinstance(action, QtGui.QAction):
                fname = str(action.data().toString())
                if not self.okToContinue():
                    return
            else:
                return
        if fname:
            # import the STL data
            (filebase, ext) = os.path.splitext(fname)
            self.logger.debug(
                "getting extension of the target : ".format(
                    self.pars_general.extension_target_file.value()))
            (self.target_file_extension, ext2) = os.path.splitext(
                self.pars_general.extension_target_file.value())

            if ext2 is not "":
                # if an extension was given then take this one
                self.target_file_extension = self.pars_general.extension_target_file.value()
            else:
                # of now extension was given, assume it to be .txt
                self.target_file_extension += ".txt"

            self.targets_file = filebase + self.target_file_extension

            self.logger.debug("Inside Import STL {} {}".format(type(fname), fname))
            self.show_message_in_status_bar("Loading {}".format(fname))
            self.logger.debug("Loading {}".format(fname))
            self.pipe.import_the_stlfile(fname)

            self.prepare_output(filebase)

            self.logger.debug("checking {}".format(self.targets_file))
            if os.path.exists(self.targets_file):
                self.show_message_in_status_bar("Loading {}".format(self.targets_file))
                self.logger.debug("Reading targets {}".format(self.targets_file))
                self.pipe.import_the_reference_point(self.targets_file)
            else:
                self.logger.warning("Could not find the reference point file")
                self.pipe.has_targets = False

            if self.pipe.has_targets:
                # in case of a proper set up of the target points, we have only one target point which belongs
                # to two pairs, so we can already identify it (which the pipe being aligned)
                self.pipe.get_index_reference_point()

            # show the message in the status bar
            self.updateStatus("Ready")
            self.logger.debug("all done, updating vtk now")
            self.updateVtkFrame()
            self.redrawActors()
            self.updateVtkFrame()

    def loadFile(self, fname=None):
        if fname is None:
            action = self.sender()
            if isinstance(action, QtGui.QAction):
                fname = str(action.data().toString())
                if not self.okToContinue():
                    return
            else:
                return
        if fname:
            self.filename = None
            if self.pars.loadConfiguration(fname):
                self.addRecentFile(fname)
                self.filename = fname
                self.transfer_parameters()
                self.dirty = False

                message = "Loaded {}".format(os.path.basename(fname))
                self.logger.debug("{}".format(message))
            else:
                message = "Failed to load the statef file"
                self.logger.debug("{}".format(message))

            self.updateStatus(message)

    def fileSave(self):
        if self.filename is None:
            self.fileSaveAs()
        else:
            if self.pars.saveConfiguration(self.filename):
                self.updateStatus("Saved as %s" % self.filename)
                self.dirty = False
            else:
                self.updateStatus("Failed to save %s" % self.filename)

    def fileSaveAs(self):
        fname = self.filename if self.filename is not None else "."
        formats = ["*.%s" % str(format).lower() for format in ["psc"]]
        fname = str(QtGui.QFileDialog.getSaveFileName(self,
                                                      "Pipe Slicer - Save Configuration", fname,
                                                      "Configuration files (%s)" % " ".join(
                                                          formats)))

        if fname:
            if "." not in fname:
                fname += ".psc"
            self.addRecentFile(fname)
            self.filename = str(fname)
            self.fileSave()

    # @profile
    def sliceDataExport(self):
        if self.exportfile is None:
            self.sliceDataExportAs()
        else:
            self.pipe.write_slice_file(self.exportfile)

    def sliceDataExportAs(self):
        fname = self.exportfile if self.exportfile is not None else "."
        formats = (["*.{}".format(format.lower())
                    for format in ["xls"]])
        fname = QtGui.QFileDialog.getSaveFileName(self,
                                                  "Pipe Slicer - Export Slice data", fname,
                                                  "Excel files ({})".format(" ".join(formats)))
        if fname:
            self.exportfile = str(fname)
            self.show_message_in_status_bar("Writing to {}".format(self.exportfile))
            self.sliceDataExport()
            self.updateStatus("Ready.")

    # @profile
    def StlDataExport(self):
        if self.exportStlfile is None:
            self.StlDataExportAs()
        else:
            # first save the stl data
            self.pipe.write_stl_file(self.exportStlfile)

            # also save the target points
            if self.pipe.target_points is not None:
                self.export_targets_file = re.sub("\.stl", self.target_file_extension,
                                                  self.exportStlfile,
                                                  re.IGNORECASE)
                self.pipe.write_target_points(self.export_targets_file)

                # update the export file name of the slice file based on the new stl file
                # self.exportfile = re.sub("\.stl$", ".h5", self.exportStlfile)

    # @profile
    def StlDataExportAs(self):
        fname = self.exportStlfile if self.exportStlfile is not None else "."
        formats = (["*.{}".format(format.lower())
                    for format in ["stl"]])
        fname = QtGui.QFileDialog.getSaveFileName(self,
                                                  "Pipe Slicer - Export STL data", fname,
                                                  "STL files ({})".format(" ".join(formats)))
        if fname:
            self.exportStlfile = str(fname)

            self.show_message_in_status_bar("Writing to {}".format(self.exportStlfile))
            self.StlDataExport()
            self.updateStatus("Ready.")

    def SaveScreenShotAs(self):
        fname = self.screenshotfile if self.screenshotfile is not None else "."
        formats = (["*.{}".format(format.lower())
                    for format in ["png"]])
        fname = QtGui.QFileDialog.getSaveFileName(self,
                                                  "Pipe Slicer - Screen Shot", fname,
                                                  "PNG files ({})".format(" ".join(formats)))
        if fname:
            self.screenshotfile = str(fname)

            self.show_message_in_status_bar("Writing to {}".format(self.screenshotfile))
            self.SaveScreenShot()
            self.updateStatus("Ready.")

    def SaveScreenShot(self):
        if self.screenshotfile is None:
            self.SaveScreenShotAs()
        else:
            self.pipe.save_image_frame(self.screenshotfile, self.vtkWidget.GetRenderWindow())

    # @profile
    def prepare_output(self, filebase):
        self.filenameStl = filebase + ".stl"
        self.exportStlfile = filebase + "_aligned.stl"
        self.exportfile = filebase + ".xls"
        self.exportXlsfile = filebase + ".xls"
        # create an output directory based on the filebase name
        self.outdir = filebase + "_out"
        self.export_image_base = os.path.join(self.outdir, os.path.basename(filebase))
        try:
            os.makedirs(self.outdir)
        except OSError as exc:
            if exc.errno == errno.EEXIST and os.path.isdir(self.outdir):
                pass
            else:
                raise

    def OpenAboutDialog(self):
        QtGui.QMessageBox.about(self, "About PipeSlicer",
                                """<b>PipeSlicer {}</b> (Build tag {})
                                <p>Copyright &copy; 2015 HMC Heerema Marine Contractors.
                                All rights reserved.
                                <p>This application can be used to create slices
                                of an STL mesh of a pipe.
                                <p>Python {} - Qt {} - PyQt {} on {} - VTK {}
                                <p>Author: Eelco van Vliet (evanvliet@hmc-heerema.com)
                                <p>Contributors: Ruben de Bruin (rdbruin@hmc-heerema.com)
                                """.format(
                                    get_clean_version(__version__),
                                    __version__,
                                    platform.python_version(),
                                    QtCore.QT_VERSION_STR,
                                    QtCore.PYQT_VERSION_STR,
                                    platform.system(),
                                    vtk.VTK_VERSION
                                ))

    def updateStatus(self, message):
        self.statusBar().showMessage(message, 5000)
        windows_title = "Pipe Slicer {}".format(get_clean_version(__version__))
        if self.filename is not None:
            windows_title += " - {}".format(os.path.basename(self.filename))
        if self.filenameStl is not None:
            windows_title += " - {}".format(os.path.basename(self.filenameStl))
        windows_title += "[*]"
        self.setWindowTitle(windows_title)
        self.setWindowModified(self.dirty)

    def addRecentFile(self, fname):
        if fname is None:
            return

        if fname not in self.recentFiles:
            self.recentFiles.prepend(QtCore.QString(fname))
            while self.recentFiles.count() > 9:
                self.recentFiles.takeLast()

    # @profile
    def AlignPipe(self, maximum_number_of_iterations=None, n_sample_1=None, n_sample_2=None):
        self.logger.info("Aligning the pipe")
        # for the button, the settings of the next iteration are taken
        if maximum_number_of_iterations is None:
            maxiter = self.pars_numerical.n_max_iters_2.value()
        else:
            # the iteration are passed ny the arguments in case we are calling AlingPipe from the ProcessAll routine
            maxiter = maximum_number_of_iterations

        if n_sample_1 is None:
            n_smpl_1 = self.pars_numerical.n_alpha_beta_1.value()
        else:
            n_smpl_1 = n_sample_1

        if n_sample_2 is None:
            n_smpl_2 = self.pars_numerical.n_alpha_beta_2.value()
        else:
            n_smpl_2 = n_sample_2

        # call the alignment routine
        self.pipe.align_pipe(maxit=maxiter, n_sample_1=n_smpl_1, n_sample_2=n_smpl_2)

        try:
            # only update if it exists
            self.ContourPlotDialog.update_plots()
        except AttributeError:
            pass

        self.clearVTKFrame()

        self.updateVtkFrame()
        self.ExportSTLAction.setEnabled(True)

    def process_all(self):
        self.logger.info("Process All")
        with Timer(name="clean_and_position_the_pipe()") as t:
            self.clean_and_position_the_pipe()
        self.SlicePipe()

        self.check_the_quality()

    def check_the_quality(self):
        # perform a quality check and raise a warning if it was not ok
        message = self.quality_check()
        if message != "OK":
            reply = QtGui.QMessageBox.warning(self,
                                              "Quality check",
                                              "Failed check with message: {}".format(message))
            self.logger.warning("Warning was issued : {}".format(message))
        else:
            self.logger.info("Quality check return : {}".format(message))

    # @profile
    def ClipPipe(self, enlarge_factor=1):
        self.logger.info("Clipping the pipe")
        self.pipe.clip_pipe(enlarge_factor)

        self.clearVTKFrame()

        self.updateVtkFrame()
        self.ExportSTLAction.setEnabled(True)

    def KeepOnlyBiggestRegions(self):
        treesection = self.pars.Parameters.names["Remove floor"]
        n = treesection.n_regions_to_keep.value()
        self.FilterConnectivity(n)

    def RemoveSmallestRegion(self):
        """
        Removes the smallest connected region from the pipe
        :return:
        """
        n_remaining = self.pipe.remove_smallest_region()

        self.show_message_in_status_bar("Sections remaining: {}".format(n_remaining))

        self.clearVTKFrame()
        self.updateVtkFrame()
        self.ExportSTLAction.setEnabled(True)

    # @profile
    def FilterConnectivity(self, max_regions=None):
        if max_regions is None:
            # if this value is none, either the function is called by clicking the Filter option directly from
            # the menu, or is was call from process_all with the value set to None because pick_largest_region is True
            # In the first case (called from the Filter from the menu), we still need to set a value if pick_largest
            # is not True. We pick the value from the final iteration (number 2)
            if not self.pipe.pick_largest_region:
                max_regions = self.pipe.maximum_connected_regions_2

        self.logger.info("Filtering connectivity of the pipe")
        self.pipe.filter_on_connectivity(max_regions, progres_function=self.update_progress)

        self.clearVTKFrame()

        self.updateVtkFrame()
        self.ExportSTLAction.setEnabled(True)

    # @profile
    def SlicePipe(self):
        self.logger.info("Slicing the pipe")
        self.cutActor = vtk.vtkActor()
        self.cutActor.GetProperty().SetColor(0, 0, 1)
        self.cutActor.GetProperty().SetEdgeColor(0, 1, 0)
        self.cutActor.GetProperty().SetLineWidth(2)
        self.cutActor.SetMapper(self.pipe.cutMapper)

        filebase = os.path.basename(self.export_image_base)

        pipe_slices_dict = OrderedDict()
        pipe_stats_dict = OrderedDict()
        for index, posx in enumerate(self.pipe.slice_list):

            self.show_message_in_status_bar("Getting slice {} at position {}".format(index, posx))

            slice_inner_wall_dataframe = self.pipe.slice_pipe(posx)

            self.updateVtkFrame(index, posx, filebase)

            # this extension is used for the filenaming and slice naming. Contains a index and the x-position
            frame_ext = "{}{:06d}".format(self.pipe.slice_extend_name, int(round(posx)))

            # create a new entry to the panel data using a dictionary
            if slice_inner_wall_dataframe is not None:
                self.logger.info(
                    "storing slice {} at position {} with label {}".format(index, posx, frame_ext))
                pipe_slices_dict[frame_ext] = slice_inner_wall_dataframe

                pipe_stats_dict[frame_ext] = self.pipe.get_stats_slice(slice_inner_wall_dataframe)

                if index == self.pipe.check_slice_index:
                    # store the label name of the slice which was set as the check slice
                    self.pipe.check_slice_name = frame_ext

                if self.pars_general.auto_export_png.value() > 0:
                    # dump a screen shot of the current slice
                    if (self.pars_general.auto_export_png.value() == 2) or (
                            index == self.pipe.check_slice_index):
                        # only write the screen shot if the auto_export_png is 2 or if it is 1 and the slice
                        # is now the current check slice
                        im_file = "{}_{}{}".format(self.export_image_base, frame_ext, ".png")
                        self.pipe.save_image_frame(im_file, self.vtkWidget.GetRenderWindow())
            else:
                self.logger.info("no slice found so not adding to the dict")

        # put all the slices into a multi-indexed data frame
        if len(list(pipe_slices_dict.items())) > 0:
            # add the current slice (with radius vs phi) in the total data frame with all the slice
            log.info("Number of slices obtained : {}".format(len(list(pipe_slices_dict.items()))))
            self.pipe.slice_dataframes = pd.concat(pipe_slices_dict)

            # also store the statistics info in a total data frame. Now the index contains the slice labels,
            # there we need to get rid of the old index with the dummy place holder (actually, we obtain a
            # multiindex by the concat operation). To overwrite the multiindex, call reset_index, over write
            # it with level_0 (which contains the slice labels) and delete the level_0 and level_1 columns
            self.pipe.slice_stat_df = pd.concat(pipe_stats_dict).reset_index()
            self.pipe.slice_stat_df.index = self.pipe.slice_stat_df.level_0
            self.pipe.slice_stat_df.drop('level_0', axis=1, inplace=True)
            self.pipe.slice_stat_df.drop('level_1', axis=1, inplace=True)

            self.ExportAction.setEnabled(True)
            self.showSlicePlot.setEnabled(True)
        else:
            log.warning("Slicing did not yield any hits")
            self.pipe.slice_dataframes = None
            self.ExportAction.setEnabled(False)

    def quality_check(self):
        """
        Checks the quality of the STL file and slices and returns a warning message if something is wrong:
        :return: QualityMessage
        """
        message = ""

        # convert the clip radius [m] to the units of the STL file
        f = 10 ** self.pipe.power_units

        size_of_domain = self.pipe.size_of_domain
        clip_r = self.pars_numerical.clip_r_range.value() * f
        self.logger.debug("comparing size {} to radius {}".format(size_of_domain, clip_r))
        for i in range(2):
            try:
                if size_of_domain[i + 1] < self.pipe.min_bounding_box * (2 * clip_r):
                    message += "Bounding box too small; "
            except ValueError:
                message += "Could not check bounding box"

        # check if we found a reference point
        if self.pipe.reference_point is None:
            message += "No reference point found"

        # check the inclination of the plane
        try:
            theta = self.pipe.slice_stat_df.ix[self.pipe.check_slice_name].theta
            self.logger.debug("checking angles of the plane : {}".format(theta))

            if abs(theta) > self.pipe.max_slice_angle:
                message += "slice plane inclined  with {} degrees; ".format(theta)
        except AttributeError:
            pass

        # no check failed yet, so set it to OK
        if message == "":
            message = "OK"

        return message

    def stop_batch_execution(self):
        self.logger.debug("Stop Batch Execution")
        self.stop_batch_processing = True

    # @profile
    def clean_and_position_the_pipe(self):
        # a list of processing steps to get the pipe at its right position
        # monitor the stop_batch_processing flag to see if the STOP button has been pushed by the batchprocessing
        # dialog
        if self.stop_batch_processing:
            return

        # set the maximum number of connected regions. If pick_largest_region is true, only the largest is used
        max_connected_regions = None
        if not self.pipe.pick_largest_region:
            max_connected_regions = self.pipe.maximum_connected_regions_1

        # remove all unconnected triangles
        self.logger.debug("Filter connectivity with {} regions".format(max_connected_regions))
        self.FilterConnectivity(max_connected_regions)

        self.logger.info("Align, clip and filter the STL data")
        # align it roughly (hence the maximum iteration is set to the max iter of the first iteration)
        self.AlignPipe(maximum_number_of_iterations=self.pars_numerical.n_max_iters_1.value(),
                       n_sample_1=self.pars_numerical.n_alpha_beta_1.value(),
                       n_sample_2=self.pars_numerical.n_alpha_beta_2.value())

        if self.stop_batch_processing:
            return

        # clip the pipe with an enlarged clip box (not too tight yet) because we havenot yet aligned properly and
        # also the number of connected regions is larger, so we need more space around the pipe
        enlargement_factor = self.pars_numerical.enlargement_factor.value()
        if bool(self.pars_numerical.second_pre_alignment.value()):
            # in case the enlargement factor is larger than 1: Clip with a bounding box enlarged with a factor x
            # and then align again using the full alpha-beta space
            self.logger.info("A Pre clip with enlargement factor {}".format(enlargement_factor))
            self.pipe.is_aligned = False
            self.ClipPipe(enlarge_factor=enlargement_factor)
            self.AlignPipe(maximum_number_of_iterations=self.pars_numerical.n_max_iters_1.value(),
                           n_sample_1=self.pars_numerical.n_alpha_beta_1.value(),
                           n_sample_2=self.pars_numerical.n_alpha_beta_2.value())

        # clip all triangle above a certain x coordinate and outside the given radius
        self.logger.debug(
            "Clipping without enlargement".format(self.pipe.maximum_connected_regions_2))
        self.ClipPipe()

        if self.stop_batch_processing:
            return

        # do a final more accurate alignment with the clean stl file
        # since we already have aligned the pipe, do not take (the usually finer) resolution of the first
        # iteration, but immediately take the value of the next iterations
        self.AlignPipe(maximum_number_of_iterations=self.pars_numerical.n_max_iters_2.value(),
                       n_sample_1=self.pars_numerical.n_alpha_beta_2.value(),
                       n_sample_2=self.pars_numerical.n_alpha_beta_2.value())

        if not self.pipe.pick_largest_region:
            # the second connectivity filter is only applied when the pick_largest_region is False
            max_connected_regions = self.pipe.maximum_connected_regions_2
            self.logger.debug("Filter connectivity with {} regions".format(max_connected_regions))
            self.FilterConnectivity(max_connected_regions)

        self.ClipPipe()

        self.AlignPipe(maximum_number_of_iterations=self.pars_numerical.n_max_iters_2.value(),
                       n_sample_1=self.pars_numerical.n_alpha_beta_2.value(),
                       n_sample_2=self.pars_numerical.n_alpha_beta_2.value())

        if self.stop_batch_processing:
            return

        # export the new aligned STL file for later usage
        if self.pars_general.auto_export_stl.value():
            self.show_message_in_status_bar("Writing aligned pipe to {}".format(self.exportStlfile))
            self.StlDataExport()

    # @profile
    def batch_execute(self, process_file_batch=None):
        self.stop_batch_processing = False
        self.logger.debug("Executing the batch")
        self.show_message_in_status_bar("Executing batch...", timer=5000)
        if process_file_batch is None:
            self.logger.warning("trying to batch process but know files are selected")
            return

        for i in process_file_batch.index:
            # extract the current file from the batch data frame
            current_file = QtCore.QString(process_file_batch.loc[i].Filename)
            self.logger.info("Batch processing file # {}: {}".format(i, current_file))

            if process_file_batch.loc[i, "Status"] == 2:
                # in case the status is 2: this file has been processed already and the results was OK. Skip it
                self.logger.info(
                    "Already successfully processed file {}. Skipping.".format(current_file))
                continue
            elif process_file_batch.loc[i, "Status"] > 2:
                self.logger.info(
                    "Already processed file {} without success. Trying again.".format(current_file))

            with Timer("{}".format(current_file)) as proctime:

                # set the status of the current file to 1 (processing) and update the table
                # TODO: updating the table of the batchprocess is now slow because all lines need to be replotted
                # Take the algorithm from PipeFitter to only update only one row
                process_file_batch.loc[i, "Status"] = 1
                self.batchProcessDlg.updateTable()

                # import the STL file
                self.importSTLFile(str(current_file))

                # align the pipe only if it was not already done so (then it has the _aligned part in the filename)
                if not bool(re.search("_aligned", current_file)):
                    self.clean_and_position_the_pipe()

                if not self.stop_batch_processing:
                    # only slice the pipe is the stop_batch_processing flag is not set
                    self.logger.debug("Slice the pipe")
                    # slice the pipe on the given slice positions
                    self.SlicePipe()

                    # export the HDF5 data file
                    # For executables generated with py2exe, hdf5 can not be used!
                    # self.show_message_in_status_bar("Writing to HDF5 file {}".format(self.exportfile))
                    # self.pipe.write_slice_file(self.exportfile)

                    # export the excel file
                    if self.pars_general.auto_export_xls.value():
                        self.show_message_in_status_bar(
                            "Writing slice data to {}".format(self.exportXlsfile))
                        self.pipe.write_slice_file(self.exportXlsfile)

                else:
                    # the stop_batch_processing flag has been set. Get out of the batch loop
                    break

            self.show_message_in_status_bar(
                "Processed {} in {:.1f} s".format(current_file, proctime.secs), timer=5000)

            # open the plot dialog and create some plots
            if self.pars_general.auto_export_png.value() > 0:
                self.slicePlotDialog = SlicePlotDlg(self.pipe, self.showSlicePlot)
                self.slicePlotDialog.update_logger(self.logger.level)

                self.logger.info(
                    "Auto export png : {}".format(self.pars_general.auto_export_png.value()))
                for cnt, ext in enumerate(self.pipe.export_extension_list):
                    self.slicePlotDialog.update_figure(plot_number=cnt)
                    image_name = self.export_image_base + ext + ".png"
                    self.logger.info("Saving image {}".format(image_name))
                    self.slicePlotDialog.save_figure_batch(image_name)

            # set the status of the current file to 2 (done)
            self.pipe.get_geometric_properies()
            message = self.quality_check()
            process_file_batch.loc[i, "Quality"] = message
            if message == "OK":
                # status 2 means that the file has been processed and all is ok
                process_file_batch.loc[i, "Status"] = 2
            else:
                # status 3 means that the file has been processed and something went wrong
                process_file_batch.loc[i, "Status"] = 3
            process_file_batch.loc[i, "Timer"] = "{:6.0f} s".format(proctime.secs)
            self.logger.debug("Time set to {}".format(process_file_batch.loc[i].Timer))

            # put the statistics of the current check slice into the proess_file_batch data frame so we can show
            # the values in the batch table
            try:
                stat_df = self.pipe.slice_stat_df.ix[self.pipe.check_slice_name]
                process_file_batch.loc[i, "Theta"] = "{:.1f}".format(stat_df.theta)
                process_file_batch.loc[i, "Rmean"] = "{:.1f}".format(stat_df.Rmean)
                process_file_batch.loc[i, "ID"] = "{:.1f}".format(stat_df.IDmean)
                process_file_batch.loc[i, "OOR"] = "{:.1f}".format(stat_df.OOR)
            except AttributeError:
                self.logger.warning(
                    "something went wrong with putting the statistics into the table.")

            self.batchProcessDlg.updateTable()

        # finish with one last table update
        self.batchProcessDlg.updateTable()

        self.show_message_in_status_bar("Ready", timer=5000)
        self.logger.info("Done with the batch")

        # when leaving, reactivate the execute button
        self.batchProcessDlg.buttonExecute.setEnabled(True)
        self.batchProcessDlg.buttonScanDir.setEnabled(True)

    def make_ovality_targets(self):

        # remove old targets (if any)
        for actor in self.ovality_target_actors:
            self.renderer.RemoveActor(actor)

        for actor in self.ovality_target_text_actors:
            self.renderer.RemoveActor(actor)

        self.ovality_target_actors = list()

        # Obtain the settings from the parameter tree
        treesection = self.pars.Parameters.names["Ellipse fitting"]

        self.pipe.ovality_targets = list()

        startx = treesection.make_start_x.value()
        starty = treesection.make_start_y.value()
        startz = treesection.make_start_z.value()
        stepx = treesection.make_step_x.value()
        stepy = treesection.make_step_y.value()
        stepz = treesection.make_step_z.value()
        n = treesection.make_n.value()

        for i in range(n + 1):
            position = (startx + i * stepx,
                        starty + i * stepy,
                        startz + i * stepz)

            foundActor = create_sphere_actor(position, 20, [1, 1, 1, 1])
            self.renderer.AddActor(foundActor)
            self.ovality_target_actors.append(foundActor)

            # Add number / text to targets in GUI, note that the number is the position in the array + 1
            textActor = create_text_billboard_actor(position, "Reference {}".format(i + 1),
                                                    self.renderer, scale=50)
            self.renderer.AddActor(textActor)
            self.ovality_target_text_actors.append(textActor)

            self.pipe.ovality_targets.append(position)

        self.renderer.Render()  # update the window
        self.vtkWidget.Render()

    def find_ovality_targets(self):

        # remove old targets (if any)
        for actor in self.ovality_target_actors:
            self.renderer.RemoveActor(actor)

        for actor in self.ovality_target_text_actors:
            self.renderer.RemoveActor(actor)

        self.ovality_target_actors = list()

        # Obtain the settings from the parameter tree
        treesection = self.pars.Parameters.names["Ellipse fitting"]

        self.pipe.find_ovality_targets(dist_first=treesection.dist1.value(),
                                       dist_second=treesection.dist2.value(),
                                       dist_third=treesection.dist3.value(),
                                       axis_alignment=treesection.alignment.value(),
                                       progres_function=self.update_progress)

        self.hide_progress_bar()

        for i, position in enumerate(self.pipe.ovality_targets):
            foundActor = create_sphere_actor(position, 20, [1, 1, 1, 1])
            self.renderer.AddActor(foundActor)
            self.ovality_target_actors.append(foundActor)

            # Add number / text to targets in GUI, note that the number is the position in the array + 1
            textActor = create_text_billboard_actor(position, "Reference {}".format(i + 1),
                                                    self.renderer, scale=50)
            self.renderer.AddActor(textActor)
            self.ovality_target_text_actors.append(textActor)

        self.renderer.Render()  # update the window
        self.vtkWidget.Render()

        # update tree-view
        # Obtain the settings from the parameter tree
        treesection.first_target.setValue(1)
        treesection.last_target.setValue(len(self.pipe.ovality_targets))

    def calc_ovality(self):

        # remove old targets (if any)
        for actor in self.ovality_target_plane_actors:
            self.renderer.RemoveActor(actor)

        self.ovality_target_plane_actors = list()

        # Obtain the settings from the parameter tree
        treesection = self.pars.Parameters.names["Ellipse fitting"]

        try:
            initial_guess = gu.string_to_3darray(treesection.initial_guess.value())
        except:
            msg = "The provided initial guess for normal vector is not a valid vector, should be something like 1,0,0 but it " + str(
                treesection.initial_guess.value())
            QtGui.QMessageBox.warning(self, "Typo?", msg)

        # Loop over the targets
        for i, position in enumerate(self.pipe.ovality_targets):

            if i + 1 < treesection.first_target.value():
                continue
            if i + 1 > treesection.last_target.value():
                continue

            try:
                normal = self.pipe.calc_tangent_plane_at(position,
                                                         tolerance_deg=treesection.tolerance_deg.value(),
                                                         max_iter=treesection.max_iter.value(),
                                                         relaxation=treesection.relaxation.value(),
                                                         initial_guess=initial_guess,
                                                         feedbackfunction=self.show_message_in_status_bar)
            except pm.MaxNumberOfIterationsExceeded as error:
                msg = "No converged solution found for reference target {}, change slice-alignment properties or skip this target".format(
                    i + 1)
                self.logger.error(msg)
                QtGui.QMessageBox.warning(self, "No convergence", msg)
                break

            # create plane
            plane_actor = create_plane_actor(position, normal, 1000, opacity=0.4)
            self.renderer.AddActor(plane_actor)
            self.ovality_target_plane_actors.append(plane_actor)

            # retrieve slice data
            position2d, data2d, center, side, up = self.pipe.get_slice_data(position, normal)

            fig = plt.figure()
            treesection_pipe = self.pars.Parameters.names["Pipe info"]
            d_nominal = treesection_pipe.nominal_diameter.value()

            tmp = self.pipe.ovality_targets[i]
            fig_title = "Slice for target {} at x={:.1f} y={:.1f} z={:.1f} \nNominal diameter = {}mm".format(
                i + 1, tmp[0], tmp[1], tmp[2], d_nominal)

            # temp is not used
            temp, temp, cx, cy, filename = ovalf.ellipse_make_plot(data2d,
                                                                   nominal_diamter=d_nominal,
                                                                   outdir=self.outdir,
                                                                   fname="ellipse_target" + str(
                                                                       i + 1),
                                                                   fig_title=fig_title)

            # calculate the center of the fitted oval in 3D
            center3d = gu.point2d_to_3d(center, side, up, [cx, cy])

            # write some more data to the info file (created by ovalf.eliipse_make_plot)
            with open(filename, 'a') as f:
                f.write("\n=== Data in 3D geometry ===\n")
                f.write(
                    "   note that if the pipe has been aligned then these coordinates are different than the\n")
                f.write("   coordinates in the .stl file\n\n")
                f.write("Reference point = {}\n".format(position))
                f.write("Plane normal = {}\n".format(normal))
                f.write("Fitted pipe center = {}\n".format(center3d))
                f.write(
                    "\n== 2D to 3D conversion == \n   [glob_x,glob_y,glob_z] = center + local_x*x-unit + local_y*y-unit\n")
                f.write(
                    "   with local_x and local_y the coordinates as written to _coordinates.txt file\n")
                f.write("   and gx,gy,gz the 3d coordinates\n\n")
                f.write("Center for 2d coordinates = {}\n".format(center))
                f.write("X unit-vector for 2d coordinates = {}\n".format(side))
                f.write("Y unit-vector for 2d coordinates = {}\n".format(up))

            foundActor = create_sphere_actor(center3d, 20, [0, 0, 1, 1])
            self.renderer.AddActor(foundActor)
            self.ovality_target_actors.append(foundActor)

            self.update_progress(i, len(self.pipe.ovality_targets))

        self.hide_progress_bar()
        self.renderer.Render()
        self.vtkWidget.Render()

        plt.show()

    def __get_clip_plane_properties(self):
        treesection = self.pars.Parameters.names["Remove floor"]
        x = treesection.x_plane.value()
        y = treesection.y_plane.value()
        z = treesection.z_plane.value()
        nx = treesection.nx_plane.value()
        ny = treesection.ny_plane.value()
        nz = treesection.nz_plane.value()

        position = (x, y, z)
        normal = (nx, ny, nz)

        return position, normal

    def update_clip_plane(self):

        position, normal = self.__get_clip_plane_properties()

        if self.cut_plane:
            self.renderer.RemoveActor(self.cut_plane)
        if self.cut_plane_arrow:
            self.renderer.RemoveActor(self.cut_plane_arrow)

        self.cut_plane = create_plane_actor(position, normal, 100000, color=[1, 0, 0], opacity=0.5)
        self.renderer.AddActor(self.cut_plane)

        endPoint = [position[i] + 1000 ** normal[i] for i in range(3)]

        self.cut_plane_arrow = create_arrow_actor(position, endPoint)
        self.renderer.AddActor(self.cut_plane_arrow)

        self.renderer.Render()
        self.vtkWidget.Render()

    def remove_model_section_below_clip_plane(self):
        position, normal = self.__get_clip_plane_properties()

        self.pipe.remove_model_section_below_clip_plane(position, normal)
        self.clearVTKFrame()
        self.updateVtkFrame()


def create_arrow_actor(startPoint, endPoint, length=1000):
    """
    Creates an arrow actor from startPoint towards endPointh with
    length of "length"

    :param startPoint:
    :param endPoint:
    :param length:
    :return: vtk Actor
    """

    # Copied from http://www.vtk.org/Wiki/VTK/Examples/Python/GeometricObjects/Display/OrientedArrow
    arrowSource = vtk.vtkArrowSource()
    # Compute a basis
    normalizedX = [0 for i in range(3)]
    normalizedY = [0 for i in range(3)]
    normalizedZ = [0 for i in range(3)]

    # The X axis is a vector from start to end
    math = vtk.vtkMath()
    math.Subtract(endPoint, startPoint, normalizedX)
    math.Normalize(normalizedX)

    # The Z axis is an arbitrary vector cross X
    arbitrary = [0 for i in range(3)]
    arbitrary[0] = random.uniform(-10, 10)
    arbitrary[1] = random.uniform(-10, 10)
    arbitrary[2] = random.uniform(-10, 10)
    math.Cross(normalizedX, arbitrary, normalizedZ)
    math.Normalize(normalizedZ)

    # The Y axis is Z cross X
    math.Cross(normalizedZ, normalizedX, normalizedY)
    matrix = vtk.vtkMatrix4x4()

    # Create the direction cosine matrix
    matrix.Identity()
    for i in range(3):
        matrix.SetElement(i, 0, normalizedX[i])
        matrix.SetElement(i, 1, normalizedY[i])
        matrix.SetElement(i, 2, normalizedZ[i])

    # Apply the transforms
    transform = vtk.vtkTransform()
    transform.Translate(startPoint)
    transform.Concatenate(matrix)
    transform.Scale(length, length, length)

    # Transform the polydata
    transformPD = vtk.vtkTransformPolyDataFilter()
    transformPD.SetTransform(transform)
    transformPD.SetInputConnection(arrowSource.GetOutputPort())

    # Create a mapper and actor for the arrow
    mapper = vtk.vtkPolyDataMapper()
    actor = vtk.vtkActor()

    mapper.SetInputConnection(transformPD.GetOutputPort())
    actor.SetMapper(mapper)

    return actor


def create_text_billboard_actor(position, text, renderer, scale=0.2):
    atext = vtk.vtkVectorText()
    atext.SetText(text)
    textMapper = vtk.vtkPolyDataMapper()
    textMapper.SetInputConnection(atext.GetOutputPort())
    textActor = vtk.vtkFollower()
    textActor.SetMapper(textMapper)
    textActor.SetScale(scale, scale, scale)
    textActor.AddPosition(position)

    # bind the text to the active camera
    textActor.SetCamera(renderer.GetActiveCamera())

    return textActor


def create_plane_actor(position, normal, size, color=[1, 1, 1], opacity=1.0):
    """
    Creates a plane actor with given size, position and normal.
    Returns the actor
    :type opacity: float
    :param position:
    :param normal:
    :param size:
    :param color:
    :param opacity:
    :return:
    """
    plane = vtk.vtkPlaneSource()

    # first make a plane with the right size (a standard plane has size 1)
    plane.SetCenter(0, 0, 0)
    plane.SetPoint1(size, 0, 0)
    plane.SetPoint2(0, size, 0)

    # then move it to the required position
    plane.SetCenter(position)
    plane.SetNormal(normal)
    plane.Update()

    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(plane.GetOutput())  # VTK <=5
    else:
        mapper.SetInputConnection(plane.GetOutputPort())

    plane_actor = vtk.vtkActor()
    plane_actor.GetProperty().SetOpacity(opacity)
    plane_actor.GetProperty().SetColor(color)
    plane_actor.SetMapper(mapper)

    return plane_actor


# @profile
def create_sphere_actor(pos, radius, color):
    """
    A routine to create an actor for a sphere
    :param pos: Position of the sphere
    :param radius: Radius of the sphere
    :param color: color of the sphere (a tuple of float, eithe 3 of 4, only the first 3 a picked
    :return:
    """
    sphere = vtk.vtkSphereSource()
    sphere.SetCenter(pos)
    sphere.SetRadius(radius)
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(sphere.GetOutput())
    else:
        mapper.SetInputConnection(sphere.GetOutputPort())

    # actor
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color[:3])

    return actor


# @profile
def create_surface_actor(data, color, opacity, show_mesh=False):
    mapper = vtk.vtkPolyDataMapper()
    if vtk.VTK_MAJOR_VERSION <= 5:
        mapper.SetInput(data.GetOutput())
    else:
        mapper.SetInputConnection(data.GetOutputPort())
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    actor.GetProperty().SetColor(color[:3])
    actor.GetProperty().SetOpacity(opacity)
    if show_mesh:
        actor.GetProperty().SetRepresentationToWireframe()
    return actor


def main():
    # get the default configuration files
    settings = set_and_write_default_values()

    # Create a GL View widget to display data
    app = QtGui.QApplication(sys.argv)
    app.setOrganizationName(settings["general"]["organisation_name"])
    app.setOrganizationDomain(settings["general"]["organisation_domain"])
    app.setApplicationName(settings["general"]["application_name"])
    app.setWindowIcon(QtGui.QIcon(":pipe-128.png"))
    win = PipeSlicerMain(logfile)
    first_title = "{} {}".format(settings["general"]["application_name"],
                                 get_clean_version(__version__))
    win.setWindowTitle(first_title)
    win.resize(1100, 700)
    win.show()

    app.exec_()


if __name__ == "__main__":
    main()
