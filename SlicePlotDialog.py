from __future__ import division
from builtins import str
from past.utils import old_div
__author__ = 'eelcovv'
# this dialog will plot the slices

import logging

from PyQt4 import QtGui, QtCore

import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import re
from numpy import pi


class SlicePlotDlg(QtGui.QDialog):
    def __init__(self, pipe, show_slice_plot, parent=None):
        super(SlicePlotDlg, self).__init__(parent)

        self.pipe = pipe

        self.show_slice_plot = show_slice_plot

        self.logger = logging.getLogger(__name__)

        # a close button
        self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        # a save button to save the image
        self.buttonSave = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save)

        # a save button to save the image
        self.buttonEdit = QtGui.QPushButton("Edit Axes...")

        # a widget to contain the image
        self.figure = plt.figure()
        self.canvas = FigureCanvas(self.figure)

        # a tool bar which we don't show, but we only take the save image functionality
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.toolbar.hide()

        # a spinbox to ask for the slice number to plot
        labelSpinbox = QtGui.QLabel(self.tr("Show slice number (0=all):"))
        labelSpinbox.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.spinBox = QtGui.QSpinBox()

        # the initial value of the spin box (0 means plot all the slices!)
        self.spinBox.setValue(0)

        # set the limits of the spin box based on the number of slices contained in the dataframe
        self.spinBox.setMinimum(0)
        if self.pipe.slice_dataframes is None:
            self.spinBox.setMaximum(0)
        else:
            max = len(self.pipe.slice_dataframes.groupby(level=0))
            self.spinBox.setMaximum(max)

        # a combobox to ask for the plot type
        labelCombo = QtGui.QLabel(self.tr("Plot Type:"))
        labelCombo.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.plotTypeComboBox = QtGui.QComboBox()
        self.plotTypeComboBox.addItem("Radius Polar")
        self.plotTypeComboBox.addItem("Radius Linear")
        self.plotTypeComboBox.addItem("Thickness Linear")

        # initialise the axis with a polar plot
        self.axis = None
        self.lines = []
        self.line_colors = []
        self.init_colors()
        self.init_plot()

        # a verical layout to contain the spinbox, combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()
        buttonLayout.addWidget(labelSpinbox)
        buttonLayout.addWidget(self.spinBox)
        buttonLayout.addWidget(labelCombo)
        buttonLayout.addWidget(self.plotTypeComboBox)
        buttonLayout.addWidget(self.buttonEdit)

        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonSave)
        buttonLayout.addWidget(self.buttonBox)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.canvas)
        # graphLayout.addWidget(self.toolbar)

        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        # layout.addWidget(self.canvas,10)
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        # add some signals to the buttons and spinboxes
        self.connect(self.buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))
        self.buttonSave.clicked.connect(self.save_figure)

        self.connect(self.spinBox, QtCore.SIGNAL("valueChanged(int)"),
                     self.update_figure)

        self.connect(self.plotTypeComboBox, QtCore.SIGNAL("currentIndexChanged(int)"),
                     self.reset_axis)

        self.connect(self.buttonEdit, QtCore.SIGNAL("clicked()"),
                     self.edit_axis)

        settings = QtCore.QSettings()

        size = settings.value("SlicePlotDlg/Size",
                              QtCore.QVariant(QtCore.QSize(800, 500))).toSize()
        self.resize(size)
        position = settings.value("SlicePlotDlg/Position",
                                  QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
        self.move(position)

        self.setWindowTitle("Pipe Slice Plot")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def edit_axis(self):
        self.toolbar.edit_parameters()

    def save_figure(self):
        self.toolbar.save_figure()

    def reset_axis(self, plot_number=None):
        # reset the axis. In case you want to do this from a batch (without the dialog, pass the init_plot_number
        # as an argument
        self.clear_lines()
        self.figure.clear()
        if plot_number is not None:
            self.init_plot(plot_number)
        elif str(self.plotTypeComboBox.currentText()) == "Radius Polar":
            self.init_plot(0)
        elif str(self.plotTypeComboBox.currentText()) == "Radius Linear":
            self.init_plot(1)
        else:
            self.init_plot(2)
        self.canvas.draw()
        self.update_figure(plot_number=plot_number)

    def init_plot(self, plot_type=0):
        # initialise the current plot area. Called every time we change plot type with the combo box
        self.axis = []
        if plot_type == 0:
            # set up the plot for polar coordinates
            gs = gridspec.GridSpec(1, 1)
            # create some extra space
            gs.update(bottom=0.1)
            self.bbox_to_anchor_pos = (1.3, 1.1)
            self.axis.append(self.figure.add_subplot(gs[:], polar=True))
            self.axis[-1].set_theta_zero_location("N")
            self.axis[-1].set_theta_direction(-1)
            self.axis[-1].set_xlabel("")
            self.axis[-1].set_ylabel("")
        else:
            # set up a normal linear plot
            gs = None
            if plot_type == 1:
                gs = gridspec.GridSpec(2, 1)
                self.bbox_to_anchor_pos = (1.3, 2.4)
            else:
                gs = gridspec.GridSpec(1, 1)
                self.bbox_to_anchor_pos = (1.3, 1.1)
            # create some extra space
            gs.update(right=0.8)

            self.axis.append(self.figure.add_subplot(gs[0, 0]))
            self.axis[-1].set_xlabel("Phi [$^o$]")
            self.axis[-1].set_xlim((0, 2 * pi))
            self.axis[-1].set_xticks([0, old_div(pi, 2), pi, 3 * pi / 2, 2 * pi])
            self.axis[-1].set_xticklabels([0, 90, 180, 270, 360])

            if plot_type == 1:
                self.axis.append(self.figure.add_subplot(gs[1, 0], sharex=self.axis[0]))
                self.axis[0].set_xlabel("")
                self.axis[-1].set_xlabel("Phi [$^o$]")
                plt.setp(self.axis[0].get_xticklabels(), visible=False)
                plt.setp(self.axis[0].get_xlabel(), visible=False)
                # we are plottig the radius
                self.axis[-1].set_ylabel("Radius in [mm]")
                self.axis[0].set_ylabel("Radius out [mm]")
            else:
                # we are plottig the thickness
                self.axis[-1].set_ylabel("Thickness [mm]")

    def clear_lines(self):
        # remove the lines from the current plot. We need to store all the lines in a list so we can explicitly
        #  remove it line by line. After that the lines can be send to the garbage collection by setting it to []
        for i, line in enumerate(self.lines):
            for ax in self.axis:
                try:
                    ax.lines.remove(line)
                except ValueError:
                    # I don't care which axis this line belonged to. Just remove it in case 'try' allows
                    pass
        self.lines = []

    def update_figure(self, index_slice_to_show=0, plot_number=None):
        # set the plot
        if self.pipe.slice_dataframes is not None:

            self.clear_lines()

            if plot_number is not None:
                # the plot number argument is not none, so the routing is called from outside. Copy the value to
                # the combobox
                self.plotTypeComboBox.setCurrentIndex(plot_number)

            index = 0
            line_color = self.get_line_color(1)
            index_slice_to_show = self.spinBox.value()
            self.logger.debug("updating the graph to show slice {} with plot_number {}".format(
                index_slice_to_show, plot_number))
            phandles = []

            # Loop over all the slice and create a plot
            for key, df in self.pipe.slice_dataframes.groupby(level=0):
                index += 1

                if index_slice_to_show > 0 and index_slice_to_show != index:
                    continue

                label = "N.A."
                match = re.search("_p[0]*(\d+)", key)
                if bool(match):
                    label = match.group(1)
                self.logger.debug("plotting slice {} with label {}".format(key, label))

                if index_slice_to_show == 0:
                    # only update the line color if all lines are plotted
                    line_color = self.get_line_color(index)
                line_solid = "-"
                line_dashed = "--"

                if self.plotTypeComboBox.currentIndex() < 2:

                    try:
                        ph, = self.axis[-1].plot(df.phi, df.radius, line_solid, color=line_color,
                                                 linewidth=1, markersize=1, label="{:5>}".format(label))
                        self.logger.debug("plotted first {}".format(plot_number))
                    except AttributeError:
                        self.logger.debug("Could not plot the inner radius. Skipping")
                    try:
                        ph2, = self.axis[self.plotTypeComboBox.currentIndex() - 1].plot(
                            df.phi, df.radius_out, line_dashed, color=line_color, linewidth=1, markersize=1)
                        self.logger.debug("plotted second {}".format(plot_number))
                        self.lines.append(ph2)
                    except AttributeError:
                        self.logger.debug("Could not plot the outer radius. Probably no outer wall present.")
                else:

                    try:
                        ph, = self.axis[0].plot(df.phi, df.Dwall, line_solid, color=line_color,
                                                linewidth=1, markersize=1, label="{:5>}".format(label))
                        #line_color = 'k'
                        #ph2, = self.axis[0].plot(df.phi, df.Dwall_projection, line_solid, color=line_color,
                        #                        linewidth=1, markersize=1, label="{:5>}".format('delta_radius'))
                        self.logger.debug("Plotted the wall thickness {}".format(plot_number))
                    except AttributeError:
                        self.logger.debug("Could not plot the thickness of the wall. Probably no outer wall present")

                try:
                    self.lines.append(ph)
                    phandles.append(ph)
                except UnboundLocalError:
                    # no correct plot was created so do not add the handle
                    pass

            plt.legend(handles=phandles, title='X-position', loc=1, bbox_to_anchor=self.bbox_to_anchor_pos)
            self.canvas.draw()

    def save_figure_batch(self, filename):
        self.logger.debug("Saving figure to {}".format(filename))
        plt.figure(self.figure.number)
        try:
            plt.savefig(filename, bbox_inches='tight', pad_inches=.1)
        except IOError:
            self.logger.warning("Could not save figure {}".format(filename))

    def get_line_color(self, index):
        # get the color of a line with the current index, where the index is looped over the color range
        return self.line_colors[(index - 1) % (len(self.line_colors) - 1)]

    def init_colors(self):
        # a long list of color names to be able to distinguis many profiles
        self.line_colors = [
            "red",
            "green",
            "blue",
            "cyan",
            "yellow",
            "black",
            "magenta",
            "firebrick",
            "purple",
            "darkgoldenrod",
            "gray",
            "burlywood",
            "chartreuse",
            "brown",
            "darkcyan",
            "deeppink",
            "gold",
            "orange",
            "greenyellow",
            "midnightblue",
            "olivedrab",
        ]

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        settings.setValue("SlicePlotDlg/Size", QtCore.QVariant(self.size()))
        settings.setValue("SlicePlotDlg/Position",
                          QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if self.show_slice_plot.isChecked():
            self.show_slice_plot.setChecked(False)
            self.show_slice_plot.setEnabled(True)
