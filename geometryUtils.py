__author__ = 'Ruben de Bruin'
"""
Some geometric helper functions that do not seem to be available in numpy
"""

import numpy as np


def normalize(vec):
    return vec / np.linalg.norm(vec)


def vec_component(vec, onto):
    """
    Returns the vector component of vec onto onto
    :param vec:
    :param onto:
    :return:
    """

    return np.dot(vec, normalize(onto))


def vec_projection(vec, onto):
    """
    Returns the projection of vec onto onto
    :param vec:
    :param onto:
    :return:
    """

    return vec_component(vec, onto) * normalize(onto)


def cos_angle_between(a, b):
    """
    Returns the cosine of the angle between two vectors
    :param a:
    :param b:
    :return:
    """
    return np.dot(normalize(a), normalize(b))


def angle_between(a, b):
    """
    Returns the angle between two vectors in radians
    :param a:
    :param b:
    :return:
    """

    return np.arccos(cos_angle_between(a, b))


def projection_onto_2d_plane(vec, center, dira, dirb):
    """
    Returns the projection of point vec onto plane with
    center at "center" and directions dira and dirb

    such that

    vec = center + r[0] * dira + r[1]*dirb

    dira and dirb need to be perpendicular unit vectors

    :param vec:
    :param center:
    :param dira:
    :param dirb:
    :return:
    """

    vec = vec - center
    r = [0, 0]
    r[0] = vec_component(vec, dira)
    r[1] = vec_component(vec, dirb)

    return np.array(r)

def point2d_to_3d(center,side,up,coordinate):
    return center + coordinate[0]*side + coordinate[1]*up


def string_to_3darray(s):
    """
    Converts a string to a 3d array
    example string_to_3darray("0, -4, 7")
    :param s: comma separated string containing 3 floats
    :return: numpy array
    """
    blks = s.split(',')
    return np.array([float(blks[0]), float(blks[1]), float(blks[2])])
