__author__ = 'Ruben de Bruin'
"""
Python implementation of algebraic ellipse fitting
ref: http://www.math.muni.cz/~zelinka/cjh/svd.pdf
"""

import numpy as np
import matplotlib.pyplot as plt


def ellipse_fit_to_data(data):
    """
    Fit ellipse though points projected onto plane
    Procedure taken from http://www.math.muni.cz/~zelinka/cjh/svd.pdf
    Fits an ellipse by minimizing the "algebraic distance"
    to given points xloc, yloc in the least squares sense
    x'A x + b'x + c = 0
     is the center a,b the main axes, alpha angle between a and x-axis
    :param data: x and y data
    :return:  C, a, b, alpha
    """
    x = data[:, 0]
    y = data[:, 1]

    n = len(x)
    ones = np.ones([1, n])

    B = np.empty([6, n])

    B[0, :] = x ** 2
    B[1, :] = x * y
    B[2, :] = y ** 2
    B[3, :] = x
    B[4, :] = y
    B[5, :] = ones

    # B = np.matrix(B)

    V, s, U = np.linalg.svd(B)

    u = V[:, 5]
    A = np.array([[u[0], u[1] / 2], [u[1] / 2, u[2]]])
    bb = np.array([[u[3]], [u[4]]])
    c = u[5]

    D, Q = np.linalg.eig(A)

    alpha = np.arctan2(Q[1, 0], Q[0, 0])

    bs = np.dot(Q.transpose(), bb)

    _A = np.matrix([[-2 * D[0], 0], [0, -2 * D[1]]])

    zs = np.linalg.solve(_A, bs)
    z = np.dot(Q, zs)

    h = np.dot(-bs.transpose(), zs) / 2 - c

    a = (h / D[0]) ** 0.5
    b = (h / D[1]) ** 0.5

    return z, a[0][0], b[0][0], alpha


def ellipse_calcxy(C, a, b, alpha, step=0.02):
    """
    Prepare plotting data (x,y) based on center, a,b and alpha
    as calculated by the fit function ellipse_fit_to_data
    :param C: Center of the ellipse
    :param a: a
    :param b: b
    :param alpha: angle
    :param step: grid resulution (in radians)
    :return: x and y to be plotted
    """

    # transformation (rotation) matrix
    s = np.sin(alpha)
    c = np.cos(alpha)
    Q = np.matrix([[c, -s], [s, c]])

    # the ellipse
    theta = np.arange(0, 2 * np.pi, step)
    xx = a * np.cos(theta)
    yy = b * np.sin(theta)
    u = np.matrix([xx.transpose(), yy.transpose()])

    # rotate the ellipse
    u = Q * u

    # and shape back to something use-full
    x = np.array(u[0, :])
    x = x[0]
    y = np.array(u[1, :])
    y = y[0]

    # and add the center
    x = x + C[0]
    y = y + C[1]

    return x, y


def ellipse_make_plot(data, nominal_diamter = -1, outdir = None, fname = None, fig_title=None):
    """
    Creates a plot
    :param data:
    :return:
    v,h : vertical radius, horizontal radius
    cx,cy : center x and y
    """
    plt.plot(data[:, 0], data[:, 1], 'b.', label="input data")

    C, a, b, alpha = ellipse_fit_to_data(data)
    x, y = ellipse_calcxy(C, a, b, alpha)

    ovality = 100 * 2 * abs(a - b) / nominal_diamter

    plt.plot(x, y, 'g-', label="fitted ellipse, ovality = %0.2f%%" % ovality)

    # draw lines a and b
    ax = [C[0] - a * np.cos(alpha), C[0] + a * np.cos(alpha)]
    ay = [C[1] - a * np.sin(alpha), C[1] + a * np.sin(alpha)]

    beta = alpha + np.pi / 2
    bx = [C[0] - b * np.cos(beta), C[0] + b * np.cos(beta)]
    by = [C[1] - b * np.sin(beta), C[1] + b * np.sin(beta)]

    # find the largest radius

    if b > a:
        vx = ax
        vy = ay
        v = a
        hx = bx
        hy = by
        h = b
        angle = alpha
    else:
        vx = bx
        vy = by
        v = b
        hx = ax
        hy = ay
        h = a
        angle = beta

    # No need to calculate the angle, we do not have a true vertical anyways
    # dispangle = np.pi/2 - angle
    # dispangle = divmod(dispangle,np.pi)

    plt.plot(hx, hy, 'r-', label="maximum radius = %.2f" % h)
    plt.plot(vx, vy, 'k-', label="minimum radius = %.2f" % v)

    plt.axis('equal')
    plt.legend(loc=10)

    if fig_title:
        plt.title(fig_title)

    textfile = fname # default value

    if fname:
        textfile = outdir + "\\" + fname + '_coordinates.txt'
        with open(textfile,'w') as f:
            for i in range(len(data)):
                f.write("{},{}\n".format(data[i, 0]-C[0][0], data[i, 1]-C[1][0]))

        textfile = outdir + "\\" + fname + '_info.txt'
        with open(textfile,'w') as f:
            f.write("=== 2D fit data ===\n")
            f.write("Nominal diamter [mm] = {}\n".format(nominal_diamter))
            f.write("a = {}\n".format(a))
            f.write("b = {}\n".format(b))
            f.write("alpha = {}\n".format(alpha))
            f.write("cx = {}\n".format(C[0][0]))
            f.write("cy = {}\n".format(C[1][0]))
            f.write("ovality = {}\n".format(ovality))

        figfile = outdir + "\\" + fname + '.png'
        plt.savefig(figfile)


    return v, h, C[0][0],C[1][0], textfile
