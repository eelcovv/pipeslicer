from __future__ import division

from builtins import object
from builtins import str

__author__ = 'eelcovv'

import logging

import pyqtgraph as pg
from pyqtgraph.configfile import ParseError
from pyqtgraph.Qt import QtCore
import pyqtgraph.parametertree.parameterTypes as pTypes
from pyqtgraph.parametertree import Parameter


class SliceSettings(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(
            dict(name='X start', type='float', value=0.005, step=0.05, limits=(None, None),
                 siPrefix=True,
                 suffix='m'))
        self.addChild(
            dict(name='X end', type='float', value=.100, step=0.05, limits=(0.005, None),
                 siPrefix=True,
                 suffix='m'))
        self.addChild(
            dict(name='Length X Range', type='float', value=.095, step=0.05, limits=(0.0, None),
                 siPrefix=True,
                 suffix='m'))
        self.addChild(
            dict(name='Delta X', type='float', value=.005, step=0.01, limits=(0.001, None),
                 siPrefix=True,
                 suffix='m'))
        self.addChild(dict(name='Number of slices', type='int', value=20, step=1, limits=(1, None)))
        self.addChild(dict(name='Match COG inner/outer profile', type='bool', value=False,
                           tip="Shift the COG of outer profile on top of the COG of the inner profile. Officially not "
                               "allowed, but sometimes a solution when the inner and outer wall measurement are not "
                               "aligned"))
        self.addChild(dict(name="Check/Statistics Slice", type="group", children=
        [
            dict(name='Index', type='int', value=11, step=1, limits=(0, 19),
                 tip="The index of the slice used for quality check and statistics in the output of the batch"),
            dict(name='Position', type='float', value=60e-3, step=0.05, readonly=True,
                 siPrefix=True, suffix="m",
                 tip="Position belonging to the check and statistics slice"),
        ]))

        self.X_start = self.param('X start')
        self.X_end = self.param('X end')
        self.Lx = self.param('Length X Range')
        self.delta_x = self.param('Delta X')
        self.n_slices = self.param('Number of slices')

        self.align_profile_cogs = self.param('Match COG inner/outer profile')

        self.check_slice_index = self.param("Check/Statistics Slice").names["Index"]
        self.check_slice_position = self.param("Check/Statistics Slice").names["Position"]

        self.X_start.sigValueChanged.connect(self.X_start_Changed)
        self.X_end.sigValueChanged.connect(self.X_end_Changed)
        self.Lx.sigValueChanged.connect(self.Lx_Changed)
        self.n_slices.sigValueChanged.connect(self.n_slices_Changed)
        self.delta_x.sigValueChanged.connect(self.delta_x_Changed)

        self.check_slice_index.sigValueChanged.connect(self.check_slice_index_Changed)
        self.check_slice_index.sigValueChanged.connect(self.check_slice_index_Changed)

    def X_start_Changed(self):
        self.X_end.setLimits((self.X_start.value(), None))
        self.Lx.setValue(self.X_end.value() - self.X_start.value(), blockSignal=self.Lx_Changed)
        self.n_slices.setValue(int(round(self.Lx.value() / self.delta_x.value())) + 1,
                               blockSignal=self.n_slices_Changed)
        self.delta_x.setValue(self.Lx.value() / (max(self.n_slices.value() - 1, 1)),
                              blockSignal=self.delta_x_Changed)
        self.check_slice_index_Changed()

    def X_end_Changed(self):
        self.X_start.setLimits((None, self.X_end.value()))
        self.Lx.setValue(self.X_end.value() - self.X_start.value(), blockSignal=self.Lx_Changed)
        try:
            self.n_slices.setValue(int(round(self.Lx.value() / self.delta_x.value())) + 1,
                                   blockSignal=self.n_slices_Changed)
        except ZeroDivisionError:
            self.n_slices.setValue(1, blockSignal=self.n_slices_Changed)

        if self.n_slices.value() > 1:
            self.delta_x.setValue(self.Lx.value() / (self.n_slices.value() - 1),
                                  blockSignal=self.delta_x_Changed)

    def Lx_Changed(self):
        self.X_end.setValue(self.X_start.value() + self.Lx.value(), blockSignal=self.X_end_Changed)
        try:
            self.n_slices.setValue(int(round(self.Lx.value() / self.delta_x.value())) + 1,
                                   blockSignal=self.n_slices_Changed)
        except ZeroDivisionError:
            self.n_slices.setValue(1, blockSignal=self.n_slices_Changed)
        if self.n_slices.value() > 1:
            self.delta_x.setValue(self.Lx.value() / (self.n_slices.value() - 1),
                                  blockSignal=self.delta_x_Changed)

    def n_slices_Changed(self):
        self.Lx.setValue(self.delta_x.value() * (self.n_slices.value() - 1),
                         blockSignal=self.Lx_Changed)
        self.X_end.setValue(self.X_start.value() + self.Lx.value(), blockSignal=self.X_end_Changed)

        self.check_slice_index.setLimits((0, self.n_slices.value() - 1))

    def delta_x_Changed(self):
        self.Lx.setValue(self.delta_x.value() * (self.n_slices.value() - 1),
                         blockSignal=self.Lx_Changed)
        self.X_end.setValue(self.X_start.value() + self.Lx.value(),
                            blockSignal=self.X_end_Changed())
        self.check_slice_index_Changed()

    def check_slice_index_Changed(self):
        self.check_slice_position.setValue(
            self.delta_x.value() * self.check_slice_index.value() + self.X_start.value())


class PipeInfo(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild(dict(name='Nominal diameter', type='float', value=1, step=1, limits=(1, None),
                           siPrefix=True, suffix='mm'))

        self.nominal_diameter = self.param('Nominal diameter')


class EllipseFitting(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # Reference target finding

        self.addChild(dict(name="Reference target finding", type="group", children=
        [
            dict(name='Max proximity neighbour 1', type='float', value=20, step=5, limits=(5, None),
                 siPrefix=True,
                 suffix='mm'),
            dict(name='Max proximity neighbour 2', type='float', value=40, step=5, limits=(5, None),
                 siPrefix=True,
                 suffix='mm'),
            dict(name='Max proximity neighbour 3', type='float', value=60, step=5, limits=(5, None),
                 siPrefix=True,
                 suffix='mm'),
            dict(name='Minimum target point alignment (cos of angle)', type='float', value=0.95,
                 step=0.05,
                 limits=(0, 1))
        ]))

        self.dist1 = self.param("Reference target finding").names['Max proximity neighbour 1']
        self.dist2 = self.param("Reference target finding").names['Max proximity neighbour 2']
        self.dist3 = self.param("Reference target finding").names['Max proximity neighbour 3']
        self.alignment = self.param("Reference target finding").names[
            'Minimum target point alignment (cos of angle)']

        # Create your own tagets
        # Reference target finding

        self.addChild(dict(name="Create reference points", type="group", children=
        [
            dict(name='initial x', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='initial y', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='initial z', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='step x', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='step y', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='step z', type='float', value=0, step=5, siPrefix=True, suffix='mm'),
            dict(name='number of points', type='int', value=1),
            dict(name='Make', type='action'),
        ]))
        self.make_start_x = self.param("Create reference points").names['initial x']
        self.make_start_y = self.param("Create reference points").names['initial y']
        self.make_start_z = self.param("Create reference points").names['initial z']
        self.make_step_x = self.param("Create reference points").names['step x']
        self.make_step_y = self.param("Create reference points").names['step y']
        self.make_step_z = self.param("Create reference points").names['step z']
        self.make_n = self.param("Create reference points").names['number of points']

        # Which target to use

        self.addChild(dict(name="References to use", type="group", children=
        [
            dict(name='First', type='int', value=1, step=1, limits=(1, 100)),
            dict(name='Last', type='int', value=1, step=1, limits=(1, 100))
        ]))

        self.first_target = self.param("References to use").names['First']
        self.last_target = self.param("References to use").names['Last']

        # Plane fitting

        self.addChild(dict(name="Slice alignment", type="group", children=
        [
            dict(name='Initial guess', type='str', value="1, 0, 0",
                 tip="Initial guess for normal vector of plane, format x,y,z"),
            dict(name='Tolerance (degrees)', type='float', value=0.1, step=0.05, limits=(0, 1)),
            dict(name='Maximum number of iterations', type='int', value=400, step=100,
                 limits=(1, None)),
            dict(name='Relaxation of update in solver', type='float', value=0.1, step=0.05,
                 limits=(0.01, 1),
                 tip="1 means full update (no relaxation) 0 means no update at all. Lower is slower and more stable. 0 will not work")
        ]))

        self.initial_guess = self.param("Slice alignment").names['Initial guess']
        self.relaxation = self.param("Slice alignment").names['Relaxation of update in solver']
        self.tolerance_deg = self.param("Slice alignment").names['Tolerance (degrees)']
        self.max_iter = self.param("Slice alignment").names['Maximum number of iterations']


class GeneralSettings(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(
            dict(name='Units STL', type='list', values=dict(m=0, dm=1, cm=2, mm=3), value=3,
                 tip="The units as used in the STL file"))
        self.addChild(
            dict(name='Extension target file   ', type='str', value="",
                 tip="The name of the text file (.txt) containing the target point must be the same as "
                     "the STL file plus the extension given here"))
        self.addChild(
            dict(name='Verbosity Mode', type='list', values=dict(Warning=0, Info=1, Debug=2),
                 value=1,
                 tip="Controls the amount of info written to the log file/screen."))

        self.addChild(
            dict(name='Auto Export STL', type='bool', value=False,
                 tip="Automatically write the aligned pipe to a new STL file with the _aligned extension."))

        self.addChild(
            dict(name='Auto Export XLS', type='bool', value=True,
                 tip="Automatically write the slices to an excel file during batch processing."))

        self.addChild(
            dict(name='Auto Export PNG', type='int', value=1, limits=(0, 2),
                 tip="Automatically write the plots and screenshots of the slices. 0: No write; 1: Only write"
                     "the check slice; 2: write all slices"))

        self.extension_target_file = self.param('Extension target file   ')
        self.power_units = self.param('Units STL')
        self.verbosity_mode = self.param('Verbosity Mode')
        self.auto_export_stl = self.param('Auto Export STL')
        self.auto_export_xls = self.param('Auto Export XLS')
        self.auto_export_png = self.param('Auto Export PNG')


class NumericalSettings(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        self.addChild(
            dict(name='Clip below X-position', type='float', value=.0, step=.05, limits=(0, None),
                 siPrefix=True,
                 suffix='m',
                 tip="Clip the STL file below this x-position. Assumes that the pipe has been aligned "
                     "already. If this value is zero, nothing will be clipped"))
        self.addChild(
            dict(name='Clip above X-position', type='float', value=.150, step=.05, limits=(0, None),
                 siPrefix=True,
                 suffix='m',
                 tip="Clip the STL file above this x-position. Assumes that the pipe has been aligned "
                     "already. Affects the cutter filter"))
        self.addChild(
            dict(name='Clip outside radius', type='float', value=.250, step=.05, limits=(0, None),
                 siPrefix=True,
                 suffix='m',
                 tip="Clip the STL file outside this r-range. Assumes that the pipe has been aligned "
                     "already. Affects the cutter filter"))
        self.addChild(
            dict(name='Enlargement factor of Pre-Clip', type='float', value=1.3, step=.05,
                 limits=(1, 2),
                 tip="Carry out an extra pre alignment with a slightly larger clip box to improve alignment of stl"
                     "files with a lot of trash. Select Second Pre-algignment to activate "))
        self.addChild(
            dict(name='Second Pre Alignment', type='bool', value=True,
                 tip="Carry out the pre-alignment two times using an enlarged clip box given by the enlargement factor."
                     " Improves alignment of pipes  with a lot of part not belong to the pipe."))

        # the members of the group as they appear in the menu
        self.addChild(
            dict(name="Alignment", type="group", children=
            [
                dict(name='Reduction factor', type='float', value=0.05, step=.1, limits=(.01, 1),
                     tip="Factor of reduction of the alpha/beta space (1 is no reduction 0.1 is fast)"),
                dict(name='Tolerance', type='float', value=1, step=.1, limits=(0.01, None),
                     suffix='degrees', tip="Final resolution of alignment algorithm"),
                dict(name='Pick Largest Region', type='bool', value=True, default=True,
                     tip="Take the largerst region for the pipe. Overrules the max regions option. Only in case that"
                         "the pipe inner and outer wall are disconnected the max. regions may work better"),
                self.addChild(dict(name="Pre Alignment", type="group", children=
                [
                    dict(name='Number of samples', type='int', value=50, step=1, limits=(2, 100),
                         tip="Number of sample in alpha/beta space to use for alignment at the first itteration"),
                    dict(name='Max. Iterations', type='int', value=1, step=1, limits=(1, 100),
                         tip="Maximum number of iterations for the first iteration"),
                    dict(name='Maximum Regions', type='int', value=5, step=5, limits=(1, None),
                         tip="Maximum number of unconnected regions for pre alignment. Pick a larger value like 5."
                             " Is not used when 'Pick Largest Region' is True")
                ])),
                self.addChild(dict(name="Final Alignment", type="group", children=[
                    dict(name='Number of samples', type='int', value=25, step=1, limits=(2, 100),
                         tip="Number of sample in alpha/beta space to use for alignment"),
                    dict(name='Max. Iterations', type='int', value=20, step=1, limits=(1, 100),
                         tip="Maximum number of iterations for the next iterations"),
                    dict(name='Maximum Regions', type='int', value=1, step=1, limits=(1, None),
                         tip="Maximum number of unconnected regions in the final alignment. Keep small, preferably 1."
                             " Is not used when 'Pick Largest Region' is True")
                ]))
            ])
        )
        self.addChild(
            dict(name="Reference Point Search Criteria", type="group", children=
            [
                dict(name='Max Angle Ref Points', type='float', value=40, step=1, limits=(0, 90),
                     suffix="deg",
                     tip="Maximum angle between left-middle-right target points. 0 will switch of test. 1 is strict, 90"
                         "accepts all point"),
                dict(name='Max Pair Distance', type='float', value=0.015, step=0.001,
                     limits=(0.001, 1), siPrefix=True,
                     suffix='m', tip="Maximum allowed distance of two pairing targets."),
                dict(name='Exclude Near Distance', type='float', value=0.2, step=1, limits=(0, 1),
                     tip="Each point located between a pairs closer than this relative distance will be exclude. "
                         "Setting to 0 switches of this test. Larger values take out more points"),
                self.addChild(dict(name='Update Reference Point', type='action'))
            ])
        )

        self.addChild(
            dict(name="Slice Check Criteria", type="group", children=
            [
                dict(name='Max angle of slice', type='float', value=2.0, step=.5, limits=(0, 90),
                     suffix="deg",
                     tip="Maximum angle between normal vectors over a slice and the x-axis. "
                         "Ideally this angle is zero. A warning is raised if the angle gets larger"),
                dict(name='Min Bounding Box Size', type='float', value=0.7, step=0.1,
                     limits=(0.1, 1),
                     tip="Min fraction of the initial radius given by the clipping radius the bounding box is "
                         "allowed to be. For smaller bounding boxes probably something is wrong and a warning "
                         "is raised."),
            ])
        )

        self.clip_x_position_below = self.param('Clip below X-position')
        self.clip_x_position_above = self.param('Clip above X-position')
        self.clip_r_range = self.param('Clip outside radius')
        self.enlargement_factor = self.param('Enlargement factor of Pre-Clip')
        self.second_pre_alignment = self.param('Second Pre Alignment')

        self.max_slice_angle = self.param("Slice Check Criteria").names["Max angle of slice"]
        self.min_bounding_box = self.param("Slice Check Criteria").names["Min Bounding Box Size"]

        self.n_alpha_beta_1 = self.param("Alignment").names["Pre Alignment"].names[
            "Number of samples"]
        self.n_alpha_beta_2 = self.param("Alignment").names["Final Alignment"].names[
            "Number of samples"]

        self.max_connected_regions_1 = self.param("Alignment").names["Pre Alignment"].names[
            "Maximum Regions"]
        self.max_connected_regions_2 = self.param("Alignment").names["Final Alignment"].names[
            "Maximum Regions"]

        self.n_max_iters_1 = self.param("Alignment").names["Pre Alignment"].names["Max. Iterations"]
        self.n_max_iters_2 = self.param("Alignment").names["Final Alignment"].names[
            "Max. Iterations"]
        self.reduction_factor = self.param("Alignment").names['Reduction factor']
        self.tolerance = self.param("Alignment").names['Tolerance']

        self.pick_largest_region = self.param("Alignment").names['Pick Largest Region']

        self.max_angle_reference_points = self.param("Reference Point Search Criteria").names[
            'Max Angle Ref Points']
        self.max_pair_distance = self.param("Reference Point Search Criteria").names[
            'Max Pair Distance']
        self.exclude_near_distance = self.param("Reference Point Search Criteria").names[
            'Exclude Near Distance']

        self.update_reference_point = self.param("Reference Point Search Criteria").names[
            'Update Reference Point']


class CameraSettings(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(dict(name='Grab Camera', type='action'))
        self.addChild(dict(name='Restore Camera', type='action'))
        self.addChild(dict(name='Position', type='str', value=(-570, 445, 950)))
        self.addChild(dict(name='Focal Point', type='str', value=(150, 0, 0)))
        self.addChild(dict(name='View Up', type='str', value=(-0.8, -0.3, -0.5)))
        self.addChild(dict(name='Clipping Range', type='str', value=(600, 2000)))
        self.addChild(dict(name='Parallel Scale', type='float', value=410))
        self.addChild(dict(name='Parallel Projection', type='float', value=0))
        self.addChild(dict(name='View Angle', type='float', value=30))

        self.grab_camera_position = self.param('Grab Camera')
        self.restore_camera_position = self.param('Restore Camera')

        self.position = self.param('Position')
        self.focal_point = self.param('Focal Point')
        self.view_angle = self.param('View Angle')
        self.view_up = self.param('View Up')
        self.clipping_range = self.param('Clipping Range')
        self.parallel_scale = self.param('Parallel Scale')
        self.parallel_projection = self.param('Parallel Projection')

    def store_camera(self, vtk_camera):
        self.position.setValue(vtk_camera.GetPosition())
        self.focal_point.setValue(vtk_camera.GetFocalPoint())
        self.view_angle.setValue(vtk_camera.GetViewAngle())
        self.view_up.setValue(vtk_camera.GetViewUp())
        self.clipping_range.setValue(vtk_camera.GetClippingRange())
        self.parallel_scale.setValue(vtk_camera.GetParallelScale())
        self.parallel_projection.setValue(vtk_camera.GetParallelProjection())

    def restore_camera(self, vtk_camera):
        vtk_camera.SetPosition(self.position.value())
        vtk_camera.SetFocalPoint(self.focal_point.value())
        vtk_camera.SetViewAngle(self.view_angle.value())
        vtk_camera.SetViewUp(self.view_up.value())
        vtk_camera.SetClippingRange(self.clipping_range.value())
        vtk_camera.SetParallelScale(self.parallel_scale.value())
        vtk_camera.SetParallelProjection(self.parallel_projection.value())
        vtk_camera.SetViewAngle(self.view_angle.value())


class FloorRemoval(pTypes.GroupParameter):
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(dict(name='plane X', type='float', value=0, step=100))
        self.addChild(dict(name='plane Y', type='float', value=0, step=100))
        self.addChild(dict(name='plane Z', type='float', value=0, step=100))
        self.addChild(dict(name='plane nX', type='float', value=0, step=0.100))
        self.addChild(dict(name='plane nY', type='float', value=0, step=0.100))
        self.addChild(dict(name='plane nZ', type='float', value=1, step=0.100))

        self.addChild(dict(name='Update plane', type='action'))
        self.addChild(dict(name='Remove points below plane', type='action',
                           tip="Arrow side of the plane will be kept, other side will be removed"))

        self.addChild(dict(name='Remove smallest unconnected region', type='action'))

        self.addChild(dict(name='n.o. regions to keep', type='int', value=1, step=1))

        self.addChild(dict(name='Remove all but XXX biggest regions', type='action'))

        self.x_plane = self.param("plane X")
        self.y_plane = self.param("plane Y")
        self.z_plane = self.param("plane Z")
        self.nx_plane = self.param("plane nX")
        self.ny_plane = self.param("plane nY")
        self.nz_plane = self.param("plane nZ")
        self.n_regions_to_keep = self.param("n.o. regions to keep")


class GraphSettings(pTypes.GroupParameter):
    # create a group of parameters of the time span and resolution which are all dependending on each
    # other so that they should mutually change
    def __init__(self, **opts):
        opts['type'] = 'bool'
        opts['value'] = True

        pTypes.GroupParameter.__init__(self, **opts)

        # the members of the group as they appear in the menu
        self.addChild(dict(name='Show Cube Axis', type='bool', value=True))

        self.addChild(dict(name="Pipe Wall", type="group", children=[
            dict(name='Show Surface', type='bool', value=True),
            dict(name='Show Mesh', type='bool', value=False, tip="Show the mesh with a wire frame"),
            dict(name='Color', type='color', value="E6FFE9", tip="The color pipe wall"),
            dict(name='Opacity', type='float', value=1.0, step=0.1, limits=(0.0, 1.0),
                 tip="Make the wall surface more transparent")
        ]))

        self.addChild(dict(name="Targets", type="group", children=[
            dict(name='Show', type='bool', value=True),
            dict(name='Size', type='int', value=4, limits=(1, 100)),
            dict(name='Color', type='color', value="FF0000", tip="The color of the target points")
        ]))
        self.addChild(dict(name="Origin", type="group", children=[
            dict(name='Show', type='bool', value=True),
            dict(name='Size', type='int', value=10, limits=(1, 100)),
            dict(name='Color', type='color', value="00FF00", tip="The color of the origin point")
        ]))
        self.addChild(dict(name="Reference Point", type="group", children=[
            dict(name='Show', type='bool', value=True),
            dict(name='Show Pair', type='int', default=-1, value=-1, limits=(-1, None),
                 tip="Index of pair to show. If set to -1, do not show pair"),
            dict(name='Size', type='int', value=4, limits=(1, 100)),
            dict(name='Color', type='color', value="0000FF",
                 tip="Set the color of the reference target point"),
            dict(name='Pair Color', type='color', value="00FFFF", tip="Set the color of the pair")
        ]))

        self.show_cube_axis = self.param('Show Cube Axis')

        self.targets_color = self.param('Targets').names["Color"]
        self.targets_show = self.param('Targets').names["Show"]
        self.targets_size = self.param('Targets').names["Size"]

        self.ref_point_color = self.param('Reference Point').names["Color"]
        self.pair_color = self.param('Reference Point').names["Pair Color"]
        self.ref_point_show = self.param('Reference Point').names["Show"]
        self.pair_show = self.param('Reference Point').names["Show Pair"]
        self.ref_point_size = self.param('Reference Point').names["Size"]

        self.origin_color = self.param('Origin').names["Color"]
        self.origin_show = self.param('Origin').names["Show"]
        self.origin_size = self.param('Origin').names["Size"]

        self.wall_color = self.param('Pipe Wall').names["Color"]
        self.wall_show = self.param('Pipe Wall').names["Show Surface"]
        self.wall_show_mesh = self.param('Pipe Wall').names["Show Mesh"]
        self.wall_opacity = self.param('Pipe Wall').names["Opacity"]


# create the data containing all data
class PipeParameters(object):
    def __init__(self, transfer_parameters=None):

        # initialse the logger
        self.logger = logging.getLogger(__name__)

        self.name = 'PipeSlicer Parameters'

        # just the definition of the default parameters of the model
        params = [
            PipeInfo(name="Pipe info"),
            SliceSettings(name="Slice"),
            EllipseFitting(name="Ellipse fitting"),
            FloorRemoval(name="Remove floor"),
            NumericalSettings(name="Numerical"),
            GeneralSettings(name="General"),
            GraphSettings(name="Graph"),
            CameraSettings(name="Camera"),
        ]

        self.Parameters = Parameter.create(name=self.name, type='group', children=params)

    def update_logger(self, level):
        self.logger.setLevel(level)

    def connect_the_whole_family(self, children, event):
        # recursively connect all the children, childeren of children, etc to the event
        for child in children:
            child.sigValueChanging.connect(event)
            self.connect_the_whole_family(child, event)

    def connect_signals_to_slots(self):

        # first connect all children, grandchilderen etc to the valueChanging event
        self.connect_the_whole_family(self.Parameters.children(), self.valueChanging)

        # conncect the global tree change to the change event
        self.Parameters.sigTreeStateChanged.connect(self.changeTreeState)

    def saveConfiguration(self, fn):
        try:
            state = self.Parameters.saveState()
            pg.configfile.writeConfigFile(state, fn)
        except IOError as e:
            raise Exception(
                "saveConfiguration failed with I/O Error({}): {}".format(e.errno, e.strerror))
        except ValueError as e:
            raise Exception(
                "saveConfiguration failed with Value Error({}): {}".format(e.errno, e.strerror))
        except:
            raise Exception("saveConfiguration failed with Unidentified error")

        return True

    def loadConfiguration(self, fn):
        try:
            state = pg.configfile.readConfigFile(fn)
            self.loadState(state)
        except ParseError as e:
            raise Exception("loadConfiguration failed with ParseError {}".format(e))
        except IOError as e:
            raise Exception(
                "loadConfiguration failed with IOerror ({}) : {}".format(e.errno, e.strerror))
        except:
            raise Exception("loadConfiguration failed with Unknown Exception")

        return True

    def loadState(self, state):
        try:
            # self.Parameters.param(self.name).clearChildren()
            self.Parameters.restoreState(state, removeChildren=False)
            self.connect_signals_to_slots()
        except:
            raise Exception("Failed restoring state")

    def valueChanging(self, param, value):
        self.logger.debug("Value changing (not finalized): {} {}".format(param, value))

    def changeTreeState(self, param, changes):
        self.logger.debug("tree changes:")
        for param, change, data in changes:
            path = self.Parameters.childPath(param)
            if path is not None:
                childName = '.'.join(path)
            else:
                childName = param.name()
            self.logger.debug("  parameter: {}".format(childName))
            self.logger.debug("  change:    {}".format(change))
            self.logger.debug("  data:      {}".format(str(data)))
            self.logger.debug("  ----------")
        self.Parameters.emit(QtCore.SIGNAL("tree_parameter_changed"), childName)

    def save(self):
        self.state = self.Parameters.saveState()

    def restoreDefaultState(self):
        self.Parameters.restoreState(self.defaults_state, removeChildren=False)

    def setCurrentState(self):
        self.Parameters.restoreState(self.current_state, removeChildren=False)

    def treeChanged(self, *args):
        self.logger.debug("signal tree changed")
