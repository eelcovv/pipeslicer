from __future__ import division
from __future__ import absolute_import
from builtins import str
from builtins import zip
from builtins import map
from builtins import range
from past.utils import old_div
from builtins import object
__author__ = 'eelcovv'

"""
The functions and classes required by pipeslicer
"""
import os
import numpy as np
from scipy import linalg
from scipy import interpolate, array
import logging as log
import pandas as pd
import vtk
import geometryUtils as gu
from PyQt4 import QtCore, QtGui
# this module must be installed via anaconda.  Shapely contains the routines to calculate a COG of a polygon
from shapely.geometry import Polygon, Point, LineString, LinearRing


# unfortunately, hdf5 can not be transferred into an executable because the pytable is not converted correctly
# I threw out all hdf5 export possibilities and use excel instead.
# import h5py

# custom error types
class MaxNumberOfIterationsExceeded(Exception):
    """custom error for exceeding the maximum number of iterations"""
    pass


class RotateObject(object):
    """
    This class has the purpose to be able to rotate an object and pass the reference back. The reason that it was
    not turned into a function (initial version used def rotate_object) is that this is causing a memory leak
    In this construction, recursive rotations want cause memory we left behind. Typical usage is

    with RotateObject(myobject, pi/6,0,1,0) as rotate:
        myobject = rotate.execute()
    Which will take the object myobject and rotate it pi/6 degrees (angle in radians!) over the y-axis.
     The Flag 'use_output_port' is used to deal with both the surface object and the polygon object
    Same trick is applied to TranslateObject
    """

    def __init__(self, obj, anglerad, x, y, z, use_output_port=True):
        self.obj = obj
        angledeg = np.degrees(anglerad)
        transform = vtk.vtkTransform()
        transform.RotateWXYZ(angledeg, x, y, z)
        self.transformFilter = vtk.vtkTransformPolyDataFilter()
        self.transformFilter.SetTransform(transform)
        if use_output_port:
            if vtk.VTK_MAJOR_VERSION <= 5:
                self.transformFilter.SetInputConnection(self.obj.GetOutputPort())
            else:
                self.transformFilter.SetInputData(self.obj.GetOutput())
        else:
            # in case we pass a vtkPolydata structure, treat it differently
            if vtk.VTK_MAJOR_VERSION <= 5:
                self.transformFilter.SetInput(self.obj)
                self.transformFilter = self.transformFilter.GetOutput()
            else:
                try:
                    self.transformFilter.SetInputConnection(self.obj)
                except TypeError:
                    self.transformFilter.SetInputData(self.obj)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def execute(self):
        # the trick with use_output_port is very ugly. But it works
        self.transformFilter.Update()
        return self.transformFilter


class TranslateObject(object):
    def __init__(self, obj, x, y, z, use_output_port=True):
        self.obj = obj
        transform = vtk.vtkTransform()
        transform.Translate(x, y, z)
        self.transformFilter = vtk.vtkTransformPolyDataFilter()
        self.transformFilter.SetTransform(transform)

        if use_output_port:
            if vtk.VTK_MAJOR_VERSION <= 5:
                self.transformFilter.SetInputConnection(self.obj.GetOutputPort())
            else:
                self.transformFilter.SetInputData(self.obj.GetOutput())
        else:
            # in case we pass a vtkPolydata structure, treat it differently
            if vtk.VTK_MAJOR_VERSION <= 5:
                self.transformFilter.SetInput(self.obj)
                self.transformFilter = self.transformFilter.GetOutput()
            else:
                try:
                    self.transformFilter.SetInputConnection(self.obj)
                except TypeError:
                    self.transformFilter.SetInputData(self.obj)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        pass

    def execute(self):
        self.transformFilter.Update()
        return self.transformFilter


# @profile
def extrap1d(interpolator):
    """
    A small utility function in order to extrapolate the interp1d function outside the boundaries
    http://stackoverflow.com/questions/2745329/how-to-make-scipy-interpolate-give-an-extrapolated-result-beyond-the-input-range
    :param interpolator:
    :return:
    """
    xs = interpolator.x
    ys = interpolator.y

    def pointwise(x):
        if x < xs[0]:
            return ys[0] + (x - xs[0]) * (ys[1] - ys[0]) / (xs[1] - xs[0])
        elif x > xs[-1]:
            return ys[-1] + (x - xs[-1]) * (ys[-1] - ys[-2]) / (xs[-1] - xs[-2])
        else:
            return interpolator(x)

    def ufunclike(xs):
        return array(list(map(pointwise, array(xs))))

    return ufunclike


class PipeAnalyse(QtCore.QObject):
    """
    This class holds all info of a single pipe.
    All the numerical part related to pipeslicer are handled by this class
    """

    def __init__(self,
                 n_histo_bins=100,
                 scale_factor=0.5,
                 parent=None,
                 ):
        super(PipeAnalyse, self).__init__(parent)

        self.logger = log.getLogger(__name__)

        # these extensions are used for the export naming of the png files
        self.export_extension_list = ["_plot_radius_polar", "_plot_radius_linear", "_plot_thickness_linear"]
        self.slice_extend_name = "slice_p"

        # initialise the pointer to the data structures
        self.points = None
        self.number_of_points = None
        self.point_data = None
        self.wall_normals = None

        self.has_rim = True

        # pointer to the reference points in case available
        self.target_points = None
        self.vtk_targets_polydata = None
        self.has_targets = False

        # the initial numpy array that are going to store the points and wall normal vectors
        self.point_array = None
        self.normal_array = None

        # the initial settings for the alpha/beta angle
        self.alpha_range_min = 0.0
        self.alpha_range_max = np.pi
        self.beta_range_min = 0.0
        self.beta_range_max = np.pi
        self.alpha_at_min = None
        self.beta_at_min = None
        self.reduction_factor = 0.25
        self.alpha = np.linspace(0, np.pi, 50)
        self.beta = np.linspace(0, np.pi, 50)

        self.max_angle_reference_points = 25
        self.max_pair_distance = 0.02  # maximum distance between two pair points
        self.exclude_near_distance = 0.2

        self.max_slice_angle = 2  # criteria used to check angle of the slice
        self.min_bounding_box = 0.7  # mininum allowed bb with respect to the radius

        # flag to indicate if we have already (roughly) aligned the pipe so that the next alignment
        # can start around 0
        self.is_aligned = False
        self.delta_alpha = None
        self.delta_beta = None
        self.alpha_beta_resolution = None

        # define geometric properties
        self.total_area = None
        self.COG = None
        self.centroid = None
        self.x_zero_position = None
        self.x_rim_position = None
        self.bounds = None
        self.corner_bottom_low = None
        self.corner_top_high = None
        self.mid_of_domain = None
        self.size_of_domain = None

        # the conversion power (raised to the 10^-1) in order to convert from m to the STL units (normally in mm so 3)
        self.power_units = 3

        # this point is used to store the reference point to indicate phi=0 at the x=0 position
        self.reference_point = None

        self.index_reference_point = None

        self.check_slice_index = 0  # index of the slice used for exporting to batch file list and checking
        self.check_slice_name = None  # name of the slice used for exporting to batch file list and checking

        # a list with the nearest neighbours
        self.neighbour_list = []

        # the maximum number of connected regions for the first (course) alignment and final alignment
        self.maximum_connected_regions_1 = 5
        self.maximum_connected_regions_2 = 1
        self.pick_largest_region = True  # if this one is true then the maximum_connected_regions is not used

        # the clip position of the pipe in m (! so much be converted to mm in the routine)
        self.clip_x_position_above = 0.200
        self.clip_x_position_below = 0.0
        self.clip_r_range = 0.25

        # in case of an iterative determination of the rotation angle to align the pipe, the scale factor determines
        # the factor at which the alpha beta range is decreased each itteration.
        self.scale_factor = scale_factor

        self.n_histo_bins = n_histo_bins

        # create the array to store the summation of the x-positions
        self.xcoorsqrsum = None

        # create a data array to store the join pdf
        self.theta_phi_pdf = None
        self.phi_edges = None
        self.theta_edges = None

        self.slice_list = None

        # the colunm names: X,Y,Z: absolute coodinates, Ycog,Zcog: coordinates with respect to COG
        # radius phi, define distance and angle to COG. Nx,Ny,Nz: the normal vector components
        self.column_names = ["X", "Y", "Z", "Ycog", "Zcog", "radius", "phi", "Nx", "Ny", "Nz", "innerWall",
                             "Dwall_normal", "Corrupted"]

        self.stl_filename = None

        # stl_surface is the pointer to the vtk structure holding all the triangles of a single pipe
        self.stl_surface = None

        # this container is going to carry all the slice data into a stack of dataframes
        self.slice_dataframes = None

        # this container will carry the statistics per slice
        self.slice_stat_df = None

        # create the cut plane for later usage
        # the cut plane
        self.cutActor = None

        # create the cut plane.
        self.plane = vtk.vtkPlane()
        self.plane.SetNormal(1, 0, 0)

        self.cutter = vtk.vtkCutter()
        self.cutter.SetCutFunction(self.plane)
        self.cutMapper = vtk.vtkPolyDataMapper()

        # ovality results

        # targets (the four stickers) and the normals of the planes fit through the targets
        self.ovality_targets = None

    def update_logger(self, level):
        self.logger.setLevel(level)

    def create_slice_list(self, start, end, n_slices):

        # multiply the dimensions in order to convert the slice position from m to the units of the STL file
        f = 10 ** (self.power_units)

        # create a list with the x-position of the slices
        self.slice_list = np.linspace(f * start, f * end, n_slices, endpoint=True)

        self.logger.debug("slice list : {}".format(self.slice_list))

    # @profile
    def import_the_stlfile(self, fname):
        # read the STL data file and calculate the surface normals
        reader = vtk.vtkSTLReader()
        reader.SetFileName(str(fname))
        reader.Update()

        if self.stl_surface is not None:
            self.logger.info("Removing the old STL file from memory")
            self.stl_surface = None
            self.vtk_targets_polydata = None
            self.data_in = None
            self.reference_point = None
            self.index_reference_point = None
            self.target_points = None
            self.has_targets = False
            self.target_data = None
            self.neighbour_list = []
            self.x_zero_position = None
            self.is_aligned = False

        self.stl_surface = vtk.vtkPolyDataNormals()
        self.stl_surface.SetInputConnection(reader.GetOutputPort())
        self.stl_surface.Update()

        # turn the vtk point and normals into numpy array so we can deal with it
        self.retrieve_surface_data()

        # obtain the distribution of the normal vectors
        self.calculate_phi_theta_distribution()

        self.logger.info("loaded the STL file {} containing {} Points and {} Cells".format(fname, self.number_of_points,
                                                                                           self.number_of_cells))
        self.stl_filename = fname

        # connect the STL to the cutter and cut mapper
        self.cutter.SetInputConnection(self.stl_surface.GetOutputPort())
        self.cutter.Update()
        self.cutMapper.SetInputConnection(self.cutter.GetOutputPort())
        self.cutMapper.Update()

    # @profile
    def import_the_reference_point(self, filename):
        # import the reference point file in this routine
        self.has_targets = True
        try:
            self.logger.info("Reading the reference point file {}".format(filename))
            column_names = ['X', 'Y', 'Z', 'c1', 'c2', 'c3', 'i1', 'i2', 'i3']
            self.target_data = None

            self.index_reference_point = None
            self.reference_point = None
            self.vtk_targets_polydata = None

            # read the CSV data file into a data frame
            self.target_data = pd.read_csv(filename, sep="\s+", comment="#", header=None)
            n_target_data = len(self.target_data.index)

            # only copy the colums names as much as we have column in the CSV file
            self.target_data.columns = column_names[:min(len(self.target_data.columns), len(column_names))]

            # we want a copy of the X,Y,Z position, so create a new data frame (otherwise you get issues later)
            self.target_points = pd.DataFrame(np.array([np.arange(n_target_data)] * 3).T, columns=column_names[:3])

            # copy the first 3 columns of the target_data into the target_points data frame
            self.target_points["X"] = self.target_data["X"]
            self.target_points["Y"] = self.target_data["Y"]
            self.target_points["Z"] = self.target_data["Z"]

            self.logger.info("Create target list with {} points".format(n_target_data))

        except IOError as err:
            self.logger.warning("Could not find the reference point file {}".format(filename))
            self.has_targets = False
        except:
            raise

        self.logger.debug("Imported the target point file")
        # create a list of the nearest neighbours
        self.get_indices_nearest_neighbours()

        try:
            vtk_target_points = vtk.vtkPoints()
            # point_cloud = pm.VtkPointCloud()
            for index, row in self.target_points.iterrows():
                # point_cloud.addPoint([row.X, row.Y, row.Z])
                vtk_target_points.InsertNextPoint([row.X, row.Y, row.Z])
            self.vtk_targets_polydata = vtk.vtkPolyData()
            self.vtk_targets_polydata.SetPoints(vtk_target_points)
        except:
            self.logger.warning("did not succeed in creating a reference point data structure")
            self.has_targets = False

    # @profile
    def update_target_points_from_vtk_polydata(self):
        # after a transormation of the vtk polydata, the target point need to be updated as well
        try:
            vtk_polydata_points = self.vtk_targets_polydata.GetPoints()
            n_points = vtk_polydata_points.GetNumberOfPoints()
        except AttributeError:
            # in python 3 we need to add GetData
            vtk_polydata_points = self.vtk_targets_polydata.GetOutput().GetPoints()
            n_points = vtk_polydata_points.GetNumberOfPoints()

        for index in range(n_points):
            point = vtk_polydata_points.GetPoint(index)
            self.target_points.loc[index, 'X'] = point[0]
            self.target_points.loc[index, 'Y'] = point[1]
            self.target_points.loc[index, 'Z'] = point[2]

    def remove_model_section_below_clip_plane(self, position, normal):
        """
        Remove section of STL below given clip-plane
        :param position:
        :param normal:
        :return:
        """
        clipPlane = vtk.vtkPlane()
        clipPlane.SetNormal(normal[0], normal[1], normal[2])
        clipPlane.SetOrigin(position[0], position[1], position[2])

        self.logger.debug("Clipping")

        # clip the stl surface with the plane
        clipper1 = vtk.vtkClipPolyData()
        clipper1.SetInputConnection(self.stl_surface.GetOutputPort())
        clipper1.SetClipFunction(clipPlane)
        clipper1.Update()

        # put the result back into our stl_surface
        self.stl_surface = clipper1

        self.retrieve_surface_data()





    # @profile
    def clip_pipe(self, enlarge_factor=1):
        # multiply the dimensions in order to convert the slice position from m to the units of the STL file
        f = 10 ** self.power_units

        # calculate the xposition based on the clip_x_position (given in m)
        # an enlargement factor can be applied in case a first rough cut needs to be made for the first round
        x_clip_pos_below = f * self.clip_x_position_below * (old_div(1.0,enlarge_factor))
        x_clip_pos_above = f * self.clip_x_position_above * enlarge_factor
        x_clip_radius = f * self.clip_r_range * enlarge_factor

        clipPlane_above = vtk.vtkPlane()
        clipPlane_above.SetNormal(-1, 0, 0)
        clipPlane_above.SetOrigin(x_clip_pos_above, 0, 0)

        clipPlane_below = vtk.vtkPlane()
        clipPlane_below.SetNormal(-1, 0, 0)
        clipPlane_below.SetOrigin(x_clip_pos_below, 0, 0)

        # rotate 90 degrees over the z axis so it is aligned with the x axis
        transorm = vtk.vtkTransform()
        transorm.RotateWXYZ(90, 0, 0, 1)

        # create a cylinder along the y axis
        cylinder = vtk.vtkCylinder()
        cylinder.SetCenter(0, 0, 0)
        cylinder.SetRadius(x_clip_radius)
        cylinder.SetTransform(transorm)

        # clip the stl surface with the plane
        clipper1 = vtk.vtkClipPolyData()
        clipper1.SetInputConnection(self.stl_surface.GetOutputPort())
        clipper1.SetClipFunction(clipPlane_above)
        clipper1.Update()

        # put the result back into our stl_surface
        self.stl_surface = clipper1

        if self.clip_x_position_below > 0:
            # clip the stl surface with the plane to remove all lower values. This is only done when the position
            # is largen than zero
            clipper2 = vtk.vtkClipPolyData()
            clipper2.SetInputConnection(self.stl_surface.GetOutputPort())
            clipper2.SetClipFunction(clipPlane_below)
            clipper2.InsideOutOn()
            clipper2.Update()

            # put the result back into our stl_surface
            self.stl_surface = clipper2

        # after the bottom of the slice has been cut off, calculate the COG again and move the origin to the
        # new COG. This the original COG may not have been totally in the center in case the scan was not
        # straight at the bottom
        self.get_geometric_properies()
        self.retrieve_surface_data()

        self.logger.debug("ref point check {}".format(self.reference_point))

        self.emit(QtCore.SIGNAL("update_statusbar_message"), "Rotating and translating the pipe to its new position...")

        if self.x_zero_position is not None:
            # we have found a reference point indicating the x-zero already, so it was already translated. Do
            # not do it again!
            self.logger.debug("Base x position in target position {}".format(self.x_zero_position))
            tx = -self.x_zero_position
        else:
            # base the translation on the mean rim position of the stl file
            self.logger.debug("Base x position in rim position {}".format(self.x_rim_position))
            tx = -self.x_rim_position
        ty = -self.COG[1]
        tz = -self.COG[2]
        self.logger.debug("transpose after clip with plane {} {} {}".format(tx, ty, tz))
        with TranslateObject(self.stl_surface, tx, ty, tz) as translate:
            self.stl_surface = translate.execute()

        # the STL surface has been rotated/translated. Now do the same with the reference points they are present
        if self.vtk_targets_polydata:
            self.logger.debug("also rotate and translate the reference points inside clipper")

            with TranslateObject(self.vtk_targets_polydata, tx, ty, tz, False) as translate:
                self.vtk_targets_polydata = translate.execute()

            self.update_target_points_from_vtk_polydata()

        # now we can clip the cylinder. The pipe should be in the center
        # clip it again with the cylinder
        clipper = vtk.vtkClipPolyData()
        clipper.SetInputConnection(self.stl_surface.GetOutputPort())
        clipper.SetClipFunction(cylinder)
        clipper.SetInsideOut(-1)
        clipper.Update()

        # put the result back into our stl_surface
        self.stl_surface = clipper

    def remove_smallest_region(self):
        """
        Removes the smallest connected region from the pipe
        """
         # filter the pipe based on the connectivity of the ST
        connectivity = vtk.vtkConnectivityFilter()
        connectivity.SetInputConnection(self.stl_surface.GetOutputPort())

        # get the number of regions
        connectivity.Update()
        n = connectivity.GetNumberOfExtractedRegions()

        self.filter_on_connectivity(n-1)

        return n-1


    # @profile
    def filter_on_connectivity(self, max_connected_regions, progres_function = None):
        """
        Remove the smallest regions from the surface.

        There is no build-in function (or I could not find it) to keep the biggest connected surfaces.
        So the first step is to add all regions one by one and determine their size (using the number of cells).
        This is quite slow.

        Once that is known it is a matter of sorting and adding the largest regions to the result.

        :param max_connected_regions: Number of regions to keep
        :param progres_function: [optional] callback function to show progres. Called with arguments (i,n)
        :return: nothing
        """# filter the pipe based on the connectivity of the ST
        connectivity = vtk.vtkConnectivityFilter()
        connectivity.SetInputConnection(self.stl_surface.GetOutputPort())

        if max_connected_regions is not None:
            # if a value of max_connected_regions is given, pick the first n regions
            connectivity.SetExtractionModeToSpecifiedRegions()
            connectivity.Update()

            if max_connected_regions > connectivity.GetNumberOfExtractedRegions():
                self.logger.error('instructed to keep {} regions but only {} regions present'.format(max_connected_regions,connectivity.GetNumberOfExtractedRegions()))

            # Determine the size of the individual regions
            region_size = []
            last_size = 0
            for i in range(connectivity.GetNumberOfExtractedRegions()):
                connectivity.AddSpecifiedRegion(i)
                connectivity.Update()

                # store the number of cells in the region that was just added
                number_of_cells = connectivity.GetOutput().GetNumberOfCells()
                region_size.append(number_of_cells-last_size)
                last_size = number_of_cells
                self.logger.debug('id / number of cells :{} {}'.format(i, region_size[-1]))

                if progres_function:
                    progres_function(i, connectivity.GetNumberOfExtractedRegions())

            # make a new filter
            connectivity = vtk.vtkConnectivityFilter()
            connectivity.SetInputConnection(self.stl_surface.GetOutputPort())
            connectivity.SetExtractionModeToSpecifiedRegions()

            region_id_from_small_to_large = np.argsort(region_size)

            for i in range(max_connected_regions):
                connectivity.AddSpecifiedRegion(region_id_from_small_to_large[-(i+1)])


        else:
            # just pick the largest region
            connectivity.SetExtractionModeToLargestRegion()

        # the the unstructured mesh back into a vtkpolymesh
        geo = vtk.vtkGeometryFilter()
        geo.SetInputConnection(connectivity.GetOutputPort())

        geo.Update()

        # put the result back into our stl_surface
        self.stl_surface = geo

    # @profile
    def align_pipe(self, maxit=10, n_sample_1=50, n_sample_2=50):
        # align the pipe based on the normal vector distribution
        # see in the SphericalDistributionAnalyser for more details
        cnt = 0

        # make sure we have the latest normal and surface data
        self.retrieve_surface_data()
        self.init_alignment()

        n_alpha = n_sample_1
        n_beta = n_sample_1

        while (not self.alpha_beta_converged) and (cnt < maxit):
            # iteratively obtain the alpha beta angles for smaller search area (thus increasing the resolution)
            # as soon as the change of the angle is below 1 degree, the converged flag is set to true

            self.get_alpha_beta_for_aligned_pipe(cnt, maxit, n_alpha, n_beta)

            # for the next iterations, update the resolution of alpha/beta space
            n_alpha = n_sample_2
            n_beta = n_sample_2

            cnt += 1

        self.rotate_and_translate_the_pipe_and_the_targets(self.alpha_at_min, self.beta_at_min)

        if self.COG[0] < 0:
            self.logger.debug("COG[0] = {}. rotating the pipe!".format(self.COG))
            # only rotate around z axis with pi degrees to change the direction of the pipe
            self.rotate_and_translate_the_pipe_and_the_targets(alpha=0, beta=0, rotation_angle_offset=np.pi,
                                                               translate_the_pipe=False)

    def rotate_and_translate_the_pipe_and_the_targets(self, alpha=0, beta=0, rotation_angle_offset=0,
                                                      translate_the_pipe=True):

        self.logger.debug("ROTATNG AROUND {} {} with rot ang {}".format(alpha, beta,
                                                                        rotation_angle_offset))

        # rotate the stl pipe by the calculated alpha and beta angle (in negative direction)
        with RotateObject(self.stl_surface, alpha, 0, 1, 0) as rotate:
            self.stl_surface = rotate.execute()

        # rotate again over the z-axis. We add a rotation offset which is either 0 or pi in order to set the
        # direction of the pipe into the positive x-axis direction. The offset is determined with get_alpha_beta
        anglez = beta + rotation_angle_offset
        with RotateObject(self.stl_surface, anglez, 0, 0, 1) as rotate:
            self.stl_surface = rotate.execute()

        try:
            self.logger.debug("also rotate and translate the reference points")
            with RotateObject(self.vtk_targets_polydata, alpha, 0, 1, 0, True) as rotate:
                self.vtk_targets_polydata = rotate.execute()
            with RotateObject(self.vtk_targets_polydata, anglez, 0, 0, 1, True) as rotate:
                self.vtk_targets_polydata = rotate.execute()

            self.update_target_points_from_vtk_polydata()
            self.get_index_reference_point()

            self.x_zero_position = self.reference_point.X
        except AttributeError:
            self.logger.info("Failed rotating the reference points. Something wrong ?")

        # get the COG and mean position of the rim of the tube
        self.get_geometric_properies()
        self.retrieve_surface_data()

        self.logger.debug("ref point check {}".format(self.reference_point))

        self.emit(QtCore.SIGNAL("update_statusbar_message"), "Rotating and translating the pipe to its new position...")

        if translate_the_pipe:

            if self.x_zero_position is not None:
                # we have found a reference point indicating the x-zero already, so it was already translated. Do
                # not do it again!
                tx = -self.x_zero_position
            else:
                # base the translation on the mean rim position of the stl file
                tx = -self.x_rim_position
            ty = -self.COG[1]
            tz = -self.COG[2]
            self.logger.debug("tranpose {} {} {}".format(tx, ty, tz))
            with TranslateObject(self.stl_surface, tx, ty, tz) as translate:
                self.stl_surface = translate.execute()

            # the STL surface has been rotated/translated. Now do the same with the reference points
            # they are present
            if self.vtk_targets_polydata:
                self.logger.debug("also rotate and translate the reference points")

                with TranslateObject(self.vtk_targets_polydata, tx, ty, tz, False) as translate:
                    self.vtk_targets_polydata = translate.execute()

                self.update_target_points_from_vtk_polydata()

        if self.vtk_targets_polydata:
            # in case we had a reference point, finally rotate around the x -axis to set the
            # reference point at phi=0
            if self.reference_point is not None:
                self.emit(QtCore.SIGNAL("update_statusbar_message"), "Rotating around x-axis to set reference point...")
                self.reference_point = self.target_points.iloc[self.index_reference_point]
                self.logger.debug("after translation ref point is {}".format(self.reference_point))
                angle_around_x = - np.arctan2(self.reference_point.Z, self.reference_point.Y)
                self.logger.debug("rotating around x axis with {} rad".format(angle_around_x))
                with RotateObject(self.stl_surface, angle_around_x, 1, 0, 0) as rotate:
                    self.stl_surface = rotate.execute()

                self.logger.debug(
                    "readjusting x zero from {} to {}".format(self.x_zero_position, self.reference_point.X))
                self.x_zero_position = self.reference_point.X

                with RotateObject(self.vtk_targets_polydata, angle_around_x, 1, 0, 0, True) as rotate:
                    self.vtk_targets_polydata = rotate.execute()

                self.update_target_points_from_vtk_polydata()

        # after alignment, update the connection to the surface with the cutter
        self.cutter.SetInputConnection(self.stl_surface.GetOutputPort())
        self.cutter.Update()
        self.cutMapper.SetInputConnection(self.cutter.GetOutputPort())
        self.cutMapper.Update()

        # get the normals and recalculate the theta-phi PDF
        self.retrieve_surface_data()
        self.get_geometric_properies()
        self.calculate_phi_theta_distribution()

        # finally, set the aligment flag so that we now that this pipe is already algined at least one time
        self.is_aligned = True

        self.emit(QtCore.SIGNAL("displayFinished"))

    # @profile
    def create_linear_ring(self, yp, zp):
        """
        Creat an geometric optject out if a list of  point
        yp: column out of a data frame (ie. a series) with the y coordinates
        zp: column out of a data frame (ie. a series) with the z coordinates
        """

        # create array of points out of the profile
        data_array = np.vstack((
            np.hstack((yp.values, np.array(yp.values[0]))),
            np.hstack((zp.values, np.array(zp.values[0])))
        ))

        # calculate the polygon COG and subtract that from the 2d data points
        # use hstack to copy the first point to the end of the list to get a closed list of points
        # create a linear ring because this has a projection function. See
        # http://stackoverflow.com/questions/33311616/find-coordinate-of-closest-point-on-polygon-shapely
        profile_ring = LinearRing(list(zip(data_array[0], data_array[1])))
        return profile_ring

    # @profile
    def slice_pipe(self, x_position, use_cog_global=False, use_projection_for_thickness=True):
        # loop over the slice list an make a cut section at every position. Assumes that the pipe is already aligned
        # with the x-axis
        self.plane.SetOrigin(x_position, 0, 0)

        self.cutter.Update()
        slice_in = None

        cutter_out = self.cutter.GetOutput()
        slice_points = cutter_out.GetPoints()
        try:
            n_slice_points = slice_points.GetNumberOfPoints()
        except AttributeError:
            n_slice_points = 0
            self.logger.warning("No points found in slice at xpos={}. Skipping it".format(x_position))

        if n_slice_points > 0:
            self.logger.debug("proceed with analysing the slice {}".format(x_position))
            slice_point_data = cutter_out.GetPointData()
            slide_data_Normals = slice_point_data.GetArray("Normals")

            # create the dataframe which is going to hold all the info of the current slice
            df1 = pd.DataFrame(index=list(range(n_slice_points)), columns=self.column_names)

            # first store all the points and normals in a numpy array
            points = np.empty((n_slice_points, 3))
            normals = np.empty((n_slice_points, 3))
            for i in range(n_slice_points):
                # the the coordinates of this point
                points[i, :] = np.array(slice_points.GetPoint(i))

                # the normal vector components
                normals[i, :] = slide_data_Normals.GetTuple(i)

            # calculate the 2D y,z position with respect to the origin at the level x=xpos, plus the radius and phi
            points2d = points[:, 1:]
            radius2d, phi2d = self.points2d_to_rad_and_phi(points2d)

            # calculate the inner product of each point with each normal vector. Since the np.dot calculates the
            # dot product of the last and one but last index, take the transpose of points2d. Then, only the
            # diagonal should be extracted
            dotprod = np.diag(np.dot(normals[:, 1:], points2d.T))

            # copy the data from the numpy arrays to the current dataframe
            df1.X = points[:, 0]
            df1.Y = points[:, 1]
            df1.Z = points[:, 2]
            df1.Ycog = points2d[:, 0]
            df1.Zcog = points2d[:, 1]
            df1.radius = radius2d
            df1.phi = phi2d
            df1.Nx = normals[:, 0]
            df1.Ny = normals[:, 1]
            df1.Nz = normals[:, 2]

            # if the dot product between the point at the wall and the normal is negative, we are at the
            # inner wall because the normal is pointing to the center (i.e. is opposite to the vector to the point)
            df1.innerWall = np.where(dotprod < 0, True, False)
            df1.okPoint = np.full(n_slice_points, True, dtype=bool)

            # extract inner and outer slice from the data frame.
            slice_in, slice_out = self.get_inner_and_outer_slice(df1)

            try:
                n_slice_in_points = len(slice_in.index)
            except AttributeError:
                # in case we can not get the number of points of the inner slice, try it again on the outer
                # slice. If that succeeds, we are going to turn the outer slice in an inner slice and carry on
                n_slice_in_points = len(slice_out.index)
                self.logger.warning("No inner wall present but we have an outer wall. Copy outer to inner")
                df1.innerWall = True
                df1.Nx *= -1
                df1.Ny *= -1
                df1.Nz *= -1
                slice_in, slice_out = self.get_inner_and_outer_slice(df1)
            except AttributeError:
                self.logger.warning("No inner and outer wall present. Continue without slicing")
                return None

            # create geometric data object
            profile_in = self.create_linear_ring(slice_in.Y, slice_in.Z)

            try:
                # try to create the outer profile. If this is possible, the outer wall exists. Otherwise set it to
                # None and skip calculating the wall thickness below
                profile_out = self.create_linear_ring(slice_out.Y, slice_out.Z)
            except AttributeError:
                self.logger.debug("Not outer wall presented so profile_out is not set")
                profile_out = None

            # set the centroid of the inner profile. In case that you want to use the global COG, just set the
            # centroid at [0. 0] (as the COG is a the origin. Normally, however, you should take the profile local
            # centroid obtained from the centroid method from shapely
            if use_cog_global:
                self.centroid_in = Point(0, 0)
            else:
                # get the centroid of the local inner profile
                self.centroid_in = profile_in.centroid

            # subtract the calculated centroid from the point list to shift both the inner and outer profile
            # in case an outer wall is present, this command shifts both the points of the inner and outer wall as
            # point2d containts both. Ideally this is what you want, as the inner and outer wall are connected
            self.logger.debug("Shifting profile centroid inner {}".format(self.centroid_in))
            points2d -= self.centroid_in

            if self.align_profile_cogs and profile_out is not None:
                # in case an outer profile is present and it is request to shift the COG to the COG of the inner profile
                # subtract the location of the outer COG from the outer profile
                # This is not really allowed, but it appears that the scan of an inner and outer profile is sometimes
                # shifted. To correct for this, just put both COG on top of each other
                self.centroid_out = profile_out.centroid
                delta_centroids = Point(self.centroid_out.x - self.centroid_in.x,
                                        self.centroid_out.y - self.centroid_in.y)
                self.logger.debug("Matching profils with out {} delta {}".format(self.centroid_out, delta_centroids))
                # the invert statement of the innerWall boolean makes a selection of all outer wall values
                points2d[np.invert(df1.innerWall.values)] -= delta_centroids

            # update the data frame for the new position and pass it through to the slices
            radius2d, phi2d = self.points2d_to_rad_and_phi(points2d)
            df1.Ycog = points2d[:, 0]
            df1.Zcog = points2d[:, 1]
            df1.radius = radius2d
            df1.phi = phi2d

            slice_in, slice_out = self.get_inner_and_outer_slice(df1)
            profile_in = self.create_linear_ring(slice_in.Ycog, slice_in.Zcog)
            self.logger.debug("New profile cog : {}".format(profile_in.centroid))
            self.logger.debug("mean min max r {} {} {} ".format(slice_in.radius.mean(), slice_in.radius.min(), slice_in.radius.max()))

            if profile_out is not None and slice_in is not None:
                # only if the outer profile is available do this part: calculate the thickness of the wall

                self.logger.info("Both inner and outer wall present. Calculating the wall thickness")

                f_inter = interpolate.interp1d(slice_out.phi, slice_out.radius, bounds_error=False)
                # to take care of the boundaries, use the extrapolation function
                f_extra = extrap1d(f_inter)

                # for the inner wall phi position, create a new column containing the interpolate outer wall radius
                slice_in["radius_out"] = f_extra(slice_in.phi)

                # calculate the wall thickness by the assumption that the distance for equal phi equals the wall
                # thickness. (which is not exactly true but OMS uses the same assumption)
                slice_in["Dwall"] = slice_in["radius_out"] - slice_in["radius"]

                # the delta_radius is only equal to the wall thickness if both profiles are perfect circels.
                # here we implemented a better way to calculate the wall thickness by projecting the points of the
                # inner profile to the outer profile. The closest distance is considererd the wall thickness

                # first create a profile for the outer wall
                profile_in = self.create_linear_ring(slice_in.Ycog, slice_in.Zcog)
                profile_out = self.create_linear_ring(slice_out.Ycog, slice_out.Zcog)

                # create an empty array to store the wall thickness for each phi location of the inner profile
                # add 1 as the inner profile is closed by adding the first value to the end of the array
                wall_thickness = np.empty(n_slice_in_points+1)

                # extract the coordinates and iterate of the points of the -inner- profile
                profile_in_points_x,  profile_in_points_y = profile_in.coords.xy
                for ii, (xp, yp) in enumerate(np.nditer([profile_in_points_x, profile_in_points_y])):
                    point_in = Point(xp, yp)

                    # project the point point_in of the inner wall to the outer wall
                    #self.logger.debug("Projecting point {}".format(point_in))
                    projection_at_outer_profile = profile_out.project(point_in)
                    point_out = profile_out.interpolate(projection_at_outer_profile)

                    # the wall thickness is equal to the distance between the points at the inner wall and its project
                    # at the outer wall
                    wall_thickness[ii] = point_in.distance(point_out)

                # this wall thickness is the closest project of each inner profile point to the outer wall
                # skip the last point as that is the same as the first because we closed the profiles
                slice_in["Dwall_projection"] = wall_thickness[:-1]

        # only return the  inner slice data frame. In case the outer slice was present, the inner slice data frame
        # contains the information via the radius_out field and Dwall.
        return slice_in

    def get_stats_slice(self, slice_df):
        """
        routine to calculate the statistic of a slice
        :param slice_df: a pandas data frame containing the information of the current slice
        :return: a new pandas data frame containing the statistics values
        """
        stat_df = pd.DataFrame(index=[0], columns=[])

        # calculate the diameter by substracting the radius at phi from the radius at phi+180 deg. The latter
        # is obtained by rolling the array over half the number of points
        nh = old_div(len(slice_df.index), 2)
        diameter = slice_df.radius + np.roll(slice_df.radius, nh)

        mean = slice_df.mean()
        min = slice_df.min()
        max = slice_df.max()

        stat_df["Rmean"] = mean.radius
        stat_df["Rmin"] = min.radius
        stat_df["Rmax"] = max.radius
        stat_df["IDmean"] = np.mean(diameter)
        stat_df["IDmin"] = np.min(diameter)
        stat_df["IDmax"] = np.max(diameter)
        stat_df["OOR"] = stat_df.IDmax - stat_df.IDmin

        # the inclination angle of the current point is given the outer product of r and N
        phi = np.arctan2(slice_df.Zcog, slice_df.Ycog)
        cosphi = np.cos(phi)
        sinphi = np.sin(phi)
        # calcuate the outerproduct between the normal vector nx,ny,nz and the vector in the y,z plane from the
        # origin to the current point at the pipe wall
        vec_out_x = slice_df.Nz.values * cosphi - slice_df.Ny.values * sinphi
        vec_out_y = -slice_df.Nx.values * sinphi
        vec_out_z = -slice_df.Nx.values * sinphi
        vec_out_mag = np.sqrt(vec_out_x ** 2 + vec_out_y ** 2 + vec_out_z ** 2)
        theta = np.arcsin(vec_out_mag)

        # store the angle in degrees
        stat_df["theta"] = np.mean(np.rad2deg(theta))

        try:
            # calculate the diameter by adding the radius(phi) to radius(phi+pi). The second term follows from
            # a roll of the radius array over half the radius array
            diameter_out = slice_df.radius_out + np.roll(slice_df.radius_out, nh)
            stat_df["ODmean"] = np.mean(diameter_out)
            stat_df["ODmin"] = np.min(diameter_out)
            stat_df["ODmax"] = np.max(diameter_out)
            stat_df["Dwall"] = np.mean(slice_df.Dwall)
        except AttributeError:
            pass

        return stat_df

    @staticmethod
    def points2d_to_rad_and_phi(pts):
        # convert the 2d x,y points to a radius and phi angle
        radius2d = np.linalg.norm(pts, axis=1)

        # arctan2 requires as input y,x!
        phi2d = np.arctan2(pts[:, 1], pts[:, 0])

        # make sure  that phi is in between 0 and 2pi (and not -pi and pi, which is the output of arctan2
        phi2d = np.where(phi2d < 0, phi2d + 2 * np.pi, phi2d)

        return radius2d, phi2d

    @staticmethod
    def get_inner_and_outer_slice(df):
        # get the inner and outer wall and sort on the angle phi
        """

        :rtype : object
        """
        df_grouped = df.groupby("innerWall")
        slice_in = None
        slice_out = None
        try:
            slice_in = df_grouped.get_group(True).sort_values(by="phi")
        except KeyError:
            # the keyerror indicates that the the group innerwall is not true, so only one side was found
            pass

        try:
            slice_out = df_grouped.get_group(False).sort_values(by="phi")
        except KeyError:
            # the keyerror indicates that the the group outerwall is not true, so only one side was found
            pass

        return slice_in, slice_out

    # @profile
    def write_stl_file(self, filename):
        # Write the stl file to disk
        self.logger.info("Writing to STL file : {}".format(filename))
        stlWriter = vtk.vtkSTLWriter()
        stlWriter.SetFileTypeToBinary()
        stlWriter.SetFileName(filename)
        if vtk.VTK_MAJOR_VERSION <= 5:
            stlWriter.SetInput(self.stl_surface.GetOutput())
        else:
            stlWriter.SetInputConnection(self.stl_surface.GetOutputPort())
        stlWriter.Write()

    # @profile
    def write_target_points(self, filename):
        # Write the target points to file
        self.logger.info("Writing to target point file : {}".format(filename))
        try:
            self.target_points.to_csv(filename, header=None, index=False, sep=" ")
        except IOError:
            self.logger.warning("No valid file with target points was written")
            pass

    # @profile
    def retrieve_surface_data(self):
        """
        read the paraview data structure into a numpy array
        """
        self.data_in = self.stl_surface.GetOutput()
        self.points = self.data_in.GetPoints()
        self.point_data = self.data_in.GetPointData()
        self.wall_normals = self.point_data.GetNormals()

        # get the total number of points
        self.number_of_points = self.data_in.GetNumberOfPoints()
        self.number_of_cells = self.data_in.GetNumberOfCells()

        # create the numpy array for storing the point positions and wall normals
        self.point_array = np.empty((self.number_of_points, 3))
        self.normal_array = np.empty((self.number_of_points, 3))
        self.set_point_array_and_retrieve_rim_position()

    def set_point_array_and_retrieve_rim_position(self):
        """
        get the position of the rim of the current surface.
        :return:
        """

        n_rim_points = 0
        self.x_rim_position = 0

        for i in range(self.number_of_points):
            # the the coordinates of this point
            self.point_array[i, :] = np.array(self.points.GetPoint(i))

            # the normal vector components
            self.normal_array[i, :] = self.wall_normals.GetTuple(i)

            # in case the pipe has been aligned, the rim position is retrieved from the triangle pointing
            # into the negative x-direction. This rim position can be used to set the pipe origin at the rim
            # however, in case also a target point with a proper reference point is loaded, this position is used
            # in stead of the rim position
            if self.normal_array[i, 0] < -0.95:
                n_rim_points += 1
                self.x_rim_position += self.point_array[i, 0]

        # get the average x position of the rim
        if n_rim_points > 0:
            self.x_rim_position /= n_rim_points

        self.logger.debug("rim position {}".format(self.x_rim_position))

    # @profile
    def init_alignment(self):
        # make sure all the settings are reset to it initial values for each alignment
        self.alpha_range_min = 0.0
        self.alpha_range_max = np.pi
        self.beta_range_min = 0.0
        self.beta_range_max = np.pi
        self.alpha_beta_converged = False

        if self.is_aligned:
            # we have a aligned pipe, so limited the search area around zero
            self.get_reduced_range(0, self.alpha)

    # @profile
    def get_alpha_beta_for_aligned_pipe(self, cnt=None, maxit=10, n_alpha=50, n_beta=50):

        # align the pipe by rotating the pipe alpha radians over the y-axis and beta radians over the z-axis, for
        # all alpha and beta in a given range (starting with the range 0 ~ pi). For each combination of alpha/beta
        # the total sum of the x-normal component squared is calculated. In case that the pipe is aligned, this sum
        # should be minimal.

        self.logger.debug("n_alpha = {}  n_beta = {}".format(n_alpha, n_beta))

        # the alpha and beta range, where alpha is the phi direction and beta the theta direction
        self.logger.debug("alpha line space {} {} {}".format(self.alpha_range_min, self.alpha_range_max, n_alpha))
        self.logger.debug("beta  line space {} {} {}".format(self.beta_range_min, self.beta_range_max, n_beta))
        self.alpha = np.linspace(self.alpha_range_min, self.alpha_range_max, n_alpha)
        self.beta = np.linspace(self.beta_range_min, self.beta_range_max, n_beta)

        if self.is_aligned and cnt is not None and cnt == 0:
            # is the pipe is already aligned we can skip the first iteration and set the alpha already
            # around zero
            (self.alpha_range_min, self.alpha_range_max) = self.get_reduced_range(0, self.alpha)
            (self.beta_range_min, self.beta_range_max) = self.get_reduced_range(0, self.beta)
            self.alpha = np.linspace(self.alpha_range_min, self.alpha_range_max, n_alpha)
            self.beta = np.linspace(self.beta_range_min, self.beta_range_max, n_beta)

            self.logger.debug("reducing alpha-beta range {} {}".format(self.alpha_range_min, self.alpha_range_max))

        # note that the resolution is defined in degrees rather than radians. I bit confusing, I know
        self.alpha_beta_resolution = np.degrees(np.linalg.norm([self.alpha[1] - self.alpha[0],
                                                                self.beta[1] - self.beta[0]]))

        # the alpha beta matrix with n_alpha / n_beta combinations
        [self.aa, self.bb] = np.meshgrid(self.alpha, self.beta, indexing='ij')

        # the x-coordinate of the vector after a rotation over the angle alpha over y axis (first) and then beta over
        # the z-axis.
        self.R0 = np.cos(self.aa) * np.cos(self.bb)
        self.R1 = -np.sin(self.bb)
        self.R2 = np.sin(self.aa) * np.cos(self.bb)

        self.xcoorsqrsum = np.zeros(self.aa.shape)
        xcoorsum = np.zeros(self.aa.shape)
        self.logger.debug("start calculating alpha-beta matrix with xcoor minimum")

        # creat a progress bar
        message = "Aligning pipe along the x-axis until resolution(={:6.3f}) < {:6.3f} or " \
                  "count(={}) > {}".format(self.alpha_beta_resolution,
                                           self.alpha_beta_tolerance, cnt, maxit - 1)

        # loop over all points and calculate the total deviation of the x coordinate after rotation
        # over alpha beta. If the rotation aligns the cylinder with x-axis, this summation xcoormin
        # becomes minimal
        for i in range(self.number_of_points):
            xcoor = self.R0 * self.normal_array[i, 0] + self.R1 * self.normal_array[i, 1] + \
                    self.R2 * self.normal_array[i, 2]
            xcoorsum += xcoor
            self.xcoorsqrsum += xcoor * xcoor

            # update the progress bar
            if (i % 20) == 0:
                self.emit(QtCore.SIGNAL("progressChanged"), i, self.number_of_points - 20, message)
                # this call prevents the GUI from stalling in case the CPU gets too busy
                QtGui.QApplication.processEvents()

        # remove progress bar
        self.emit(QtCore.SIGNAL("displayFinished"))
        self.emit(QtCore.SIGNAL("update_alpha_beta_contour_plot"))
        QtGui.QApplication.processEvents()

        # find the minimum in the xcoorsum matrix and return the corresponding alpha and beta angle
        [iamin, ibmin] = np.where(self.xcoorsqrsum == np.amin(self.xcoorsqrsum))
        # this prevents a bug in case that more than one minimum is found. Make sure you only pick the first one
        self.ia = iamin[0]
        self.ib = ibmin[0]

        # this value should also roughly represent the location of the rim
        self.xcoorsum_at_alpha_beta_min = old_div(xcoorsum[self.ia, self.ib], self.number_of_points)

        # the summation of the x-coordinate in case that the pipe is aligned is negative if the pipe is in the positive
        # x-direction because the rim is looking towards the negative x direction. In case the summation is positive,
        # the opposite is the case, which means we need to change the orientation of the pipe with pi rad

        #    self.rotation_angle_offset = 0
        #else:
        #    self.rotation_angle_offset = np.pi

        self.alpha_at_min = self.alpha[self.ia]
        self.beta_at_min = self.beta[self.ib]

        if cnt is not None and cnt > 0:
            self.logger.debug("Calculating miniumum by least squares")
            (am, bm) = self.least_square_minimum()
            if am is not None and bm is not None:
                self.logger.debug(
                    "overruling pixel min {} {} with {} {}".format(self.alpha_at_min, self.beta_at_min, am, bm))
                self.alpha_at_min = am
                self.beta_at_min = bm

        self.logger.debug(
            "Minimum found {} {} with alpha={} beta={} and xcoorsum {}".format(
                self.ia, self.ib, self.alpha_at_min, self.beta_at_min, self.xcoorsum_at_alpha_beta_min))

        if self.alpha_beta_resolution < self.alpha_beta_tolerance:
            self.alpha_beta_converged = True

        (self.alpha_range_min, self.alpha_range_max) = self.get_reduced_range(self.alpha_at_min, self.alpha)
        (self.beta_range_min, self.beta_range_max) = self.get_reduced_range(self.beta_at_min, self.beta)

        self.logger.debug("alpha min / plus {} {} beta min / plus {} {}".format(
            self.alpha_range_min, self.alpha_range_max, self.beta_range_min, self.beta_range_max))
        self.logger.debug("alpha beta reslution {}".format(
            self.alpha_beta_resolution))

    def least_square_minimum(self):
        # carry out a least square fit to get the minimum value of the alpha-beta 2d surface. Improves the
        # accuracy a bit

        degree = 3
        A1 = np.vander(self.aa.flatten(), degree)
        A2 = np.vander(self.bb.flatten(), degree)
        A = np.hstack((A1, A2))

        self.logger.debug("Start least square fitting")
        (coeffs, residuals, rank, sing_vals) = np.linalg.lstsq(A, self.xcoorsqrsum.flatten())

        acoeffs = coeffs[0:degree]
        bcoeffs = coeffs[degree:2 * degree]

        try:
            aminlst = old_div(-acoeffs[1], (2 * acoeffs[0]))
            bminlst = old_div(-bcoeffs[1], (2 * bcoeffs[0]))
        except ValueError:
            self.logger.warning("No 2nd order polynomial found")
            aminlst = None
            bminlst = None

        self.logger.debug("Coeff {}".format(coeffs))
        self.logger.debug("resid {}".format(residuals))
        self.logger.debug("rank  {}".format(rank))
        self.logger.debug("sing_vals {}".format(sing_vals))
        self.logger.debug("alsq {} blsq {}".format(aminlst, bminlst))

        return aminlst, bminlst

    # @profile
    def get_reduced_range(self, phi_at_min, allphis):
        """
        a routine to calculate the reduced range of angle around the central point phi_at_min
        In case a fscale>1 is given, the range is enlarged
        :param phi_at_min: the angle at which the last minimum was found
        :param allphis: the complite range of angle
        :param fscale: a scale factor to reduce (fscale<1) or enlarged (fscale>1) the range
        :return:
        """
        minphi = np.min(allphis)
        maxphi = np.max(allphis)
        phirange = (maxphi - minphi)
        minphi_new = phi_at_min - phirange * self.reduction_factor / 2.0
        maxphi_new = phi_at_min + phirange * self.reduction_factor / 2.0
        return (minphi_new, maxphi_new)

    # @profile
    def calculate_phi_theta_distribution(self):
        try:
            self.logger.info("Trying to calculate phi-theta distribution")
            self.phis = np.arctan2(self.normal_array[:, 1], self.normal_array[:, 2])
            self.thetas = np.arcsin(self.normal_array[:, 0])
            self.theta_phi_pdf, self.phi_edges, self.theta_edges = np.histogram2d(self.thetas, self.phis,
                                                                                  bins=self.n_histo_bins)
            self.logger.info("Successfully calculated the theta phi pdf")
        except TypeError:
            self.logger.warning("Failed to create the theta/phi pdf pdf")
            pass

    # @profile
    def save_image_frame(self, filename, renderWindow):
        self.logger.info("Write to : {}".format(filename))
        w2if = vtk.vtkWindowToImageFilter()
        w2if.SetInput(renderWindow)
        w2if.Update()
        writer = vtk.vtkPNGWriter()
        if vtk.VTK_MAJOR_VERSION <= 5:
            writer.SetInput(w2if.GetOutput())
        else:
            writer.SetInputConnection(w2if.GetOutputPort())
        writer.SetFileName(str(filename))
        writer.Write()

    # @profile
    def write_slice_file(self, filename):
        # Write the stl file to disk
        # warning: pytables module must be installed!

        if self.slice_dataframes is not None:
            (filebase, ext) = os.path.splitext(filename)

            filename_stat = "{}_stats{}".format(filebase,ext)

            if ext == ".xls":
                # write the data to an excel file. Each slice is stored in a separate page
                self.logger.info("Exporting to Excel file {}".format(filename))
                with pd.ExcelWriter(filename, append=False) as writer:
                    for key, df in self.slice_dataframes.groupby(level=0):
                        df.to_excel(writer, sheet_name=key)
                self.logger.info("Exporting to Excel statistics file {}".format(filename_stat))
                with pd.ExcelWriter(filename_stat, append=False) as writer:
                    self.slice_stat_df.to_excel(writer, sheet_name="Statistics")
            elif ext == ".h5":
                try:
                    # this should work, otherwise HDFStore does not work as well and stops you process
                    # this is the case for the py2exe executable
                    import tables
                except ImportError:
                    self.logger.warning("tables module can not be loaded due to HDF5.ddl not found error. "
                                        "Skipping writing the h5 file")
                else:
                    self.logger.info("Exporting to HDF5 file {}".format(filename))
                    # succeeded importing tables, so this should now work
                    with pd.HDFStore(filename, append=False) as store:
                        store["slice_dataframes"] = self.slice_dataframes

                        # self.logger.info("writing to HD5 slice file : {}".format(filename))
                        # self.slice_dataframes.to_hdf(filename,"slice_dataframes")
            else:
                self.logger.warning("extension {} not recognised. Skipping".format(ext))
            # with pd.io.pytables.HDFStore(str(filename), append=False) as store:
            #   store["slice_dataframes"] = self.slice_dataframes
            self.logger.info("Done.")

    # @profile
    def get_geometric_properies(self):
        # here the COG and the bounding box of the pipe are calculated. Also an estimate of the x position of
        # the rim is obtained

        centerOfMassFilter = vtk.vtkCenterOfMass()

        if vtk.VTK_MAJOR_VERSION <= 5:
            data = self.stl_surface.GetOutput()
            centerOfMassFilter.SetInput(data)
        else:
            data = self.stl_surface.GetOutput()
            centerOfMassFilter.SetInputData(data)

        centerOfMassFilter.SetUseScalarsAsWeights(False)
        centerOfMassFilter.Update()

        self.COG = centerOfMassFilter.GetCenter()
        self.bounds = np.asarray(data.GetBounds())
        self.corner_bottom_low = self.bounds[0:6:2]  # [x0 y0 z0] low left corner
        self.corner_top_high = self.bounds[1:6:2]  # [x1 y1 z1] oppositet op right corner
        self.mid_of_domain = 0.5 * (self.corner_bottom_low + self.corner_top_high)
        self.size_of_domain = self.corner_top_high - self.corner_bottom_low

        self.logger.info("{:40} : {}".format("Corner Bottom Low", self.corner_bottom_low))
        self.logger.info("{:40} : {}".format("Corner Top High", self.corner_top_high))
        self.logger.info("{:40} : {}".format("Lenght of Box", self.size_of_domain))
        self.logger.info("{:40} : {}".format("Mid if Pipe", self.mid_of_domain))
        self.logger.info("{:40} : {}".format("COG", self.COG))
        self.logger.info("{:40} : {}".format("Total Area", self.total_area))

    def vectors_from_common_point(self, ind_pair1, ind_pair2):
        """
        Given two pair of indices belong to two pair of points, return the two vectors point from
        the common point of the two pair. If the two pair do not have a common point, return None
        :param ind_pair1: first pair of indices [i,j]
        :param ind_pair2: second pair of inices [i,j]
        :return:
        """

        pair_points = ind_pair1 + ind_pair2
        icount = np.bincount(pair_points)
        self.logger.debug("bincont ont pairs {}".format(pair_points))
        vec1 = None
        vec2 = None
        if max(icount) > 1:

            # the index_pivot it the index of the  common point of the two pairs
            index_pivot = np.argmax(icount)

            # calculate the vectors belonging to the pairs
            vec1 = self.target_points.iloc[ind_pair1[0]] - self.target_points.iloc[ind_pair1[1]]
            vec1 = old_div(vec1, linalg.norm(vec1))

            # assume that the second index j must be the index_pivot, in that case change the sign of the
            # vector if actually i is the index
            if ind_pair1[0] == index_pivot:
                vec1 = -vec1

            vec2 = self.target_points.iloc[ind_pair2[0]] - self.target_points.iloc[ind_pair2[1]]
            vec2 = old_div(vec2, linalg.norm(vec2))
            if ind_pair2[0] == index_pivot:
                vec2 = -vec2

        return vec1, vec2

    # @profile
    def get_indices_nearest_neighbours(self):
        # this function determines from a list of x,y,z point which points are nearest neighbours. The np points in
        # the list are turned into a distance matrix np x np where at the location i,j the distance between point
        # i and j given (hence this is a symmetric matrix with zeros at the diagonal). From this distance matrix
        # the n_closest distances are searched for and returned in a list

        # distance function used by apply statement of the dataframe
        distance = lambda column1, column2: np.linalg.norm(column1 - column2)

        # this create the matrix of all the delta x, delta y, and delta z between all the points i with all the other
        # points j. The distance is calculated after that

        self.distance_X = self.target_points.X.apply(
            lambda col1: self.target_points.X.apply(lambda col2: distance(col1, col2)))

        self.distance_Y = self.target_points.Y.apply(
            lambda col1: self.target_points.Y.apply(lambda col2: distance(col1, col2)))

        self.distance_Z = self.target_points.Z.apply(
            lambda col1: self.target_points.Z.apply(lambda col2: distance(col1, col2)))

        self.distance = np.sqrt(np.square(self.distance_X) + np.square(self.distance_Y) + np.square(self.distance_Z))

        # turn the matrix dataframe into a 2D numpy array and set all the zero value (the diagonal of the distances
        # between the points with itself to a large value to get rid of them
        dist_array = self.distance.values
        dist_array[dist_array == 0] = 1e9

        # loop over the n_closest neighbours
        # only take into account the two closest pairs. Otherwise the outer ref points may form a pair as well
        # which messes up the nearest neighbour finder who is looking for the point belong to the most pairs
        n_closest = len(self.target_points)
        max_distance = self.max_pair_distance * 10 ** self.power_units
        self.logger.debug("checking {} point with max pair distance {}".format(n_closest, max_distance))
        self.neighbour_list = []
        for i in range(n_closest):
            # this is a trick to the i,j index of the minimum value in the 2D array
            (imin, jmin) = np.unravel_index(dist_array.argmin(), dist_array.shape)

            # get the line connecting this pair
            p1 = Point(self.target_points.iloc[imin])
            p2 = Point(self.target_points.iloc[jmin])

            # warning: the line.length give the 2D length (only taking into account the delta x and delta y)
            # therefore use the linalg norm funtion
            length = dist_array[imin, jmin]

            self.logger.debug("checking pair {}/{} and {}/{} with a distance of {} with {}."
                              "".format(imin, p1, jmin, p2, length, max_distance))

            if length < max_distance:
                self.logger.debug("ADDING pair {}".format(imin, jmin))
                # store the neighbours in the list which we need later to find the neirbouring pairs sorted on distance
                self.neighbour_list.append([imin, jmin])
            else:
                self.logger.debug("SKIPPING pair {} {}".format(imin, jmin))

            # take them out the distance array and go to the next point
            dist_array[imin, jmin] = dist_array[jmin, imin] = 1e9

        # now check the neighbours. Sometimes two left and right point next the the center point in the triplet
        # are considered as neighbour. Any neighbour pair with a point in between them will be removed
        self.logger.debug("neighbours before {}".format(self.neighbour_list))
        faulty_pairs = []
        for i, j in self.neighbour_list:

            # get the position of the two neighbour
            p1 = Point(self.target_points.iloc[i])
            p2 = Point(self.target_points.iloc[j])
            # the line connecting the neighbours
            line = LineString([p1, p2])

            length = dist_array[i, j]

            self.logger.debug("check neighbour points: i={}/{} j={}/{} line={} dist={}".format(
                i, p1, j, p2, line, length))

            # loop over all the other neighbour points
            for k in np.array(self.neighbour_list).flatten():
                if k == i or k == j:
                    # if this point is one of the neighbour i/j, skip it
                    continue

                # get the position of this neighbour point
                p3 = Point(self.target_points.iloc[k])

                # calculate the distance from this point k to the line between the current pair
                distance_to_line = line.distance(p3)

                # calculate the vectors belonging to the pairs
                vec1 = self.target_points.iloc[i] - self.target_points.iloc[k]
                vec1 = old_div(vec1, linalg.norm(vec1))
                vec2 = self.target_points.iloc[j] - self.target_points.iloc[k]
                vec2 = old_div(vec2, linalg.norm(vec2))

                # If the two vectors are in oposite direction, the dotprodut is negative
                dotprod = np.inner(vec1, vec2)

                self.logger.debug("compare current pair with point {} {} with distance {} and dotprod={}".format(k, p3,
                                                                                                  distance_to_line,
                                                                                                  dotprod))

                # the distance of this neighbour point is less than 0.5 than the distance of the current
                # neighbour pair. So this pair has a point in between and can not be a pair. Take it out!
                # another demand is that the dotproduct is negative so that you are sure that the point is inbetween
                # the pair
                if distance_to_line < self.exclude_near_distance * length and dotprod < 0:
                    self.logger.debug("REMOVING PAIR {}/{} {}/{} {}/{} : {}".format(i, p1, j, p2, k, p3,
                                                                                    distance_to_line))
                    faulty_pairs.append([i, j])
                    # go to the next pair
                    break

        # now actually take out the faulty pairs
        for [i, j] in faulty_pairs:
            if len(self.neighbour_list) > 2:
                self.neighbour_list.remove([i, j])

        self.logger.debug("neighbours after first check {}".format(self.neighbour_list))
        # last check: each pair should be able to be combined with another pair that is almost in the
        # same direction, such that the angle between the vectors is close too. If that is not the case,
        # take it out
        faulty_pairs = []
        cos_phi = np.cos(np.deg2rad(self.max_angle_reference_points))
        for cnt1, [i1, j1] in enumerate(self.neighbour_list):
            has_partner_pair = False
            for cnt2, [i2, j2] in enumerate(self.neighbour_list):
                if cnt1 == cnt2:
                    continue

                # call this routine to get the two vectors pointing away from the common point
                vec1, vec2 = self.vectors_from_common_point([i1, j1], [i2, j2])

                if vec1 is None or vec2 is None:
                    # one of the vectors was None, so continue
                    continue

                # If the two vectors are in the same direction, the dotprodut value is larger than cos(angle_between_vecs)
                dotprod = np.inner(vec1, vec2)
                self.logger.debug("cnt1={}/{}/{} cnt2{}/{}/{} dotprod={} cos_phi={}".format(
                    cnt1, i1, j1, cnt2, i2, j2, dotprod, cos_phi))

                # since the two vectors are point  away from the common point, if the are aligned the cos_phi is -1
                if dotprod < -cos_phi:
                    self.logger.debug("TRUE!".format(cnt1, cnt2, vec1, vec2, dotprod))
                    has_partner_pair = True

            if not has_partner_pair:
                p1 = Point(self.target_points.iloc[i1])
                p2 = Point(self.target_points.iloc[j1])
                self.logger.info("No partner found. Taking out {}/{} {}/{}".format(i1, p1, j1, p2))
                # no partner pair has been found. Take this pair out of the list
                faulty_pairs.append([i1, j1])

        # take out the faulty pairs
        for [i, j] in faulty_pairs:
            # only take out the pair if the neighbour list is longer than 2.
            if len(self.neighbour_list) > 2:
                self.neighbour_list.remove([i, j])

        self.logger.info("neighbours after second {}".format(self.neighbour_list))

        if not self.neighbour_list:
            self.has_targets = False

    # @profile
    def get_index_reference_point(self):
        # in this routine it is assumed that the pipe has been aligned and the neighbours list has been created

        indices = np.array(self.neighbour_list).flatten()

        try:
            icount = np.bincount(indices)
            # it is assumed that the target points which has the most neighbours is the reference point
            self.index_reference_point = np.argmax(icount)
            self.logger.debug("index_reference point : {}".format(self.index_reference_point))
            self.reference_point = self.target_points.iloc[self.index_reference_point]
            self.logger.debug("reference position = {}".format(self.reference_point))
        except TypeError:
            self.logger.warning("An empty neighbour list was found. Perhaps change the parameters")
            self.index_reference_point = None
            self.reference_point = None

    def find_ovality_targets(self, dist_first = 20, dist_second=40, dist_third = 60, axis_alignment = 0.95, progres_function=None):
        """
        Find the ovality targets. These are the four targets in a line

        :return:
         Found targets are saved to self.ovality_targets
        """

        self.logger.info("first {} second {} third {} angle = {}".format(dist_first, dist_second, dist_third, axis_alignment))

        # TODO: Add text to points
        # see http://www.vtk.org/Wiki/VTK/Examples/Cxx/Visualization/Follower

        points = self.target_points.copy(True)
        aPoints = points.values

        self.ovality_targets = list()
        iPoint = 0

        while True:

            myPoint = aPoints[iPoint, :]

            relativePosition = aPoints - myPoint
            distance = np.linalg.norm(relativePosition, axis=1)

            # get the distance from low to high
            index_sorted = np.argsort(distance)

            # now check if this point qualifies as a 4-point target
            # this is the case if the two nearest points are
            # within 10 and a third one within 20

            # note that distance index_sorted(0) is always 0 at it is
            # the point itself
            first = distance[index_sorted[1]]
            second = distance[index_sorted[2]]
            third = distance[index_sorted[3]]

            if (first < dist_first) and (second < dist_second) and (third < dist_third):
                # we found a target point
                # print 'four points that are pretty close'
                suspects = aPoints[index_sorted[0:4], :]
                relSuspects = suspects - myPoint

                # check if the points are on a line by calculating the cos(angle) between the points
                rel1 = relSuspects[1, :]
                rel2 = relSuspects[2, :]
                rel3 = relSuspects[3, :]
                cosang1 = old_div(np.dot(rel1, rel2), (np.linalg.norm(rel1) * np.linalg.norm(rel2)))
                cosang2 = old_div(np.dot(rel3, rel2), (np.linalg.norm(rel3) * np.linalg.norm(rel2)))

                if abs(cosang1) > axis_alignment and abs(cosang2) > axis_alignment:

                    # The four points that we've found are one a line

                    average = np.mean(aPoints[index_sorted[0:4], :], axis=0)
                    self.ovality_targets.append(average)
                    aPoints = np.delete(aPoints, index_sorted[0:4], 0)

                    # print "point found"
                    # print cosang1
                    # print cosang2

                    # print(first)
                    # print(second)
                    # print(third)

                    if iPoint > 4:
                        iPoint -= 4
                    else:
                        iPoint = 0

            iPoint += 1

            if progres_function:
                progres_function(iPoint, aPoints.shape[0])

            # stop if we have less than 4 points left
            if aPoints.shape[0] < 4:
                break

            if iPoint >= aPoints.shape[0]:
                break

    def __getslicepoints__(self, position, normal):
        """
        Returns a ndarray with the 3D points and normals
        of the slice

        Example:
        points, normals,n_slice_points = self.__getslicepoints__(position, normal)

        :param position:
        :param normal:
        :return:
        """

        # create the cut plane.
        plane = vtk.vtkPlane()
        plane.SetNormal(normal)
        plane.SetOrigin(position)

        cutter = vtk.vtkCutter()
        cutter.SetCutFunction(plane)
        cutter.SetInputConnection(self.stl_surface.GetOutputPort())
        cutter.Update()

        cutter_out = cutter.GetOutput()

        slice_points = cutter_out.GetPoints()
        slice_point_data = cutter_out.GetPointData()
        slide_data_Normals = slice_point_data.GetArray("Normals")

        # first store all the points and normals in a numpy array
        n_slice_points = slice_points.GetNumberOfPoints()

        points = np.empty((n_slice_points, 3))
        normals = np.empty((n_slice_points, 3))
        for i in range(n_slice_points):
            # the the coordinates of this point
            points[i, :] = np.array(slice_points.GetPoint(i))

            # the normal vector components
            normals[i, :] = slide_data_Normals.GetTuple(i)

        return points, normals, n_slice_points

    def get_slice_data(self, position, normal):
        """
        Returns the 2D coordinates of the pipe-points for a 2D-slice defined by
        position and normal.

        The vertical vector (y) is create between the mean of the points in the slice and
        the give position

        the slice point projected in the fitted 2d plane
        the center of the plane is the mean of the coordinates
        the first coordinate is "sideways"
        the second coordiate is "up" in which the target is defined at the top of the pipe
        the target is stored in "target"

        :param position:
         A point in the slice
        :param normal:
         the normal vector of the slice
        :return:
        position_2d,coordinates : projected position and slice points
        center, side, up : coordinate system used for the 2d points
        """

        # create the cut plane.
        points, normals, n_points = self.__getslicepoints__(position, normal)

        # calculate the center of the pipe, in the plane
        # we know that some of the points may be missing, to the center is just an estimate
        center = np.mean(points, axis=0)

        # move up such that it it in the plane
        move = gu.vec_projection(center - position, normal)
        center = center - move

        # create orthonormal basis
        up = gu.normalize(position - center)
        side = np.cross(up, normal)

        points2d = np.empty((n_points, 2))

        # Save target point (which is an average of points) and points
        position2d = gu.projection_onto_2d_plane(position, center, side, up)
        for i in range(n_points):
            points2d[i, :] = gu.projection_onto_2d_plane(points[i, :], center, side, up)

        return position2d, points2d, center, side, up

    def calc_tangent_plane_at(self, position, relaxation = 0.1, tolerance_deg = 0.1, max_iter=1000, initial_guess = np.array([1, 0, 0]),feedbackfunction=None):
        """
        Calculate the ovality at the previously found ovality targets (obtained from self.ovality_targets)
        :return:
         Found targets are saved to self.ovality_plane_normals
        """

        normal = initial_guess

        self.logger.info("Finding slice orientation for position {}".format(position))

        counter = 0

        while True:
            # remember previous normal
            previous_normal = normal

            # From these normals, we will try to construct
            # a slice plane orientation such that the normal
            # are all in the plane
            # The strategy to do this is as follows
            # 1. divide the normals into two groups
            # 2. for each group, take the average vector
            # 3. the cross product of these two average vectors
            #    will be the new normal for the slice plane

            points, normals, n_slice_points = self.__getslicepoints__(position, normal)

            if n_slice_points == 0:
                raise Exception('Empty slice')


            refecence_vector = normals[1, :]
            cosang = np.empty([n_slice_points])

            for i in range(n_slice_points):
                current_vector = normals[i, :]
                cosang[i] = old_div(np.dot(current_vector, refecence_vector), (
                np.linalg.norm(refecence_vector) * np.linalg.norm(current_vector)))

            # vectors in the same direction as reference_vector will have a cosang of 1
            # vectors opposite (in the same plane!) as reference_vector will have a cosang of -1
            # vectors perpendicular to refernce vector will have a cosang of 0

            # sort the absolute value of cosang from low to high
            # the 50% low values will be one group, the 50% high values will be the second one

            index_sorted = np.argsort(abs(cosang))
            index_a, index_b = np.array_split(index_sorted, 2)

            vec_a = np.array([0, 0, 0])
            for i in index_a:
                if cosang[i] > 0:
                    vec_a = vec_a + normals[i, :]
                else:
                    vec_a = vec_a - normals[i, :]

            vec_b = np.array([0, 0, 0])
            for i in index_b:
                if cosang[i] > 0:
                    vec_b = vec_b + normals[i, :]
                else:
                    vec_b = vec_b - normals[i, :]

            vec_a = vec_a
            vec_b = vec_b

            calc_normal = np.cross(vec_a, vec_b)
            calc_normal = old_div(calc_normal, np.linalg.norm(calc_normal))

            # align normal to have same direction is previous normal (else we will never converge)
            if np.dot(calc_normal, previous_normal) < 0:
                calc_normal = -calc_normal

            # angle change between normals
            convergence_step = np.degrees(np.arccos(np.dot(calc_normal, previous_normal)))

            # apply relaxation

            normal = (relaxation * calc_normal) + ((1 - relaxation) * previous_normal)  # does not conserve length!
            normal = old_div(normal, np.linalg.norm(normal))

            self.logger.info("Iteration {} - Convergence error ={}deg - normal = {}".format(counter,convergence_step, normal))
            if feedbackfunction:
                if old_div(counter,10.0) == round(old_div(counter,10.0)):
                    feedbackfunction("Iteration {} - Convergence error ={}deg - normal = {}".format(counter,convergence_step, normal))

            if convergence_step < tolerance_deg:
                # self.ovality_plane_normals.append(normal)
                self.logger.info("Found normal")
                return normal
                break

            # maxiter
            counter += 1
            if counter > max_iter:
                raise MaxNumberOfIterationsExceeded('Maximum number of iterations exceeded when fitting plane through target')
