"""""
small utility to initialise the default settings and create a yaml file
"""""
import yaml


def set_and_write_default_values(file_name=None):
    """""
    routine to generate the default settings. Use only one time to generate the yaml settings file. Then, turn off
    and just set and read the settings from the yaml file
    """""
    settings = dict()
    settings["general"] = dict()
    settings["general"]["application_name"] = "PipeSlicer"
    settings["general"]["organisation_name"] = "Heerema Marine Contractors"
    settings["general"]["organisation_domain"] = "hmc-heerema.com"

    settings["nsis"] = dict()
    settings["nsis"]["run_py2exe"] = True # run the py2exe script and generate a dist directory with the executable
    settings["nsis"]["create_the_script"] = True # create the nsis installer script
    settings["nsis"]["compile_the_script"] = True # compile the nsis installer script and creat a installer
    settings["nsis"]["compressor"] = "lzma" # the comressor to use: lzma|bzip2|zlib. The first is the best but slow
    # write the settings to the yaml file
    if file_name is not None:
        with open(file_name, "w") as stream:
            yaml.dump(settings, stream=stream, default_flow_style=False)

    return settings


