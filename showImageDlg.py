__author__ = 'eelcovv'
# this dialog will show the image of the pipe

import logging

from PyQt4 import QtGui, QtCore

import matplotlib.gridspec as gridspec
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt4 import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import re
import numpy as np
from numpy import pi


class ShowImageDlg(QtGui.QDialog):
    def __init__(self, button_handle, parent=None):
        super(ShowImageDlg, self).__init__(parent)

        self.logger = logging.getLogger(__name__)

        # a handle to the button activating this windows, used to change the button activation when closing agai
        self.button_handle = button_handle

        # a close button
        self.buttonBox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Close)

        # a combobox to ask for the plot type
        labelCombo = QtGui.QLabel(self.tr("Plot Type:"))
        labelCombo.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignBottom)
        self.plotTypeComboBox = QtGui.QComboBox()
        self.plotTypeComboBox.addItem("View on Pipe")
        self.plotTypeComboBox.addItem("Linear Profiles")

        # this variable is used to pass the current to image which can be shown. Set from outside this class
        self.image_name_list = [None, None]

        self.image = None

        self.label = QtGui.QLabel()

        # a verical layout combo box and two buttons
        buttonLayout = QtGui.QVBoxLayout()

        buttonLayout.addWidget(self.plotTypeComboBox)
        buttonLayout.addStretch()
        buttonLayout.addWidget(self.buttonBox)

        graphLayout = QtGui.QVBoxLayout()
        graphLayout.addWidget(self.label)



        # a horizontal layout to store the canvas left and button etc right
        # the integer 3 and 1 give the relative sizes of the left and right part
        layout = QtGui.QHBoxLayout()
        # layout.addWidget(self.canvas,10)
        layout.addLayout(graphLayout, 10)
        layout.addLayout(buttonLayout, 1)

        self.setLayout(layout)

        # add some signals to the buttons and
        self.connect(self.buttonBox, QtCore.SIGNAL("rejected()"),
                     self, QtCore.SLOT("close()"))

        self.connect(self.plotTypeComboBox, QtCore.SIGNAL("currentIndexChanged(int)"),
                     self.show_image)

        settings = QtCore.QSettings()

        size = settings.value("ShowImageDlg/Size",
                              QtCore.QVariant(QtCore.QSize(800, 500))).toSize()
        self.resize(size)
        position = settings.value("ShowImageDlg/Position",
                                  QtCore.QVariant(QtCore.QPoint(0, 0))).toPoint()
        self.move(position)

        self.setWindowTitle("Pipe Image")

    def show_image(self, image_number):

        self.logger.debug("updating {}".format(self.image_name_list))
        try:
            if self.plotTypeComboBox.currentText() == "View on Pipe":
                image_name = self.image_name_list[0]
            else:
                image_name = self.image_name_list[1]
        except (IndexError, TypeError):
            image_name = "Could not set image name of this type"

        try:
            self.logger.debug("trying to show the image {}".format(image_name))
            pixmap = QtGui.QPixmap(image_name)
            self.label.setPixmap(pixmap)
            self.label.show()
            self.setWindowTitle(image_name)
        except (AttributeError, TypeError):
            self.label.setText("No image available : {}".format(image_name))
            self.label.show()
            self.logger.info("Could not set the pixmap")

    def update_logger(self, level):
        self.logger.setLevel(level)

    def closeEvent(self, event):
        # before closing the window store its size and position
        settings = QtCore.QSettings()

        settings.setValue("ShowImageDlg/Size", QtCore.QVariant(self.size()))
        settings.setValue("ShowImageDlg/Position",
                          QtCore.QVariant(self.pos()))

        # uncheck the showSpectraPlot button before closing the dialog
        if not self.button_handle.isEnabled():
            self.button_handle.setEnabled(True)
