__author__ = 'Ruben de Bruin'
"""
Test-file for fitting an ellipse
ref: http://www.math.muni.cz/~zelinka/cjh/svd.pdf
"""

import numpy as np
import matplotlib.pyplot as plt
import oval_fitter as ovalf

if __name__ == '__main__':

    data = np.array([[-2.8939, 4.1521],
            [-2.0614, 2.1684],
            [-0.1404, 1.9764],
            [2.6772, 3.0323],
            [5.1746, 5.7199],
            [3.2535, 8.1196],
            [-0.1724, 6.8398]])

    # or
    # data = np.loadtxt("""c:\\data\\test.txt""")

    fig = plt.figure()

    ovalf.ellipse_make_plot(data,outdir="c:\\data",fname="test")

    plt.show()